package com.yihaodian.bi.kafka.consumer;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.common.StatConstants.KafkaConsumerStat;
import com.yihaodian.bi.kafka.TrackerVO;

/**
 * 类TrackerBatchConsumer.java的实现描述：TODO 类实现描述
 * 
 * @author zhaoheng Jul 20, 2015 10:23:55 AM
 */
public class KafkaTrackerConsumerThread extends Thread {

    public static final String KAFKATOPIC = "tracker";
    public static final Logger LOG        = LoggerFactory.getLogger(KafkaTrackerConsumerThread.class);
    TrackerVO                  tracker    = null;
    BlockingQueue<TrackerVO>   queue      = new LinkedBlockingDeque<TrackerVO>(100);
    KafkaTrackerConsumer       consumer;

    // 统计数据
    Pattern                    p          = Pattern.compile("^https?://[^/]*yhd\\.com.*");
    boolean                    isRun      = false;
    private KafkaConsumerStat  stat;

    public KafkaTrackerConsumerThread(String groupId, boolean isRun) throws IOException{
        this(groupId, KAFKATOPIC, isRun);
    }

    public KafkaTrackerConsumerThread(String groupId) throws IOException{
        this(groupId, KAFKATOPIC, false);
    }

    KafkaTrackerConsumerThread(String groupId, String topic, boolean isRun) throws IOException{
        init(groupId, topic);
        this.isRun = isRun;
        stat = new KafkaConsumerStat(groupId);
    }

    public BlockingQueue<TrackerVO> getQueue() {
        return queue;
    }

    private void init(String groupId, String topic) throws IOException {
        setDaemon(true);
        if (Debug.DEBUG) {
            consumer = new OfflineConsumer("sample.dat.zip");
        } else {
            consumer = new OnlineKafkaTrackerConsumer(groupId, topic);
        }
    }

    public void run() {
        while (consumer.hasNext()) {
            if (!this.isRun) break;

            String s = consumer.next();
            stat.incr(stat.KAFKA_TRACKER_CONSUMER_COUNT);

            // 过滤来自百度的爬虫数据
            if (s.contains("http://www.baidu.com/search/spider.html")) {
                stat.incr(stat.KAFKA_TRACKER_CONSUMER_FILTER_BAIDU);
                continue;
            }
            String cols[] = s.split("\\t");
            // 列小于30的丢弃
            if (cols.length < 30) {
                stat.incr(stat.KAFKA_TRACKER_CONSUMER_FILTER_COLUMN_LT30);
                continue;
            }
            // 过滤URL
            if (!StringUtils.isEmpty(cols[1])) {// 为空的不处理
                Matcher m = p.matcher(cols[1]);
                if (!m.find()) {
                    stat.incr(stat.KAFKA_TRACKER_CONSUMER_FILTER_URL_NONYHD);
                    continue;
                }
            }

            if (cols[1].startsWith("http://union.yihaodian.com/link_make/viewPicInfo.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewSearchBox.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewRanking.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/getUserCookies.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewShoppingWindow.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/getUserCookies.do")) {
                stat.incr(stat.KAFKA_TRACKER_CONSUMER_FILTER_URL_UNION);
                continue;
            }

            tracker = new TrackerVO(cols);

            try {
                queue.put(tracker);
                stat.intervalWriteAndClean();
            } catch (Exception e) {
                LOG.error("exception", e);
            }
        }
        if (Debug.DEBUG) System.out.println("KafkaTrackerConsumerThread stopped....");
        stat.intervalWriteAndClean(true);
    }

    public void setIsRun(boolean b) {
        isRun = b;
    }

}
