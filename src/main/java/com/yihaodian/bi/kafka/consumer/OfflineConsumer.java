package com.yihaodian.bi.kafka.consumer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.yihaodian.bi.common.util.FileUtil;

public class OfflineConsumer implements KafkaTrackerConsumer {

    BufferedReader             reader;
    Iterator<BufferedReader>   readerIter;
    String                     str;
    public static List<String> mockList  = null;
    public static int          mockPoint = 0;

    public OfflineConsumer(String file){
        try {
            readerIter = FileUtil.readZipFile(file).iterator();
            if (readerIter.hasNext()) reader = readerIter.next();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized boolean hasNext() {
        try {
            str = reader.readLine();
            if (str != null) return true;
            if (!readerIter.hasNext()) return false;
            reader.close();
            reader = readerIter.next();
            str = reader.readLine();
            if (str == null) {
                reader.close();
                return false;
            }
        } catch (IOException e) {
        }
        return true;
    }

    @Override
    public synchronized String next() {
        return str;
    }
}
