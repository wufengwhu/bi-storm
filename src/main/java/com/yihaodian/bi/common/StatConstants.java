package com.yihaodian.bi.common;

import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.cached.AsyncCommitCMap;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.cached.ConcurrentMemoryCMap;
import com.yihaodian.bi.cached.factory.CMapFactory;
import com.yihaodian.bi.cached.factory.HBaseCMapFactory;
import com.yihaodian.bi.common.util.DateUtil;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.storm.BaseStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 统计类信息 类StatConstants.java的实现描述：TODO 类实现描述
 *
 * @author zhaoheng Aug 5, 2015 1:33:40 PM
 */
public class StatConstants {

    public static int INTERVAL = DateUtil.MINUTE_FIVE;
    public static SimpleDateFormat FORMATER = DateUtil.YYYYMMDDHHMM;
    public static boolean OPEN_STAT = true;
    public static final Logger LOG = LoggerFactory.getLogger(StatConstants.class);
    private static AbstractBaseCMap statHbaseAsyncLRU = null;

    public synchronized static AbstractBaseCMap getStatAsyncHBase() throws IOException {
        if (statHbaseAsyncLRU == null) {
            if (!Debug.DEBUG) {
                statHbaseAsyncLRU = new AsyncCommitCMap(
                        HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.STAT_KV_TABLE,
                                false, null), true,
                        CMapFactory.ASYNC_INTERVAL);
            } else {
                statHbaseAsyncLRU = new ConcurrentMemoryCMap(new ConcurrentHashMap(CMapFactory.CACHE_SIZE));
            }
        }
        return statHbaseAsyncLRU;
    }

    public static abstract class Stat implements Serializable {

        protected long lastPoint = DateUtil.getLastCheckPointWholeFiveMinute();
        protected int intervalMinute = INTERVAL;
        protected int intervalMinuteMills = intervalMinute * 60 * 1000;
        protected Map<String, AtomicInteger> incs = new ConcurrentHashMap<String, AtomicInteger>();

        public void incr(String key) {
            incr(key, 1);
        }

        public void incr(String key, int inc) {
            if (!OPEN_STAT) {
                return;
            }
            if (incs.get(key) == null) {
                incs.put(key, new AtomicInteger(inc));
            } else {
                incs.get(key).addAndGet(inc);
            }
        }

        protected synchronized void intervalWriteToCMapAndClean(CMap cmap, String prefix, boolean writeNow) {
            if (OPEN_STAT && cmap != null) {
                long cur = System.currentTimeMillis();
                if (writeNow || cur - lastPoint > intervalMinuteMills) {
                    lastPoint = DateUtil.getRecentMinutePoint(intervalMinute * -1);
                    String dt = FORMATER.format(new Date(lastPoint));
                    int tmpValue = 0;
                    for (String key : incs.keySet()) {
                        tmpValue = incs.get(key).get();
                        incs.get(key).set(0);
                        try {
                            cmap.incr(prefix + "_" + key + "_" + dt, tmpValue);
//                            if (Debug.DEBUG) System.out.println(prefix + "_" + key + "_" + dt + "="
//                                    + cmap.get(prefix + "_" + key + "_" + dt));
                        } catch (Exception e) {
                            LOG.error("write stat exception", e);
                        }
                    }
                }
            }
        }
    }

    public static class SpoutBoltStat extends Stat {

        protected static CMap    cache;
        protected static String  KAFKA_SPOUT_SWITCHER = "kafka_spout_switcher";
        protected static String  ON                   = "on";
        protected static String  OFF                  = "off";
        protected static long    HEART_BEAT_INTERVAL  = 30 * 1000;
        protected static long    HEART_BEAT_INIT_TIME = 3 * 60 * 1000;
        protected static long    lastTime             = System.currentTimeMillis();
        protected static boolean init                 = true;

        static {
            try {
                cache = getStatAsyncHBase();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        protected String tag;
        protected BaseStat base;

        public SpoutBoltStat(BaseStat k) {
            base = k;
            tag = k.getTag();
        }

        public SpoutBoltStat(String tag) {
            this.tag = tag.toLowerCase();
        }

        public Long getValue(String key, String dt) {
            try {
//                LOG.info("key:" + tag + "_" + key + "_" + dt  + ";value:" + cache.getLong(tag + "_" + key + "_" + dt));
                
                if (cache.getLong(tag + "_" + key + "_" + dt) == null)
                    return 0L;
                return cache.getLong(tag + "_" + key + "_" + dt);
            } catch (NumberFormatException ne) {
                return 0L;
            } catch (Exception e) {
                LOG.error("getValue exception", e);
                return 0L;
            }
        }

        public void intervalWriteAndClean() {
            intervalWriteToCMapAndClean(cache, tag, false);
        }

        public void intervalWriteAndClean(boolean writeNow) {
            intervalWriteToCMapAndClean(cache, tag, true);
        }

        public void setSwitcher(boolean b) throws Exception {
            if (b) cache.put(tag + "_" + KAFKA_SPOUT_SWITCHER, ON);
            else cache.put(tag + "_" + KAFKA_SPOUT_SWITCHER, OFF);
        }

        public boolean isAlive() throws Exception {
            return ON.equals(cache.get(tag + "_" + KAFKA_SPOUT_SWITCHER));
        }

        public boolean keepAlive() throws Exception {
            if (Debug.DEBUG) isAlive();
            if (OPEN_STAT && cache != null) {
                long cur = System.currentTimeMillis();
                if (init && cur - lastTime > HEART_BEAT_INIT_TIME) {
                    init = false;
                    lastTime = cur;
                    return isAlive();
                } else if (!init && cur - lastTime > HEART_BEAT_INTERVAL) {
                    lastTime = cur;
                    return isAlive();
                }
            }

            return true;
        }
    }

    public static class KafkaConsumerStat extends SpoutBoltStat {

        public static String KAFKA_TRACKER_CONSUMER_COUNT = "tracker_cum_count";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_COUNT = "tracker_cum_filter_count";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_COLUMN_LT30 = "tracker_cum_filter_cols_lt30";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_BAIDU = "tracker_cum_f_baidu";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_URL_NONYHD = "tracker_cum_f_urlnonyhd";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_URL_UNION = "tracker_cum_f_urlunion";
        public static String KAFKA_TRACKER_CONSUMER_FILTER_BUTTON = "tracker_cum_f_button";

        public KafkaConsumerStat(String k) {
            super(k);
        }
    }

    public static class KafkaSpoutStat extends SpoutBoltStat {

        public static String KAFKA_TRACKER_SPOUT_EMIT_ALL = "tracker_spout_emit";
        public static String KAFKA_TRACKER_SPOUT_EMIT_SUCCESS = "tracker_spout_emit_succ";
        public static String KAFKA_TRACKER_SPOUT_EMIT_FAIL = "tracker_spout_emit_fail";
        public static String KAFKA_TRACKER_SPOUT_EMIT_MSG_ALL = "tracker_spout_msg";
        public static String KAFKA_TRACKER_SPOUT_EMIT_MSG_SUCCESS = "tracker_spout_msg_succ";
        public static String KAFKA_TRACKER_SPOUT_EMIT_MSG_FAIL = "tracker_spout_msg_fail";
        public static String KAFKA_TRACKER_SPOUT_TRAN_SKIP = "tracker_spout_tran_skip";

        public KafkaSpoutStat(BaseStat k) {
            super(k);
        }

        public KafkaSpoutStat(String k) {
            super(k);
        }

    }

    public static class PVUVStat extends SpoutBoltStat {

        public static String UV_BOLT_GUID_NULL = "tracker_bolt_guid_null";
        public static String UV_BOLT_GUID_NOTNULL = "tracker_bolt_guid_notnull";

        public PVUVStat(BaseStat k) {
            super(k);
        }

        public PVUVStat(String k) {
            super(k);
        }
    }

    public static class CacheStat extends SpoutBoltStat {

        public static String TRACKER_BOLT_RECV = "tracker_bolt_recv";
        public static String TRACKER_BOLT_SUCC = "tracker_bolt_succ";
        public static String TRACKER_BOLT_FAIL = "tracker_bolt_fail";
        public long lastPoint = DateUtil.getRecentMinutePoint(intervalMinute * -1);
        public long lastValue = 0;

        @Override
        public boolean isAlive() {
            long curLastPoint = DateUtil.getRecentMinutePoint(intervalMinute * -1);
            if (curLastPoint != lastPoint) {
                lastPoint = curLastPoint;
                lastValue = 0;
            }
            String dt = StatConstants.FORMATER.format(new Date(curLastPoint));
            long curValue = getValue(TRACKER_BOLT_SUCC, dt);
            if (curValue - lastValue <= 0 && lastValue != 0) {
                LOG.warn("Not Alive!!! curValue:" + curValue + "; lastValue:" + lastValue);
                return false;
            }

            lastValue = curValue;
            return true;

        }

        public CacheStat(BaseStat k) {
            super(k);
        }

        public CacheStat(String k) {
            super(k);
        }

    }

    public static void main(String[] args) throws InterruptedException {
        Debug.DEBUG = true;
        KafkaSpoutStat test = new KafkaSpoutStat("Test");
        while (true) {
            // test.incr(test.KAFKA_TRACKER_SPOUT_EMIT_ALL);
            // test.intervalWriteAndClean();
            long dt = DateUtil.getRecentMinutePoint(-5);
            System.out.println(dt + " = " + DateUtil.YYYYMMDDHHMMSS.format(dt));
            Thread.sleep(50L);
        }
    }
}
