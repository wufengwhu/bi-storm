package com.yihaodian.bi.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;

public class FileUtil {

    public static Configuration getConf() {
        Configuration conf = new Configuration();
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        conf.set("fs.defaultFS", "hdfs://yhd-jqhadoop2.int.yihaodian.com:8020");
        conf.set("dfs.namenode.servicerpc-address", "yhd-jqhadoop2.int.yihaodian.com:8022");
        return conf;
    }

    public static List<BufferedReader> readZipFile(String file) throws Exception {
        List<BufferedReader> brs = new ArrayList<BufferedReader>();
        ZipFile zf = new ZipFile(file);
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        ZipInputStream zin = new ZipInputStream(in);
        ZipEntry ze;
        while ((ze = zin.getNextEntry()) != null) {
            if (ze.isDirectory()) {
            } else {
                long size = ze.getSize();
                if (size > 0) {
                    brs.add(new BufferedReader(new InputStreamReader(zf.getInputStream(ze))));
                }
            }
        }
        zin.closeEntry();
        return brs;
    }

    public static BufferedReader readFileByHadoop(String f) throws Exception {
        FileSystem fs = FileSystem.get(getConf());
        FileStatus fss = fs.getFileStatus(new Path(f));
        InputStream s = fs.open(new Path(f));
        return new BufferedReader(new InputStreamReader(s));
    }

    public static void main(String[] args) throws Exception {
        TrackerLogicUtils.getChannelHBaseLRUReadonly();
    }
}
