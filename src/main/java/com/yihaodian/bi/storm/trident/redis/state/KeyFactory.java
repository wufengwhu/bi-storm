/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yihaodian.bi.storm.trident.redis.state;

import backtype.storm.tuple.Values;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * KeyFactory defines conversion of state key (which could be compounded) -> Redis key.
 */
public interface KeyFactory extends Serializable {
    /**
     * Converts state key to Redis key.
     * @param key state key
     * @return Redis key
     */
    String build(List<Object> key);

    /**
     * Default Key Factory
     */
    class DefaultKeyFactory implements KeyFactory {
        /**
         * {@inheritDoc}
         * <p/>
         * Currently DefaultKeyFactory returns just first element of list.
         *
         * @param key state key
         * @return Redis key
         * @throws RuntimeException when key is compound key
         * @see KeyFactory#build(List)
         */
        @Override
        public String build(List<Object> key) {
            if (key.size() != 1)
                throw new RuntimeException("Default KeyFactory does not support compound keys");

            return (String) key.get(0);
        }
    }

    class GlobStatKeyFactory implements KeyFactory, Function {

        @Override
        public void execute(TridentTuple input, TridentCollector collector) {
            // "glob_" + timeline.substring(0, 11) + "0"; // 全局统计;
            String timeline = input.getStringByField("timeline");
            collector.emit(new Values("glob_" + timeline.substring(0, 11) + "0"));
        }

        @Override
        public String build(List<Object> key) {
            return null;
        }

        @Override
        public void prepare(Map map, TridentOperationContext tridentOperationContext) {

        }

        @Override
        public void cleanup() {

        }
    }

    class AppStatKeyFactory implements KeyFactory, Function {

        @Override
        public void execute(TridentTuple input, TridentCollector collector) {
            int platformId = input.getIntegerByField("platformId");
            String timeline = input.getStringByField("timeline");
            String app_stat_key = "user_" + platformId + "_1_" + timeline.substring(0, 11) + "0";
            collector.emit(new Values(app_stat_key));
        }

        @Override
        public String build(List<Object> key) {
            return null;
        }

        @Override
        public void prepare(Map map, TridentOperationContext tridentOperationContext) {

        }

        @Override
        public void cleanup() {

        }
    }

}

