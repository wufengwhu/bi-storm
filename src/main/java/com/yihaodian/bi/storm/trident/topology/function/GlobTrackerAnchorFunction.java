package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.HTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.EXIST;
import java.io.IOException;
import java.util.Map;

/**
 * @author fengwu on 15/7/29
 */
public class GlobTrackerAnchorFunction extends BaseFunction {

    private static Logger LOG = LoggerFactory.getLogger(GlobTrackerAnchorFunction.class);

    //public static final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();
    private String[] preStatus = new String[]{"", "", ""}; // pltmId , provId, chnlId
    private String[] curStatus = new String[]{"", "", ""};
    private String[] update = new String[]{"", "", ""};
    private String globStatusKey;
    private CMap cache;
    private HTable table;
    private String timeline;
    private String dateYYYYMMDD;   //

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        String guid = input.getStringByField("guid");
        timeline = input.getStringByField("timeline");
        String session_id = input.getStringByField("session_id");
        String dateYYYYMMDD = timeline.substring(0, 8);  //全局窗口（20150724）
        String win = timeline.substring(0, 11) + "0";    // win为时间窗口（10分钟）
        String statusKey = dateYYYYMMDD + "_" + guid + "_" + session_id; // 判断有效UV用
        globStatusKey = dateYYYYMMDD + "_" + guid; // 去重用
        boolean isInvalid = false;    // 标记该条tuple是否需要参加后续有效的uv计算
        boolean isRepeated = false;   // 标记全局是否已经存在该guid的统计key

        try {
            if (cache.get(statusKey) == null) { // 非有效
                isInvalid = true;
                cache.put(statusKey, EXIST);
            } else {
                // 有效（但需要去重）, 更新当前tuple的省份， 渠道信息，锚定该条tuple，表示要减去先前已经统计，但算错的
                if (cache.get(globStatusKey) == null) { // 全局不存在该guid
                    cache.put(globStatusKey, EXIST);
                } else {
                    isRepeated = true;
                }
            }
        } catch (Exception e) {
            LOG.error("HBaseAsyncLRU cache error", e);
            e.printStackTrace();
        }
        // 重新emit
        Values values = new Values();
        values.add(isInvalid);
        values.add(isRepeated);
        collector.emit(values);
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        // 记录流过数据信息，补充当前数据缺失信息,若此map在内存中丢失，只会影响后面统计小部分数据的准确性
        try {
            Long starts = System.currentTimeMillis();
            cache = TrackerLogicUtils.getHBaseAsyncLRU(HBaseConstant.Table.UV_CACHE_TEST_TABLE.tbName, null);
            table = HBaseFactory.getTable(HBaseConstant.Table.UV_CACHE_TEST_TABLE);
            table.setAutoFlush(true, false);
            LOG.debug("HBaseAsyncLRU cache init success and cost " + (System.currentTimeMillis() - starts));
        } catch (IOException e) {
            LOG.error("ProvDecremeter:get hbase table or cache error", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void cleanup() {
        if (table != null) {
            try {
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
