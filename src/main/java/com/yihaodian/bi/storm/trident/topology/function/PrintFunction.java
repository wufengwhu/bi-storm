/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yihaodian.bi.storm.trident.topology.function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;
import java.util.Random;

public class PrintFunction extends BaseFunction {

    private static final Logger LOG = LoggerFactory.getLogger(PrintFunction.class);

    public static final Random RANDOM = new Random();

    @Override
    public void execute(TridentTuple input, TridentCollector tridentCollector) {
        if(RANDOM.nextInt(1000) > 995) {
            System.out.println(input.toString());
        }
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        // 记录流过的数据信息，补充当前数据缺失信息
//        JedisPool jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
//                poolConfig.getHost(),
//                poolConfig.getPort(),
//                poolConfig.getTimeout(),
//                poolConfig.getPassword(),
//                poolConfig.getDatabase());
//
//        jedis = jedisPool.getResource();
        System.out.println("hbase result prepare++++++++++++++");

        //redisProxy = YCacheCMapFactory.getRedisCache();
    }

    @Override
    public void cleanup() {
//        if (jedis != null){
//            jedis.close();
//        }
    }
}
