package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import com.yihaodian.bi.storm.trident.hbase.common.Utils;
import org.apache.hadoop.hbase.client.HTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.io.IOException;
import java.util.Map;

/**
 * @author fengwu on 15/7/29
 */
public class H5PCAnchor extends BaseFunction {

    private static Logger LOG = LoggerFactory.getLogger(H5PCAnchor.class);

//    public static final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();

//    private JedisPoolConfig poolConfig;
//
//    private Jedis jedis;

    private HTable table;
    private CMap cache;



    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        String guid = input.getStringByField("guid");
        int platformId = input.getIntegerByField("platformId");
        String timeline = input.getStringByField("timeline");
        String end_user_id = input.getStringByField("end_user_id");


        String glob_h5pc_key = "H5PC_" + guid + "_" + timeline.substring(0, 8); // H5，PC 的全局key 是 H5PC_guid_20150724
        String notVipKey = "user_" + platformId + "_0_" + timeline.substring(0, 11) + "0"; // 非会员统计key
        String vipKey = "user_" + platformId + "_1_" + timeline.substring(0, 11) + "0"; // 会员统计
        String h5pc_state_key = "";

        String prev_uid = null;
        try {
            prev_uid = cache.get(glob_h5pc_key);
            if (prev_uid == null) {
                cache.put(glob_h5pc_key, end_user_id);
                if (end_user_id.isEmpty()) {
                    // 统计非会员
                    h5pc_state_key = notVipKey;
                } else {
                    // 统计会员
                    h5pc_state_key = vipKey;
                }
            } else {  // prev_uid != null
                if (prev_uid.isEmpty() && !end_user_id.isEmpty()) {
                    cache.put(glob_h5pc_key, end_user_id);
                    // 先前按非会员统计的，现在发现是会员，需要重新更新已统计的结果
                    try {
                        Utils.decrement(table, notVipKey);    // 需要更改已经统计的非会员流量
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error("H5PCAnchor:decrement from hbase table error with rowkey=" + notVipKey, e);
                    }
                    h5pc_state_key = vipKey;  // 会员流量
                } else { // 测试
                    //h5pc_state_key = "repeated_" + platformId + "_1_" + timeline.substring(0, 11) + "0";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Values values = new Values();
        values.add(h5pc_state_key);
        collector.emit(values);
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        // 记录流过的数据信息，补充当前数据缺失信息
        try {
            cache = TrackerLogicUtils.getNeedleHBaseAsyncLRU();
            table = HBaseFactory.getTable(HBaseConstant.Table.UV_CACHE_TEST_TABLE);
            table.setAutoFlush(true, false);
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("H5PCAnchor:get hbase table or cache error", e);
        }
    }

    @Override
    public void cleanup() {
        try {
            table.close();
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("H5PCAnchor:close hbase table error", e);
        }
    }

//    private void updateCacheH5PC(TrackerLogicUtils.PlatformType pt) throws Exception {
//        String usr_id = t.getEnd_user_id();
//        if (cache.get(userStatusKey()) == null) { // guid 不存在
//
//            if (usr_id != null && !usr_id.isEmpty()) { // 当前track已登陆
//                cache.put(userStatusKey(), pt.getName() + "@1"); // user_timestamp_guid => pltfmId
//                if (cache.get(vipStatusKey(pt.getLiteral(), usr_id)) == null) { // 该用户不存在
//                    cache.put(vipStatusKey(pt.getLiteral(), usr_id), EXIST);
//                    if (pt == TrackerLogicUtils.PlatformType.H5 || pt == TrackerLogicUtils.PlatformType.PC) {
//                        cache.incr(userKey(pt.getName(), TrackerLogicUtils.VipType.VIP.getType(), dateYYYYMMDDHHM0), 1);
//                    }
//
//                }
//            } else { // 未登陆
//                cache.put(userStatusKey(), pt.getName() + "@0");
//                cache.incr(userKey(pt.getName(), TrackerLogicUtils.VipType.NON_VIP.getType(), dateYYYYMMDDHHM0), 1);
//            }
//        } else {
//            if (usr_id != null && !usr_id.isEmpty()) { // guid存在 当前已登陆
//                if (cache.get(vipStatusKey(TrackerLogicUtils.PlatformType.H5.getLiteral(), usr_id)) == null
//                        && cache.get(vipStatusKey(TrackerLogicUtils.PlatformType.PC.getLiteral(), usr_id)) == null
//                        && cache.get(vipStatusKey(TrackerLogicUtils.PlatformType.APP.getLiteral(), usr_id)) == null) { // 用户不存在于任何平台
//                    String[] tk = cache.get(userStatusKey()).split("@");
//                    String prePt = tk[0];
//                    String preLg = tk[1];
//                    cache.put(vipStatusKey(prePt, usr_id), EXIST);
//                    if ("0".equals(preLg)
//                            && (TrackerLogicUtils.PlatformType.H5.getName().equals(prePt) || TrackerLogicUtils.PlatformType.PC.getName().equals(prePt))) {
//                        cache.incr(userKey(prePt, TrackerLogicUtils.VipType.VIP.getType(), dateYYYYMMDDHHM0), 1);
//                        cache.incr(userKey(prePt, TrackerLogicUtils.VipType.NON_VIP.getType(), dateYYYYMMDDHHM0), -1);
//
//                        cache.put(userStatusKey(), prePt + "@1");
//                    }
//                }
//            }
//        }
//    }



}
