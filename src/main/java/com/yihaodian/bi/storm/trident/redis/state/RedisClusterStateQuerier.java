/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yihaodian.bi.storm.trident.redis.state;

import com.yihaodian.bi.storm.trident.redis.mapper.RedisLookupMapper;
import com.yihaodian.common.yredis.RedisProxy;

import java.util.ArrayList;
import java.util.List;

/**
 * BaseQueryFunction implementation for Redis Cluster environment.
 *
 * @see AbstractRedisStateQuerier
 */
public class RedisClusterStateQuerier extends AbstractRedisStateQuerier<RedisClusterState> {
    /**
     * Constructor
     *
     * @param lookupMapper mapper for querying
     */
    public RedisClusterStateQuerier(RedisLookupMapper lookupMapper) {
        super(lookupMapper);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<String> retrieveValuesFromRedis(RedisClusterState state, List<String> keys) {
        RedisProxy jedisClusterProxy = null;
        try {
            jedisClusterProxy = state.getJedisClusterProxy();
            List<String> redisVals = new ArrayList<String>();

            for (String key : keys) {
                switch (dataType) {
                case STRING:
                    redisVals.add(jedisClusterProxy.get(key));
                    break;
                case HASH:
                    redisVals.add(jedisClusterProxy.hget(additionalKey, key));
                    break;
                default:
                    throw new IllegalArgumentException("Cannot process such data type: " + dataType);
                }
            }

            return redisVals;
        } finally {
            if (jedisClusterProxy != null) {
                //state.returnJedisCluster(jedisCluster);
            }
        }
    }
}
