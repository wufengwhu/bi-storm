package com.yihaodian.bi.storm.trident.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.Scheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.kafka.consumer.OfflineConsumer;
import com.yihaodian.bi.storm.TopoGroup;
import com.yihaodian.bi.storm.common.util.Constant;
import com.yihaodian.bi.storm.logic.tracker.spout.KafkaTrackerSpout;
import com.yihaodian.bi.storm.trident.redis.common.JedisPoolConfig;
import com.yihaodian.bi.storm.trident.redis.state.KeyFactory;
import com.yihaodian.bi.storm.trident.redis.state.Options;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria;
import com.yihaodian.bi.storm.trident.hbase.mapper.*;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseMapState;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseState;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseStateFactory;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseUpdater;
import com.yihaodian.bi.storm.trident.redis.mapper.RedisDataTypeDescription;
import com.yihaodian.bi.storm.trident.redis.state.RedisMapState;
import com.yihaodian.bi.storm.trident.topology.aggregator.FunnelStatAgg;
import com.yihaodian.bi.storm.trident.topology.filter.*;
import com.yihaodian.bi.storm.trident.topology.function.*;
import kafka.api.OffsetRequest;
import org.apache.hadoop.hbase.client.Durability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;
import storm.kafka.trident.OpaqueTridentKafkaSpout;
import storm.kafka.trident.TridentKafkaConfig;
import storm.trident.Stream;
import storm.trident.TridentTopology;
import storm.trident.operation.builtin.Count;
import storm.trident.operation.builtin.Sum;
import storm.trident.state.StateFactory;
import storm.trident.testing.FixedBatchSpout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fengwu on 15/7/22
 */
public class TridentUVMultiTopology {

    private static Logger LOG = LoggerFactory.getLogger(TridentUVMultiTopology.class);

    /**
     * 测试spout
     *
     * @return
     */
    private static FixedBatchSpout getTestBatchSpout() {
        Fields fields = new Fields("trackerEvent");
        List<List<Object>> output = new ArrayList<List<Object>>();
        OfflineConsumer consumer = new OfflineConsumer("sample.dat.zip");
        int count = 0;
        while (consumer.hasNext()) {
            count++;
            String str = consumer.next();
            TrackerVO trackerVo = new TrackerVO(str.split("\\t"));
//            //System.out.println(trackerVo.toString());
//            trackerMsg.add(trackerVo);
//            trackerMsg.add(Constant.SEO);
//            int platformId = TrackerLogicUtils.getPlatformType(trackerVo).getType();
//            //System.out.println("platformId:"+platformId);
//            trackerMsg.add(platformId);
//            trackerMsg.add(trackerVo.getGu_id());
//            trackerMsg.add(trackerVo.getSession_id());
//            String timeline = trackerVo.getTrack_time().replaceAll("[-: ]", "");
//            trackerMsg.add(timeline);
//            trackerMsg.add(TrackerLogicUtils.provId(trackerVo));
//            trackerMsg.add(trackerVo.getEnd_user_id());
            output.add(Arrays.asList(new Object[]{trackerVo}));
            if (count == 100) {
                break;
            }
        }

        List<Object>[] outputArray = new List[output.size()];
        for (int i = 0; i < output.size(); i++) {
            outputArray[i] = output.get(i);
        }

        FixedBatchSpout fixedBatchSpout = new FixedBatchSpout(fields, 100, outputArray);
        fixedBatchSpout.setCycle(false);
        return fixedBatchSpout;
    }

    private static OpaqueTridentKafkaSpout buildOpaqueTridentKafkaSpout(String clienId) {
        // opaque kafka spout (support replay data from the last read position, it is optional)
        BrokerHosts brokerHosts = new ZkHosts(Constant.KAFKA_ZK_CONNECTION);
        TridentKafkaConfig kafkaConfig = new TridentKafkaConfig(brokerHosts, Constant.KAFKA_CONSUMER_NAME_TRACKER, clienId);

        kafkaConfig.scheme = new SchemeAsMultiScheme(new TrackerMessageScheme());
        kafkaConfig.startOffsetTime = OffsetRequest.LatestTime();
        return new OpaqueTridentKafkaSpout(kafkaConfig);
    }

    private static KafkaSpout buildKafkaSpout(String clienId) {
        BrokerHosts brokerHosts = new ZkHosts(Constant.KAFKA_ZK_CONNECTION);
        SpoutConfig config = new SpoutConfig(brokerHosts, Constant.KAFKA_CONSUMER_NAME_TRACKER, "", clienId);
        config.scheme = new SchemeAsMultiScheme(new TrackerMessageScheme());
        config.startOffsetTime = OffsetRequest.LatestTime();
        return new KafkaSpout(config);
    }

    private static Stream buildConsumerStream(TridentTopology topology, String txid, String clienId) {
        return topology.newStream(txid, buildOpaqueTridentKafkaSpout(clienId))
                .each(new Fields("rowTrackerMsgEvent"), new TrackerEventBuilder(), new Fields("trackerEvent"))
                .each(new Fields("trackerEvent"), new GlobUVExtractFunction(),
                        new Fields("channel", "platformId", "guid", "session_id", "timeline", "provId", "end_user_id"));
    }

    private static StateFactory buildOpaqueStatFactory(JedisPoolConfig poolConfig) {
        // String jedisConfigurePath = "redis.xml";
        //RedisProxy redisProxy = getJedisClusterProxy(jedisConfigurePath);
        Options opts = new Options();
        opts.dataTypeDescription = new RedisDataTypeDescription(RedisDataTypeDescription.RedisDataType.STRING);
        opts.expireIntervalSec = 43200;
        StateFactory opaqueStatFactory = RedisMapState.opaque(poolConfig, opts);
        return opaqueStatFactory;

        //return RedisClusterMapState.opaque(YCacheCMapFactory.getRedisCache(), opts);
    }

    private static StateFactory buildOpaqueStatFactory() {
        TridentHBaseMapMapper tridentHBaseMapMapper = new SimpleTridentHBaseMapMapper("value");
        HBaseValueMapper rowToStormValueMapper = new SimpleHBaseValueMapper();

        HBaseProjectionCriteria projectionCriteria = new HBaseProjectionCriteria();
        projectionCriteria.addColumn(new HBaseProjectionCriteria.ColumnMetaData("cf", "value"));

        HBaseMapState.Options options = new HBaseMapState.Options()
                .withColumnFamily("cf")
                .withDurability(Durability.SKIP_WAL)
                .withMapper(tridentHBaseMapMapper)
                .withProjectionCriteria(projectionCriteria)
                .withRowToStormValueMapper(rowToStormValueMapper)
                .withTableName("bi_uv_cache_test");

        return HBaseMapState.opaque(options);
    }


    public static StormTopology buildAppUVTopology() {
        TridentTopology topology = new TridentTopology();
        //topology.newStream("app_uv_trident", getTestBatchSpout())
        // app valid uv in ten minutes slides
        topology.newStream("app_uv_trident", buildOpaqueTridentKafkaSpout("app_uv"))
                .each(new Fields("rowTrackerMsgEvent"),
                        new TrackerEventBuilder(),
                        new Fields("trackerEvent"))
                .each(new Fields("trackerEvent"), new AppTrackerAnchorFunction(),
                        new Fields("timeline", "platformId", "isInvalid"))
                .project(new Fields("timeline", "platformId", "isInvalid"))
                .each(new Fields("platformId", "isInvalid"), new AppUVFilter())
                .each(new Fields("platformId", "timeline"), new KeyFactory.AppStatKeyFactory(), new Fields("app_stat_key"))
                .groupBy(new Fields("app_stat_key"))
                .persistentAggregate(buildOpaqueStatFactory(), new Count(), new Fields("app_stat_key_counts"))
                .parallelismHint(12);
        return topology.build();
    }

    private static Stream filtrateAndSupply(Stream beaforeStream) {
        // valid uv tracker stream
        return beaforeStream.project(new Fields(
                "channel", "platformId", "guid", "session_id", "timeline", "provId"))
                .each(new Fields("channel", "platformId", "guid", "session_id", "timeline", "provId"),
                        new GlobTrackerAnchorFunction(),
                        new Fields("pre_provId", "pre_channel", "cleaned_provId",
                                "cleaned_channel", "isInvalid", "isRepeated"))
                .each(new Fields("isInvalid"), new InvalidUVFilter()).parallelismHint(6);
    }

    public static StormTopology buildH5PCTopology() {
        // h5pc uv (the tracker events doesn't need belong to the same session even they all launched by the same identity)
        TridentTopology topology = new TridentTopology();
        topology.newStream("h5pc_uv_trident", buildOpaqueTridentKafkaSpout("h5pc_uv"))
                .each(new Fields("rowTrackerMsgEvent"),
                        new TrackerEventBuilder(),
                        new Fields("trackerEvent"))
                .each(new Fields("trackerEvent"), new H5PCUVExtractFunction(),
                        new Fields("guid", "timeline", "end_user_id", "platformId"))
                .project(new Fields("guid", "timeline", "end_user_id", "platformId"))
                .each(new Fields("platformId"), new H5PCFilter())
                .each(new Fields("guid", "timeline", "end_user_id", "platformId"),
                        new H5PCAnchor(),
                        new Fields("h5pc_state_key"))
                .each(new Fields("h5pc_state_key"), new EmptyFieldFilter())
                .groupBy(new Fields("h5pc_state_key"))
                .persistentAggregate(buildOpaqueStatFactory(), new Count(), new Fields("h5pc_counts"));

        return topology.build();
    }

    public static StormTopology buildGlobUVTopology() {
        TridentTopology topology = new TridentTopology();
        //Stream validTrackerStream = topology.newStream("glob_uv_trident", getTestBatchSpout());
        // glob valid and filtrate repeated uv in ten minutes slides,
        topology.newStream("glob_uv_trident", buildOpaqueTridentKafkaSpout("glob_uv"))
                .each(new Fields("rowTrackerMsgEvent"),
                        new TrackerEventBuilder(),
                        new Fields("trackerEvent"))
                .each(new Fields("trackerEvent"),
                        new GlobUVExtractFunction(),
                        new Fields("guid", "session_id", "timeline"))
                .project(new Fields("guid", "session_id", "timeline"))
                .each(new Fields("guid", "session_id", "timeline"),
                        new GlobTrackerAnchorFunction(),
                        new Fields("isInvalid", "isRepeated"))
                .each(new Fields("isInvalid", "isRepeated"), new GlobUVFilter())
                .each(new Fields("timeline"), new KeyFactory.GlobStatKeyFactory(), new Fields("glob_valid_uv_key"))
                .groupBy(new Fields("glob_valid_uv_key"))
                .persistentAggregate(buildOpaqueStatFactory(), new Count(), new Fields("glob_valid_uv_counts"))
                .parallelismHint(12);

        return topology.build();
    }


    public static StormTopology buidFunnelTopology() {
        // hbase map stat
        TridentHBaseMapMapper tridentHBaseMapper = new SimpleTridentHBaseMapMapper("value");
                //.withColumnFamily("cf")
                //.withColumnFields(new Fields("funnel_stat_index"))
//                .withCounterFields(new Fields(""))
//                .withRowKeyField("funnel_stat_index");

        HBaseValueMapper rowToStormValueMapper = new SimpleHBaseValueMapper();

        HBaseProjectionCriteria projectionCriteria = new HBaseProjectionCriteria();
        projectionCriteria.addColumn(new HBaseProjectionCriteria.ColumnMetaData("cf", "value"));

        HBaseMapState.Options options = new HBaseMapState.Options()
                .withDurability(Durability.SKIP_WAL)
                .withMapper(tridentHBaseMapper)
                .withProjectionCriteria(projectionCriteria)
                .withRowToStormValueMapper(rowToStormValueMapper)
                .withTableName(HBaseConstant.Table.TRIDENT_FUNNEL_CACHE_TABLE.tbName);

        StateFactory factory = HBaseMapState.nonTransactional(options);

        // redis map stat
//        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder().build();
//
//        RedisDataTypeDescription dataTypeDescription = new RedisDataTypeDescription(
//                RedisDataTypeDescription.RedisDataType.STRING);
//        StateFactory factory = RedisMapState.nonTransactional(poolConfig, dataTypeDescription);

        TridentTopology topology = new TridentTopology();
        topology.newStream("funnel_uv_trident", new KafkaTrackerSpout("Funnel"))
//                .each(new Fields("rowTrackerMsgEvent"),
//                        new TrackerEventBuilder(),
//                        new Fields("trackerEvent"))
                .each(new Fields("tracktime", "guid", "sid", "pltfmId", "uid", "provId", "tracku", "referer", "url",
                                "container", "pagetypeId", "refpagetypeId", "orderCode", "posTypeId", "linkPos", "buttonPos"),
                        new FunnelAnchorFunction(),
                        new Fields("dateYYYYMMDDHH",
                                "platformCode",
                                "chnnlType",
                                "chnnlVal",
                                "pageCategId",
                                "refpageCategId",
                                "isRcmd",
                                "isAddCart",
                                "isBtnLink",
                                "isAcdvUv",
                                "isCoreUv",
                                "isOwnUv"))
                .project(new Fields("guid",
                        "dateYYYYMMDDHH",
                        "platformCode",
                        "chnnlType",
                        "chnnlVal",
                        "pageCategId",
                        "refpageCategId",
                        "isRcmd",
                        "isAddCart",
                        "isBtnLink",
                        "isAcdvUv",
                        "isCoreUv",
                        "isOwnUv",
                        "orderCode"))
                .aggregate(new Fields("guid",
                        "dateYYYYMMDDHH",
                        "platformCode",
                        "chnnlType",
                        "chnnlVal",
                        "pageCategId",
                        "refpageCategId",
                        "isRcmd",
                        "isAddCart",
                        "isBtnLink",
                        "isAcdvUv",
                        "isCoreUv",
                        "isOwnUv",
                        "orderCode"),new FunnelStatAgg(), new Fields("funnel_stat_index", "count"))
                .groupBy(new Fields("funnel_stat_index"))
                .persistentAggregate(factory, new Fields("count"), new Sum(), new Fields("sum"))
                .parallelismHint(12);

        return topology.build();
    }

    public static StormTopology buildProvUVTopology() {
        TridentTopology topology = new TridentTopology();
        String spoutId = "myProvUVTridentSpout";
        //Stream validTrackerStream = topology.newStream("prov_uv_trident", getTestBatchSpout());
        Stream validTrackerStream = buildConsumerStream(topology, "prov_uv_trident", "prov_uv");
        Stream afterStream = filtrateAndSupply(validTrackerStream);

        // province valid uv in ten minutes slides, include modify previous uv statistics which field "ext_field2" is null
        afterStream.each(new Fields("isRepeated", "pre_provId", "cleaned_provId", "timeline"),
                new ProvDecremeter(), new Fields("prov_stat_key"))
                .groupBy(new Fields("prov_stat_key"))
                .persistentAggregate(buildOpaqueStatFactory(), new Count(), new Fields("prov_stat_key_counts"));

        return topology.build();
    }

    public static StormTopology buildChannelUVTopology() {
        TridentTopology topology = new TridentTopology();
        String spoutId = "myChannelUVTridentSpout";
        //Stream validTrackerStream = topology.newStream("channel_uv_trident", getTestBatchSpout());
        Stream validTrackerStream = buildConsumerStream(topology, "channel_uv_trident", "channel_uv");
        Stream afterStream = filtrateAndSupply(validTrackerStream);

        // channel valid uv in ten minutes slides, include modify previous uv statistics which field "channel" is null
        afterStream.each(new Fields("isRepeated", "pre_channel", "cleaned_channel", "timeline"),
                new ChannelDecremeter(), new Fields("chnl_stat_key"))
                .groupBy(new Fields("chnl_stat_key"))
                .persistentAggregate(buildOpaqueStatFactory(), new Count(), new Fields(""));

        return topology.build();
    }

    public static void main(String[] args) throws Exception {
        Config conf = new Config();
//        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder().setHost("10.161.144.56")
//                .setPort(16449).build();
        //JedisPoolConfig poolConfig = new JedisPoolConfig.Builder().build();

        if (args.length == 1) {
            //conf.setMaxSpoutPending(100);
            conf.setNumWorkers(3);
            Debug.DEBUG = true;
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("uvCounter", conf, buildTopology(args[0]));
            System.out.println("submit topology success");
            Thread.sleep(60 * 1000);
            cluster.killTopology("uvCounter");
            System.out.println("kill topology success");
            cluster.shutdown();
            System.out.println("shut down storm success");
            System.exit(0);
        } else if (args.length == 2) {
            conf.setNumWorkers(12);
            StormSubmitter.submitTopology(args[0], conf, buildTopology(args[1]));
        } else {
            System.out.println("Usage: TridentUVTopology <topology name> [stat index]");
        }
    }

    private static StormTopology buildTopology(String statIndexName) throws IllegalArgumentException {
        if (statIndexName.equals("h5pc")) {
            return buildH5PCTopology();
        } else if (statIndexName.equals("app")) {
            return buildAppUVTopology();
        } else if (statIndexName.equals("glob")) {
            return buildGlobUVTopology();
        } else if (statIndexName.equals("channel")) {
            return buildChannelUVTopology();
        } else if (statIndexName.equals("prov")) {
            return buildProvUVTopology();
        } else if (statIndexName.equals("funnel")) {
            return buidFunnelTopology();
        } else {
            throw new IllegalArgumentException("can't support stat index");
        }
    }

    private static class TrackerMessageScheme implements Scheme {
        public List<Object> deserialize(byte[] bytes) {
            try {
                String msg = new String(bytes, "UTF-8");
                return new Values(msg);
            } catch (UnsupportedEncodingException e) {
                LOG.error("can't deserialize bytes from kafka");
                e.printStackTrace();
            }
            //TODO: what happend if returns null?
            return null;
        }

        public Fields getOutputFields() {
            return new Fields("rowTrackerMsgEvent");
        }
    }
}
