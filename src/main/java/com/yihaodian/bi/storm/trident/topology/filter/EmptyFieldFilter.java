package com.yihaodian.bi.storm.trident.topology.filter;

import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/8/7
 */
public class EmptyFieldFilter implements Filter {

    @Override
    public boolean isKeep(TridentTuple input) {
        String stat_key = input.getString(0);
        return !stat_key.isEmpty();
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
