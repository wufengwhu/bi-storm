package com.yihaodian.bi.storm.trident.topology.filter;

import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/7/30
 */
public class AppUVFilter implements Filter {
    private static Logger LOG = LoggerFactory.getLogger(AppUVFilter.class);
    @Override
    public boolean isKeep(TridentTuple input) {
        int platformId = input.getIntegerByField("platformId");
        boolean isInvalid = input.getBooleanByField("isInvalid");
        return  TrackerLogicUtils.PlatformType.APP.getType() == platformId && !isInvalid;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
