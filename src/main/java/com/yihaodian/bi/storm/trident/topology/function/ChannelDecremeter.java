package com.yihaodian.bi.storm.trident.topology.function;

import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.storm.common.util.Constant;
import com.yihaodian.bi.storm.trident.hbase.common.Utils;
import org.apache.hadoop.hbase.client.HTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.io.IOException;
import java.util.Map;

/**
 * @author fengwu on 15/8/3
 */
public class ChannelDecremeter implements Function {

    private static Logger LOG = LoggerFactory.getLogger(ChannelDecremeter.class);

    private HTable table;

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        boolean isRepeated = input.getBoolean(0);
        // TODO 渠道处理
        String pre_channel = input.getStringByField("pre_channel");
        String cleaned_channel = input.getStringByField("cleaned_channel");
        String timeline = input.getStringByField("timeline");

        if ((pre_channel.equals(Constant.UNKNOWN) || pre_channel.equals(Constant.DIRECT))
                && (!cleaned_channel.equals(Constant.UNKNOWN) && !cleaned_channel.equals(Constant.DIRECT))) {
            try {
                Utils.decrement(table, "chnl_" + pre_channel + "_" + timeline.substring(0, 11) + "0");
            } catch (Exception e) {
                LOG.info("ChannelDecremeter:decrement from hbase error", e);
                e.printStackTrace();
            }
        }

        String chnl_stat_key = "chnl_" + cleaned_channel + "_" + timeline.substring(0, 11) + "0";
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        try {
            table = HBaseFactory.getTable(HBaseConstant.Table.UV_CACHE_TEST_TABLE);
            table.setAutoFlush(true, false);
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("ProvDecremeter:get hbase table or cache error", e);
        }
    }

    @Override
    public void cleanup() {
        if (table != null) {
            try {
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
