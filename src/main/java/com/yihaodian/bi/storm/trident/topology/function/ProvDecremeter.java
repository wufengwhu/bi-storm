package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.storm.trident.hbase.common.Utils;
import org.apache.hadoop.hbase.client.HTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.io.IOException;
import java.util.Map;

/**
 * @author fengwu on 15/8/3
 */
public class ProvDecremeter implements Function {

    private static Logger LOG = LoggerFactory.getLogger(ProvDecremeter.class);

    private HTable table;

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        try {
            table = HBaseFactory.getTable(HBaseConstant.Table.UV_CACHE_TEST_TABLE);
            table.setAutoFlush(true, false);
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("ProvDecremeter:get hbase table or cache error", e);
        }
    }

    @Override
    public void cleanup() {
        if (table != null) {
            try {
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        boolean isRepeated = input.getBoolean(0);
        String pre_provId = input.getStringByField("pre_provId");
        String cleaned_provId = input.getStringByField("cleaned_provId");
        String timeline = input.getStringByField("timeline");
        if (isRepeated && pre_provId.isEmpty() && !cleaned_provId.isEmpty()) {
            try {
                Utils.decrement(table, "prov_" + pre_provId + "_" + timeline.substring(0, 11) + "0");
            } catch (Exception e) {
                LOG.info("ProvDecremeter:decrement from hbase error", e);
                e.printStackTrace();
            }
        }
        String prov_stat_key = "prov_" + cleaned_provId + "_" + timeline.substring(0, 11) + "0";

        collector.emit(new Values(prov_stat_key));
    }
}
