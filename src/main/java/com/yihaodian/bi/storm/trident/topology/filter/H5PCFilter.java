package com.yihaodian.bi.storm.trident.topology.filter;

import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;
import static com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.PlatformType.*;

/**
 * @author fengwu on 15/7/29
 */
public class H5PCFilter implements Filter {

    @Override
    public boolean isKeep(TridentTuple input) {
        int platformId = input.getIntegerByField("platformId");
        return H5.getType() == platformId || PC.getType() == platformId;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
