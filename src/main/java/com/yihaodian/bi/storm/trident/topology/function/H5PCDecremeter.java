package com.yihaodian.bi.storm.trident.topology.function;

import com.yihaodian.bi.storm.trident.redis.common.JedisPoolConfig;
import com.yihaodian.common.yredis.RedisProxy;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/7/30
 */
public class H5PCDecremeter implements Function {

    public static final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();

    private JedisPoolConfig jedisPoolConfig;

    public H5PCDecremeter(JedisPoolConfig jedisPoolConfig) {
        this.jedisPoolConfig = jedisPoolConfig;
    }

    public H5PCDecremeter() {
    }

    private RedisProxy jedisClusterProxy;

    private Jedis jedis;

    public H5PCDecremeter(RedisProxy jedisClusterProxy) {
        this.jedisClusterProxy = jedisClusterProxy;
    }


    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        String h5pc_needDecrement4NotVip = input.getStringByField("h5pc_needDecrement4NotVip");
        String h5pc_state_key = input.getStringByField("h5pc_state_key");
        System.out.println("******");
//        System.out.println("h5pc_state_key is empty: " + h5pc_state_key.isEmpty());
        if (!h5pc_needDecrement4NotVip.isEmpty()) {
            System.out.println("************");

        }
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        JedisPool jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
                jedisPoolConfig.getHost(),
                jedisPoolConfig.getPort(),
                jedisPoolConfig.getTimeout(),
                jedisPoolConfig.getPassword(),
                jedisPoolConfig.getDatabase());

        jedis = jedisPool.getResource();
    }

    @Override
    public void cleanup() {
//        if (jedis != null) {
//            jedis.close();
//        }
    }
}
