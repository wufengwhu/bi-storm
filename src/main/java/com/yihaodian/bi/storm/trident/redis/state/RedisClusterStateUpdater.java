/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yihaodian.bi.storm.trident.redis.state;

import com.yihaodian.bi.storm.trident.redis.mapper.RedisDataTypeDescription;
import com.yihaodian.bi.storm.trident.redis.mapper.RedisStoreMapper;
import com.yihaodian.common.yredis.RedisProxy;

import java.util.Map;

/**
 * BaseStateUpdater implementation for Redis Cluster environment.
 *
 * @see AbstractRedisStateUpdater
 */
public class RedisClusterStateUpdater extends AbstractRedisStateUpdater<RedisClusterState> {
    /**
     * Constructor
     *
     * @param storeMapper mapper for storing
     */
    public RedisClusterStateUpdater(RedisStoreMapper storeMapper) {
        super(storeMapper);
    }

    /**
     * Sets expire (time to live) if needed.
     *
     * @param expireIntervalSec time to live in seconds
     * @return RedisClusterStateUpdater itself
     */
    public RedisClusterStateUpdater withExpire(int expireIntervalSec) {
        setExpireInterval(expireIntervalSec);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateStatesToRedis(RedisClusterState redisClusterState, Map<String, String> keyToValue) {
        RedisProxy jedisClusterProxy = null;
        try {
            jedisClusterProxy = redisClusterState.getJedisClusterProxy();

            for (Map.Entry<String, String> kvEntry : keyToValue.entrySet()) {
                String key = kvEntry.getKey();
                String value = kvEntry.getValue();

                switch (dataType) {
                case STRING:
//                    if (this.expireIntervalSec > 0) {
//                        jedisClusterProxy.setex(key, expireIntervalSec, value);
//                    } else {
//                        jedisClusterProxy.set(key, value);
//                    }
                    jedisClusterProxy.incrBy(key, Integer.valueOf(value));
                    jedisClusterProxy.expire(key,expireIntervalSec);
                    break;
                case HASH:
                    jedisClusterProxy.hincrBy(additionalKey, key, Integer.valueOf(value));
                    break;
                default:
                    throw new IllegalArgumentException("Cannot process such data type: " + dataType);
                }
            }

            // send expire command for hash only once
            // it expires key itself entirely, so use it with caution
            if (dataType == RedisDataTypeDescription.RedisDataType.HASH &&
                    this.expireIntervalSec > 0) {
                jedisClusterProxy.expire(additionalKey, expireIntervalSec);
            }
        } finally {
            // 释放redis资源
            if (jedisClusterProxy != null) {  // ycache 自动释放？
                //redisClusterState.returnJedisCluster(jedisCluster);
            }
        }
    }
}
