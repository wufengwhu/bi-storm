package com.yihaodian.bi.storm.trident.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria;
import com.yihaodian.bi.storm.trident.hbase.mapper.HBaseValueMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.SimpleTridentHBaseMapMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.TridentHBaseMapMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.SimpleHBaseValueMapper;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseMapState;
import com.yihaodian.bi.storm.trident.topology.function.PrintFunction;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import storm.trident.Stream;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.builtin.MapGet;
import storm.trident.operation.builtin.Sum;
import storm.trident.state.StateFactory;
import storm.trident.testing.FixedBatchSpout;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengwu on 15/8/13
 */
public class WordCountTridentHbaseMap {

    public static StormTopology buildTopology() {
        Fields fields = new Fields("word", "count");
        FixedBatchSpout spout = new FixedBatchSpout(fields, 100,
                new Values("storm", 1),
                new Values("trident", 1),
                new Values("needs", 1),
                new Values("javadoc", 1)
        );
        spout.setCycle(false);

        TridentHBaseMapMapper tridentHBaseMapMapper = new SimpleTridentHBaseMapMapper("value");

        HBaseValueMapper rowToStormValueMapper = new SimpleHBaseValueMapper();

        HBaseProjectionCriteria projectionCriteria = new HBaseProjectionCriteria();
        projectionCriteria.addColumn(new HBaseProjectionCriteria.ColumnMetaData("cf", "value"));

        HBaseMapState.Options options = new HBaseMapState.Options()
                .withColumnFamily("cf")
                .withDurability(Durability.SYNC_WAL)
                .withMapper(tridentHBaseMapMapper)
                .withProjectionCriteria(projectionCriteria)
                .withRowToStormValueMapper(rowToStormValueMapper)
                .withTableName("word_count");

        StateFactory stateFactory = HBaseMapState.opaque(options);

        TridentTopology topology = new TridentTopology();
        Stream stream = topology.newStream("spout1", spout);

        TridentState state = stream.groupBy(new Fields("word"))
                .persistentAggregate(stateFactory, new Fields("count"), new Sum(), new Fields("sum"));

        stream.stateQuery(state, new Fields("word"), new MapGet(), new Fields("sum"))
                .each(new Fields("word", "sum"), new PrintFunction(), new Fields());

        return topology.build();

    }

    public static void main(String[] args) throws Exception {

//        Config conf = new Config();
//        //conf.setMaxSpoutPending(5);
//        //setLocalConfiguration(conf, args[0]);
//        if (args.length == 0) {
//            LocalCluster cluster = new LocalCluster();
//            cluster.submitTopology("wordCounter", conf, buildTopology());
//            Thread.sleep(60 * 1000);
//            cluster.killTopology("wordCounter");
//            cluster.shutdown();
//            System.exit(0);
//        } else if (args.length == 1) {
//            conf.setNumWorkers(3);
//            StormSubmitter.submitTopology(args[0], conf, buildTopology());
//        } else {
//            System.out.println("Usage: WordCountTridentHbaseMap <hdfs url> [topology name]");
//        }

        Configuration config = HBaseConfiguration.create();
        if(args.length > 0){
            config.set("hbase.rootdir", args[0]);
        }

        HTable table = new HTable(config, "word_count");
        List<Put> puts = new ArrayList<Put>();

        for (String word : WordSpout.words) {
            // put
            Put put = new Put(Bytes.toBytes(word));
            put.add(Bytes.toBytes("cf"), Bytes.toBytes("count"),Bytes.toBytes(1L));
            puts.add(put);
        }
        table.put(puts);

        for (String word : WordSpout.words) {
            Get get = new Get(Bytes.toBytes(word));
            Result result = table.get(get);

            byte[] countBytes = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("count"));
            byte[] wordBytes = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("word"));

            String wordStr = Bytes.toString(wordBytes);
            System.out.println(wordStr);
            long count = Bytes.toLong(countBytes);
            System.out.println("Word: '" + wordStr + "', Count: " + count);
        }

    }
}
