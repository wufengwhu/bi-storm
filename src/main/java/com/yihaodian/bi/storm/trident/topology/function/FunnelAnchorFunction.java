package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.topology.FailedException;
import backtype.storm.tuple.Values;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage._;
import static com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.initFunnelCache;

/**
 * Created by fengwu on 15/9/11.
 */
public class FunnelAnchorFunction extends BaseStat implements Function {

    private static Logger LOG = LoggerFactory.getLogger(FunnelAnchorFunction.class);

    private AbstractBaseCMap pageCategCache;

    private TrackerVO t;

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        try {
            //TrackerVO t = (TrackerVO) input.getValueByField("trackerEvent");
            String guid = input.getStringByField("guid");
            t.setSession_id(input.getStringByField("sid"));
            t.setPlatform(input.getStringByField("pltfmId"));
            t.setExt_field2(input.getStringByField("provId"));
            t.setTrack_time(input.getStringByField("tracktime"));
            t.setTracker_u(input.getStringByField("tracku"));
            t.setReferer(input.getStringByField("referer"));
            t.setEnd_user_id(input.getStringByField("uid"));
            t.setUrl(input.getStringByField("url"));
            t.setContainer(input.getStringByField("container"));
            t.setPagetypeid(input.getStringByField("pagetypeId"));
            t.setRefpagetypeid(input.getStringByField("refpagetypeId"));
            t.setLink_position(input.getStringByField("linkPos"));
            t.setButton_position(input.getStringByField("buttonPos"));
            t.setPositiontypeid(input.getStringByField("posTypeId"));

            String pagetypeId = t.getPagetypeid();

            TrackerLogicUtils.PlatformType pltm = TrackerLogicUtils.getPlatformType(t);
            TrackerLogicUtils.ChannelResult chnl = TrackerLogicUtils.channelFunnel(t);

            String refpagetypeId = t.getRefpagetypeid();
            String pageCategId = pageCategCache.get(pagetypeId);
            String refpageCategId = pageCategCache.get(refpagetypeId);

            boolean isRcmd = TrackerLogicUtils.isRcmdUv(t);

            String timestamp = t.getTrack_time().replaceAll("[-: ]", "");
            String dateYYYYMMDDHH = timestamp.substring(0, 10); // 全局窗口（2015072410

            boolean isAddCart = TrackerLogicUtils.isAddCart(t);
            boolean isBtnLink = TrackerLogicUtils.isBtnLink(t.getButton_position());

            boolean isAcdvUv = TrackerLogicUtils.isAcdvUv(chnl.getVal());
            boolean isCoreUv = TrackerLogicUtils.isCoreUv(chnl.getVal());
            boolean isOwnUv = TrackerLogicUtils.isOwnUv(chnl.getVal());

            Values values = new Values(
                    dateYYYYMMDDHH,
                    pltm.getName(),
                    chnl.getType(),
                    chnl.getVal(),
                    pageCategId,
                    refpageCategId,
                    isRcmd,
                    isAddCart,
                    isBtnLink,
                    isAcdvUv,
                    isCoreUv,
                    isOwnUv);
            collector.emit(values);
        }catch (Exception e){
            LOG.error("FunnelAnchorFunction:execute exception ", e);
            collector.reportError(e);
            throw new FailedException(e);
        }
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        try{
            t = new TrackerVO();
            pageCategCache = TrackerLogicUtils.getPageTypeToCategHBaseLRUReadonly();
            initFunnelCache();
        }catch (Exception e){
            LOG.error("FunnelAnchorFunction:prepare exception ", e);
            throw new FailedException(e);
        }
    }

    @Override
    public void cleanup() {

    }
}
