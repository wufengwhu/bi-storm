package com.yihaodian.bi.storm.trident.topology.aggregator;

import backtype.storm.topology.FailedException;
import backtype.storm.tuple.Values;
import com.google.common.base.Strings;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.yihaodian.bi.storm.logic.tracker.FunnelPackage.*;
import static com.yihaodian.bi.storm.logic.tracker.FunnelPackage.CART_;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.EXIST;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.USER_;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage._;

/**
 * Created by fengwu on 15/9/11.
 */
public class FunnelStatAgg extends BaseAggregator<ConcurrentHashMap<String, Long>> {

    private static Logger LOG = LoggerFactory.getLogger(FunnelStatAgg.class);

    private static AbstractBaseCMap funnelCache;

    private String dateYYYYMMDDHH;

    private String guid;

    private String acdvUvKey;

    private String acdvUvStatKey;

    private String coreUvKey;

    private String coreUvStatKey;

    private String ownUvKey;

    private String ownUvStatKey;

    private String rcmdUvKey;

    private String rcmdUvStatKey;

    private String detlUvKey;

    private String detlUvStatKey;

    private String cmsUvKey;

    private String cmsUvStatKey;

    private String kwUvKey;

    private String kwUvStatKey;

    private String categUvKey;

    private String categStatKey;

    private String chnlUvKey;

    private String chnlStatKey;

    private String cmsDetlUvKey;

    private String cmsDetlStatKey;

    private String kwDetlUvKey;

    private String kwDetlStatKey;

    private String categDetlUvKey;

    private String categDetlStatKey;

    private String chnlDetlUvKey;

    private String chnlDetlStatKey;

    private String orderStatusKey;

    private boolean isRcmd;
    private String rcmdDetlUvKey;
    private String rcmdDetlStatKey;
    private String landStatKey;

    private String globStatusKey;
    private boolean isAddCart;
    private String pageCategId;
    private String refpageCategId;
    private String cartUvKey;
    private String cartStatKey;
    private Boolean isBtnLink;
    private String platformCode;
    private Boolean isAcdvUv;
    private boolean isCoreUv;
    private Boolean isOwnUv;
    private String orderStatKey;
    private int chnnlType;
    private String chnnlVal;

    @Override
    public ConcurrentHashMap<String, Long> init(Object o, TridentCollector tridentCollector) {
        return new ConcurrentHashMap<String, Long>();
    }

    private void clear() {
        cartUvKey = null;
        cartStatKey = null;
        orderStatKey = null;
        detlUvKey = null;
        detlUvStatKey = null;
        cmsUvKey = null;
        cmsUvStatKey = null;
        kwUvKey = null;
        kwUvStatKey = null;
        categUvKey = null;
        categStatKey = null;
        chnlUvKey = null;
        chnlStatKey = null;
        cmsDetlUvKey = null;
        cmsDetlStatKey = null;
        kwDetlUvKey = null;
        kwDetlStatKey = null;
        categDetlUvKey = null;
        categDetlStatKey = null;
        chnlDetlUvKey = null;
        chnlDetlStatKey = null;
        orderStatusKey = null;
        rcmdDetlUvKey = null;
        rcmdDetlStatKey = null;
    }

    private void initTridentTuple(TridentTuple input) {
        clear();
        guid = input.getStringByField("guid");
        isRcmd = input.getBooleanByField("isRcmd");
        isAddCart = input.getBooleanByField("isAddCart");
        isAcdvUv = input.getBooleanByField("isAcdvUv");
        isCoreUv = input.getBooleanByField("isCoreUv");
        isOwnUv = input.getBooleanByField("isOwnUv");
        isBtnLink = input.getBooleanByField("isBtnLink");
        dateYYYYMMDDHH = input.getStringByField("dateYYYYMMDDHH"); // 全局窗口（2015072410）
        pageCategId = input.getStringByField("pageCategId");
        refpageCategId = input.getStringByField("refpageCategId");
        platformCode = input.getStringByField("platformCode");
        chnnlType = input.getIntegerByField("chnnlType");
        chnnlVal = input.getStringByField("chnnlVal");

        landStatKey = LAND_ + dateYYYYMMDDHH; // 全站landing page统计 land_yyyyMMddHHM0
        globStatusKey = dateYYYYMMDDHH + "_" + guid; // 去重用 yyyyMMdd_guid
        acdvUvKey = ACDV_ + globStatusKey;
        coreUvKey = CORE_ + globStatusKey;
        ownUvKey = OWN_ + globStatusKey;
        rcmdUvKey = RCMD_ + globStatusKey;

        acdvUvStatKey = ACDV_ + dateYYYYMMDDHH;
        coreUvStatKey = CORE_ + dateYYYYMMDDHH;
        ownUvStatKey = OWN_ + dateYYYYMMDDHH;
        rcmdUvStatKey = RCMD_ + dateYYYYMMDDHH;

        if (!Strings.isNullOrEmpty(pageCategId)) {
            if (pageCategId.equals("1")) {
                cmsUvKey = CMS_ + globStatusKey;
                cmsUvStatKey = CMS_ + dateYYYYMMDDHH;
                // 点击加入购物车了
                if (isAddCart) {
                    cmsDetlUvKey = CMS_ + DETL_ + globStatusKey;
                    cmsDetlStatKey = CMS_ + DETL_ + dateYYYYMMDDHH;
                }
            } else if (pageCategId.equals("2")) {
                kwUvKey = KW_ + globStatusKey;
                kwUvStatKey = KW_ + dateYYYYMMDDHH;
                if (isAddCart) {
                    kwDetlUvKey = KW_ + DETL_ + globStatusKey;
                    kwDetlStatKey = KW_ + DETL_ + dateYYYYMMDDHH;
                }
            } else if (pageCategId.equals("3")) {
                categUvKey = CATEG_ + globStatusKey;
                categStatKey = CATEG_ + dateYYYYMMDDHH;
                if (isAddCart) {
                    categDetlUvKey = CATEG_ + DETL_ + globStatusKey;
                    categDetlStatKey = CATEG_ + DETL_ + dateYYYYMMDDHH;
                }
            } else if (pageCategId.equals("10")) {
                chnlUvKey = CHNL_ + globStatusKey;
                chnlStatKey = CHNL_ + dateYYYYMMDDHH;
                if (isAddCart) {
                    chnlDetlUvKey = CHNL_ + DETL_ + globStatusKey;
                    chnlDetlStatKey = CHNL_ + DETL_ + dateYYYYMMDDHH;
                }
            } else if (pageCategId.equals("7")) {
                // 商详页统计key
                detlUvKey = DETL_ + globStatusKey;
                detlUvStatKey = DETL_ + dateYYYYMMDDHH;
                // 跳转商详页 统计key
                if (!Strings.isNullOrEmpty(refpageCategId)) {
                    if (refpageCategId.equals("1")) {
                        // cms_detl_uv
                        cmsDetlUvKey = CMS_ + DETL_ + globStatusKey;
                        cmsDetlStatKey = CMS_ + DETL_ + dateYYYYMMDDHH;
                    } else if (refpageCategId.equals("2")) {
                        // kw_detl_uv
                        kwDetlUvKey = KW_ + DETL_ + globStatusKey;
                        kwDetlStatKey = KW_ + DETL_ + dateYYYYMMDDHH;
                    } else if (refpageCategId.equals("3")) {
                        // categ_detl_uv
                        categDetlUvKey = CATEG_ + DETL_ + globStatusKey;
                        categDetlStatKey = CATEG_ + DETL_ + dateYYYYMMDDHH;
                    } else if (refpageCategId.equals("10")) {
                        chnlDetlUvKey = CHNL_ + DETL_ + globStatusKey;
                        chnlDetlStatKey = CHNL_ + DETL_ + dateYYYYMMDDHH;
                    }
                }
                if (isRcmd) {
                    rcmdDetlUvKey = RCMD_ + DETL_ + globStatusKey;
                    rcmdDetlStatKey = RCMD_ + DETL_ + dateYYYYMMDDHH;
                }
            }
        }

        // TODO 购物车 判断
        if (isAddCart) {
            // 加入购物车产生的商详页统计
            detlUvKey = DETL_ + globStatusKey;
            detlUvStatKey = DETL_ + dateYYYYMMDDHH;

            //
            if (isRcmd) {
                rcmdDetlUvKey = RCMD_ + DETL_ + globStatusKey;
                rcmdDetlStatKey = RCMD_ + DETL_ + dateYYYYMMDDHH;
            }

            cartUvKey = CART_ + globStatusKey;
            cartStatKey = CART_ + dateYYYYMMDDHH;
        }
    }

    @Override
    public void aggregate(ConcurrentHashMap<String, Long> updated,
                          TridentTuple input,
                          TridentCollector collector) {
        initTridentTuple(input);
        try {
            if (!isBtnLink) {
                if (funnelCache.get(globStatusKey) == null) { // 还未统计
                    // glob landing page uv stats
                    funnelCache.put(globStatusKey, chnnlVal + _ + String.valueOf(chnnlType));
                    if (updated.containsKey(landStatKey)) {
                        updated.put(landStatKey, updated.get(landStatKey) + 1L);
                    } else {
                        updated.put(landStatKey, 1L);
                    }
                    // PC, APP, H5 flow 三个平台的以GUID互斥， 总和为全站流量和
                    String pltStatKey = USER_ + platformCode + _ + dateYYYYMMDDHH;
                    if (updated.containsKey(pltStatKey)) {
                        updated.put(pltStatKey, updated.get(pltStatKey) + 1L);
                    } else {
                        updated.put(pltStatKey, 1L);
                    }

                    // 自有 ，核心， 主动 根据渠道划分， 非互斥， 一个GUID存在多个渠道时，取最早的那个
                    if (isAcdvUv && funnelCache.get(acdvUvKey) == null) {
                        funnelCache.put(acdvUvKey, EXIST);
                        if (updated.containsKey(acdvUvStatKey)) {
                            updated.put(acdvUvStatKey, updated.get(acdvUvStatKey) + 1L);
                        } else {
                            updated.put(acdvUvStatKey, 1L);
                        }
                    }
                    if (isCoreUv && funnelCache.get(coreUvKey) == null) {
                        funnelCache.put(coreUvKey, EXIST);
                        if (updated.containsKey(coreUvStatKey)) {
                            updated.put(coreUvStatKey, updated.get(coreUvStatKey) + 1L);
                        } else {
                            updated.put(coreUvStatKey, 1L);
                        }
                    }
                    if (isOwnUv && funnelCache.get(ownUvKey) == null) {
                        funnelCache.put(ownUvKey, EXIST);
                        if (updated.containsKey(ownUvStatKey)) {
                            updated.put(ownUvStatKey, updated.get(ownUvStatKey) + 1L);
                        } else {
                            updated.put(ownUvStatKey, 1L);
                        }
                    }
                } else {
                    String[] tk = StringUtils.splitPreserveAllTokens(funnelCache.get(globStatusKey), _);
                    if ("0".equals(tk[0]) && "2".equals(String.valueOf(chnnlType))) {
                        if (isAcdvUv) {
                            if (updated.containsKey(acdvUvStatKey)) {
                                updated.put(acdvUvStatKey, updated.get(acdvUvStatKey) + 1L);
                            } else {
                                updated.put(acdvUvStatKey, 1L);
                            }
                        }
                        if (isCoreUv) {
                            if (updated.containsKey(coreUvStatKey)) {
                                updated.put(coreUvStatKey, updated.get(coreUvStatKey) + 1L);
                            } else {
                                updated.put(coreUvStatKey, 1L);
                            }
                        }
                        if (isOwnUv) {
                            if (updated.containsKey(ownUvStatKey)) {
                                updated.put(ownUvStatKey, updated.get(ownUvStatKey) + 1L);
                            } else {
                                updated.put(ownUvStatKey, 1L);
                            }
                        }

                        if (TrackerLogicUtils.isAcdvUv(tk[0])) {
                            if (updated.containsKey(acdvUvStatKey)) {
                                updated.put(acdvUvStatKey, updated.get(acdvUvStatKey) - 1L);
                            } else {
                                updated.put(acdvUvStatKey, -1L);
                            }
                        }
                        if (TrackerLogicUtils.isCoreUv(tk[0])) {
                            if (updated.containsKey(coreUvStatKey)) {
                                updated.put(coreUvStatKey, updated.get(coreUvStatKey) - 1L);
                            } else {
                                updated.put(coreUvStatKey, -1L);
                            }
                        }
                        if (TrackerLogicUtils.isOwnUv(tk[0])) {
                            if (updated.containsKey(ownUvStatKey)) {
                                updated.put(ownUvStatKey, updated.get(ownUvStatKey) - 1L);
                            } else {
                                updated.put(ownUvStatKey, -1L);
                            }
                        }
                        funnelCache.put(globStatusKey, chnnlVal + _ + String.valueOf(chnnlType));
                    }
                }

                // stat landing pages stream next jump by search, category, CMS, channel, personal-recommendation,
                if (!Strings.isNullOrEmpty(cmsUvKey) && funnelCache.get(cmsUvKey) == null) {
                    funnelCache.put(cmsUvKey, EXIST);
                    if (updated.containsKey(cmsUvStatKey)) {
                        updated.put(cmsUvStatKey, updated.get(cmsUvStatKey) + 1L);
                    } else {
                        updated.put(cmsUvStatKey, 1L);
                    }
                }

                if (!Strings.isNullOrEmpty(kwUvKey) && funnelCache.get(kwUvKey) == null) {
                    funnelCache.put(kwUvKey, EXIST);
                    if (updated.containsKey(kwUvStatKey)) {
                        updated.put(kwUvStatKey, updated.get(kwUvStatKey) + 1L);
                    } else {
                        updated.put(kwUvStatKey, 1L);
                    }
                }

                if (!Strings.isNullOrEmpty(categUvKey) && funnelCache.get(categUvKey) == null) {
                    funnelCache.put(categUvKey, EXIST);
                    if (updated.containsKey(categStatKey)) {
                        updated.put(categStatKey, updated.get(categStatKey) + 1L);
                    } else {
                        updated.put(categStatKey, 1L);
                    }
                }

                if (!Strings.isNullOrEmpty(chnlUvKey) && funnelCache.get(chnlUvKey) == null) {
                    funnelCache.put(chnlUvKey, EXIST);
                    if (updated.containsKey(chnlStatKey)) {
                        updated.put(chnlStatKey, updated.get(chnlStatKey) + 1L);
                    } else {
                        updated.put(chnlStatKey, 1L);
                    }
                }
            }

            // detl uv
            if (!Strings.isNullOrEmpty(detlUvKey) && funnelCache.get(detlUvKey) == null) {
                funnelCache.put(detlUvKey, EXIST);
                if (updated.containsKey(detlUvStatKey)) {
                    updated.put(detlUvStatKey, updated.get(detlUvStatKey) + 1L);
                } else {
                    updated.put(detlUvStatKey, 1L);
                }
            }

            // 跳转商祥统计
            if (!Strings.isNullOrEmpty(cmsDetlUvKey) && funnelCache.get(cmsDetlUvKey) == null) {
                funnelCache.put(cmsDetlUvKey, EXIST);
                if (updated.containsKey(cmsDetlStatKey)) {
                    updated.put(cmsDetlStatKey, updated.get(cmsDetlStatKey) + 1L);
                } else {
                    updated.put(cmsDetlStatKey, 1L);
                }
            }

            if (!Strings.isNullOrEmpty(kwDetlUvKey) && funnelCache.get(kwDetlUvKey) == null) {
                funnelCache.put(kwDetlUvKey, EXIST);
                if (updated.containsKey(kwDetlStatKey)) {
                    updated.put(kwDetlStatKey, updated.get(kwDetlStatKey) + 1L);
                } else {
                    updated.put(kwDetlStatKey, 1L);
                }
            }
            if (!Strings.isNullOrEmpty(categDetlUvKey) && funnelCache.get(categDetlUvKey) == null) {
                funnelCache.put(categDetlUvKey, EXIST);
                if (updated.containsKey(categDetlStatKey)) {
                    updated.put(categDetlStatKey, updated.get(categDetlStatKey) + 1L);
                } else {
                    updated.put(categDetlStatKey, 1L);
                }
            }
            if (!Strings.isNullOrEmpty(chnlDetlUvKey) && funnelCache.get(chnlDetlUvKey) == null) {
                funnelCache.put(chnlDetlUvKey, EXIST);
                if (updated.containsKey(chnlDetlStatKey)) {
                    updated.put(chnlDetlStatKey, updated.get(chnlDetlStatKey) + 1L);
                } else {
                    updated.put(chnlDetlStatKey, 1L);
                }
            }

            // cart uv
            if (!Strings.isNullOrEmpty(cartUvKey) && funnelCache.get(cartUvKey) == null) {
                funnelCache.put(cartUvKey, EXIST);
                if (updated.containsKey(cartStatKey)) {
                    updated.put(cartStatKey, updated.get(cartStatKey) + 1L);
                } else {
                    updated.put(cartStatKey, 1L);
                }
            }

            // rcmd => cart
            if (!Strings.isNullOrEmpty(rcmdDetlUvKey) && funnelCache.get(rcmdDetlUvKey) == null) {
                funnelCache.put(rcmdDetlUvKey, EXIST);
                if (updated.containsKey(rcmdDetlStatKey)) {
                    updated.put(rcmdDetlStatKey, updated.get(rcmdDetlStatKey) + 1L);
                } else {
                    updated.put(rcmdDetlStatKey, 1L);
                }
            }

            // order
            if (!Strings.isNullOrEmpty(pageCategId) && (pageCategId.equals("8") || pageCategId.equals("19"))) {
                String orderCode = input.getStringByField("orderCode");
                if (!Strings.isNullOrEmpty(orderCode)) {
                    orderStatusKey = ORDER_ + orderCode + _ + dateYYYYMMDDHH;
                    if (funnelCache.get(orderStatusKey) == null) {
                        funnelCache.put(orderStatusKey, EXIST);
                        orderStatKey = ORDER_ + dateYYYYMMDDHH; // 订单统计key
                        if (updated.containsKey(orderStatKey)) {
                            updated.put(orderStatKey, updated.get(orderStatKey) + 1L);
                        } else {
                            updated.put(orderStatKey, 1L);
                        }
                    }
                }
            }

            // 个性与精准化
            if (isRcmd && funnelCache.get(rcmdUvKey) == null) {
                funnelCache.put(rcmdUvKey, EXIST);
                if (updated.containsKey(rcmdUvStatKey)) {
                    updated.put(rcmdUvStatKey, updated.get(rcmdUvStatKey) + 1L);
                } else {
                    updated.put(rcmdUvStatKey, 1L);
                }
            }
        } catch (Exception e) {
            collector.reportError(e);
            e.printStackTrace();
            LOG.error("FunnelStatAgg:aggregate exception ", e);
        }
    }

    @Override
    public void complete(ConcurrentHashMap<String, Long> partialStatMap, TridentCollector collector) {
        // partial stats complete, 重新emit,emit的是统计结果
        // tuple 聚合然后分裂emit
        try {
            for (Map.Entry<String, Long> entry : partialStatMap.entrySet()) {
                collector.emit(new Values(entry.getKey(), entry.getValue()));
            }
            partialStatMap.clear();
        } catch (Exception e) {
            LOG.error("FunnelStatAgg:complete exception ", e);
            collector.reportError(e);
            throw new FailedException(e);
        }
    }

    @Override
    public void prepare(Map arg0, TridentOperationContext arg1) {
        try {
            funnelCache = TrackerLogicUtils.getHBaseAsyncLRU(
                    HBaseConstant.Table.TRIDENT_FUNNEL_GUID_CACHE_TABLE.tbName, null);
        } catch (IOException e) {
            LOG.error("FunnelStatAgg prepare error", e);
            throw new FailedException(e);
        }
    }
}
