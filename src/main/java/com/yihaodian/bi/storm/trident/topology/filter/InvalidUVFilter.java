package com.yihaodian.bi.storm.trident.topology.filter;

import com.yihaodian.bi.storm.common.model.TrackerVo;
import com.yihaodian.bi.storm.common.util.URLUtils;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/7/23
 */
public class InvalidUVFilter implements Filter {

    @Override
    public boolean isKeep(TridentTuple input) {
        boolean isInvalid = input.getBoolean(0);
        return !isInvalid;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
