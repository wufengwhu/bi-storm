package com.yihaodian.bi.storm.trident.topology.filter;

import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/8/3
 */
public class GlobUVFilter implements Filter {
    @Override
    public boolean isKeep(TridentTuple input) {
        boolean isRepeated = input.getBooleanByField("isRepeated");
        boolean isInvalid = input.getBooleanByField("isInvalid");
        return !isRepeated && !isInvalid;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
