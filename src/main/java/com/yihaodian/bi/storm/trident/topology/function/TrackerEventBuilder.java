package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.common.model.TrackerVo;
import com.yihaodian.bi.storm.common.util.URLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;
import java.util.Random;

/**
 * @author fengwu on 15/7/24
 */
public class TrackerEventBuilder implements Function {

    private static Logger LOG = LoggerFactory.getLogger(TrackerEventBuilder.class);

    private static final Random RANDOM = new Random();

    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        String[] trackerVos = tuple.getString(0).split("\\n");
        for (String s : trackerVos) {
            String cols[] = s.split("\\t");
            if (isKeep(s, cols)) {
                TrackerVO trackerVO = new TrackerVO(cols);
                collector.emit(new Values(trackerVO));
            }
        }
    }

    private boolean isKeep(String trackerStr, String[] cols) {

        // 过滤一：列小于30的丢弃
        if (cols.length < 30) {
            return false;
        }
        // 过滤二：过滤来自百度的爬虫数据
        if (trackerStr.contains("http://www.baidu.com/search/spider.html")) {
            return false;
        }
        //过滤三：过滤URL
        if (!"".equals(cols[1]) && cols[1] != null) {//为空的不处理
            if (!"yhd.com".equals(URLUtils.getHostFromURL(cols[1]))) {
                return false;
            }
        }
        if (cols[1].startsWith("http://union.yihaodian.com/link_make/viewPicInfo.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewSearchBox.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewRanking.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/getUserCookies.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/viewShoppingWindow.do")
                || cols[1].startsWith("http://union.yhd.com/resourceCenter/getUserCookies.do")) {
            return false;
        }

//        TrackerVO tracker = new TrackerVO(cols);

        // 过滤四：过滤button_position 不为空的
//        if (tracker.getButton_position() != null
//                && !"null".equals(tracker.getButton_position()) && tracker.getButton_position().length() > 1) {
//            return false;
//        }

        // guid, session_id is null
//        if (tracker.getGu_id() == null || tracker.getSession_id() == null) {
//            return false;
//        }

        return true;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
    }

    @Override
    public void cleanup() {

    }
}
