package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * Created by fengwu on 15/8/25.
 */
public class H5PCUVExtractFunction implements Function {

    @Override
    public void execute(TridentTuple input, TridentCollector tridentCollector) {
        TrackerVO trackerVo = (TrackerVO) input.getValueByField("trackerEvent");
        Values values = new Values();

        String guid = trackerVo.getGu_id();
        values.add(guid);
        String timeline = trackerVo.getTrack_time().replaceAll("[-: ]", "");  // 时间到秒
        values.add(timeline);
        String usr_id = trackerVo.getEnd_user_id();
        values.add(usr_id);
        int platformId = TrackerLogicUtils.getPlatformType(trackerVo).getType();
        values.add(platformId);

        tridentCollector.emit(values);
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
