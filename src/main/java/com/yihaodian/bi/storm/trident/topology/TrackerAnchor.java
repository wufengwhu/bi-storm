package com.yihaodian.bi.storm.trident.topology;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.storm.common.util.Constant;
import com.yihaodian.bi.storm.trident.redis.common.JedisPoolConfig;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/7/29
 */
public class TrackerAnchor extends BaseFunction {

    public static final redis.clients.jedis.JedisPoolConfig DEFAULT_POOL_CONFIG = new redis.clients.jedis.JedisPoolConfig();
    private String[] preStatus = new String[]{"", "", ""}; // pltmId , provId, chnlId
    private String[] curStatus = new String[]{"", "", ""};
    private String[] update = new String[]{"", "", ""};
    private String globStatusKey;
    private JedisPoolConfig poolConfig;
    private Jedis jedis;
    private CMap auxCache;

    public TrackerAnchor(JedisPoolConfig poolConfig) {
        this.poolConfig = poolConfig;
    }

    public TrackerAnchor() {
    }

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        String guid = input.getStringByField("guid");
        //TrackerVO t = (TrackerVO)input.getValueByField("trackerEvent");
        String channel = input.getStringByField("channel");
        int platformId = input.getIntegerByField("platformId");
        //String platformId = Utils.pltmType(t);
        String provId = input.getStringByField("provId");
        //String provId = Utils.provId(t);
        String timeline = input.getStringByField("timeline");
        String session_id = input.getStringByField("session_id");
        String globWin = timeline.substring(0, 8);  //全局窗口（20150724）
        String win = timeline.substring(0, 11) + "0";    // win为时间窗口（10分钟）
        String statusKey = globWin + "_" + guid + "_" + session_id; // 判断有效UV用
        globStatusKey = globWin + "_" + guid; // 去重用
        //String[] currInfo = new String[]{platform, ext_field2, channel};
        boolean isInvalid = false;    // 标记该条tuple是否需要参加后续有效的uv计算
        boolean isRepeated = false;   // 标记全局是否已经存在该guid的统计key
        if (jedis.get(statusKey) == null) { // 非有效
            isInvalid = true;
        } else { // 有效（但需要去重）, 更新当前tuple的省份， 渠道信息，锚定该条tuple，表示要减去先前已经统计，但算错的
            setPreState(jedis.get(statusKey));
            setCurState(new String[]{String.valueOf(platformId), provId, channel});
            updateCurrInfo(statusKey, isRepeated);
        }
        jedis.setex(statusKey, 43200, StringUtils.join(update, "_"));// 更新statusCache
        // 重新emit
        Values values = new Values();
        values.add(preStatus[1]);
        values.add(preStatus[2]);
        values.add(update[1]);
        values.add(update[2]);
        values.add(isInvalid);
        values.add(isRepeated);
        collector.emit(values);
    }

    private void updateCurrInfo(String statusKey, boolean isRepeated) {
        // 全局缓存状态更新，无论缓存是否命中
        if (preStatus[1].isEmpty() && !curStatus[1].isEmpty()) {
            //cache.incr("prov_" + curStatus[1] + win, 1);
            //cache.incr("prov_" + preStatus[1] + win, -1);  // 之前数据没有省份
            update[1] = curStatus[1];
        } else {
            update[1] = preStatus[1];
        }

        if ((preStatus[2].equals(Constant.UNKNOWN) || preStatus[2].equals(Constant.DIRECT))
                && (!curStatus[2].equals(Constant.UNKNOWN) && !curStatus[2].equals(Constant.DIRECT))) {
            //cache.incr("chnl_" + curStatus[2] + win, 1);
            //cache.incr("chnl_" + curStatus[2] + win, -1);  // 之前数据没有渠道
            update[2] = curStatus[2];
        } else {
            update[2] = preStatus[2];
        }

        if (jedis.get(globStatusKey) == null) { // 全局不存在该guid
            jedis.setex(globStatusKey, 43200, "1");
        } else {  // 标记重复uv, 作为后面判断是否修改已经入库的统计的依据
            isRepeated = true;
        }
    }

    private void setPreState(String vals) {
        String[] s = StringUtils.splitPreserveAllTokens(vals, "_");

        for (int i = 0; i < 3; i++) {
            this.preStatus[i] = s[i];
        }
    }

    private void setCurState(String[] vals) {
        for (int i = 0; i < 3; i++) {
            this.curStatus[i] = vals[i];
        }
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        // 记录流过数据信息，补充当前数据缺失信息,若此map在内存中丢失，只会影响后面统计小部分数据的准确性
//        try {
        JedisPool jedisPool = new JedisPool(DEFAULT_POOL_CONFIG,
                poolConfig.getHost(),
                poolConfig.getPort(),
                poolConfig.getTimeout(),
                poolConfig.getPassword(),
                poolConfig.getDatabase());

        jedis = jedisPool.getResource();

        //auxCache = CMapFactory.getChannelHBaseLRUReadonly(); // 维表 用于查询渠道
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }

    @Override
    public void cleanup() {
        if(jedis != null){
            jedis.close();
        }
    }
}
