/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yihaodian.bi.storm.trident.redis.state;

import backtype.storm.task.IMetricsContext;
//import org.apache.storm.redis.common.config.JedisClusterConfig;
//import redis.clients.jedis.JedisCluster;
import com.yihaodian.common.yredis.RedisProxy;
import com.yihaodian.common.yredis.client.YredisProxyFactory;
import com.yihaodian.common.yredis.client.exception.RedisInitException;
import storm.trident.state.State;
import storm.trident.state.StateFactory;

import java.util.Map;

/**
 * Implementation of State for Redis Cluster environment.
 */
public class RedisClusterState implements State {
    /**
     * {@inheritDoc}
     */
    @Override
    public void beginCommit(Long aLong) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commit(Long aLong) {
    }

    /**
     * RedisClusterState.Factory implements StateFactory for Redis Cluster environment.
     *
     * @see StateFactory
     */
    public static class Factory implements StateFactory {

        private String  jedisClusterConfigPath;

        public Factory(String configPath) {
            this.jedisClusterConfigPath = configPath;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public State makeState(@SuppressWarnings("rawtypes") Map conf, IMetricsContext metrics, int partitionIndex, int numPartitions) {
            try {
                YredisProxyFactory.configure(jedisClusterConfigPath);
            } catch (RedisInitException e) {
                e.printStackTrace();
            }

            RedisProxy jedisClusterProxy = YredisProxyFactory.getClient("bi_redis_test");

            return new RedisClusterState(jedisClusterProxy);
        }
    }

    private RedisProxy jedisClusterProxy;

    /**
     *
     * @param jedisClusterProxy
     */
    public RedisClusterState(RedisProxy jedisClusterProxy) {
        this.jedisClusterProxy = jedisClusterProxy;
    }

    /**
     * Borrows JedisCluster instance.
     * <p/>
     * Note that you should return borrowed instance when you finish using instance.
     *
     * @return JedisCluster instance
     */
    public RedisProxy getJedisClusterProxy() {
        return jedisClusterProxy;
    }

}
