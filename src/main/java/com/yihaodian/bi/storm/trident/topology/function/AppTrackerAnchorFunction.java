package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.cached.HBaseCMap;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.Tag;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria;
import com.yihaodian.bi.storm.trident.hbase.common.Utils;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.EXIST;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.USER_;

import java.io.IOException;
import java.util.Map;

/**
 * Created by fengwu on 15/8/25.
 */
public class AppTrackerAnchorFunction implements Function {

    private static Logger LOG = LoggerFactory.getLogger(AppTrackerAnchorFunction.class);

    private String appSessionStatusKey;
    private String appGlobStatusKey;
    private String timestamp;
    private AbstractBaseCMap cache;
    private AbstractBaseCMap errorCache;
    private HTable table;
    private String timeline;
    private String dateYYYYMMDD;


    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        TrackerVO trackerVo = (TrackerVO) input.getValueByField("trackerEvent");
        Values values = new Values();

        TrackerLogicUtils.PlatformType platformType = TrackerLogicUtils.getPlatformType(trackerVo);
        int platformId = platformType.getType();
        String platformValue = platformType.getName();

        String guid = trackerVo.getGu_id();
        String session_id = trackerVo.getSession_id();
        timestamp = trackerVo.getTrack_time();
        String timeline = timestamp.replaceAll("[-: ]", "");  // 时间到秒
        values.add(timeline);
        String dateYYYYMMDD = timeline.substring(0, 8);  //全局窗口（20150724)
        String dateYYYYMMDDHHM0 = timestamp.substring(0, 11) + "0"; // win为时间窗口（10分钟）
        appSessionStatusKey = USER_ + dateYYYYMMDD + "_" + guid + "_" + session_id; // 判断app有效UV用
        appGlobStatusKey = USER_ + dateYYYYMMDD + "_" + guid; // app去重用 yyyyMMdd_guid
        boolean isInvalid = false;
        Tag appCurrStatusTag = new Tag(platformValue, timestamp, 1);

        try {
            String currTagStr = appCurrStatusTag.toString();
            if (cache.get(appSessionStatusKey) == null) { // 非有效
                isInvalid = true;
                cache.put(appSessionStatusKey, currTagStr);
            } else {
                // app uv 有效（但需要去重), 同一session的tuple
                String appStatStatusKey = appGlobStatusKey + "_" + TrackerLogicUtils.PlatformType.APP.getLiteral();
                //当前app tuple 比之前保存状态的tuple的时间戳还要早,要刷新之前的cache状态
                Tag appPrevStatusTag = Tag.parseTag(cache.get(appSessionStatusKey));

                if (appCurrStatusTag.compareTo(appPrevStatusTag) == 0) {
                    LOG.info("tuple is valid and is before previous");
                    if ((TrackerLogicUtils.PlatformType.APP.getName().equals(appCurrStatusTag.getValue()))
                            && cache.get(appStatStatusKey) == null) {
                        cache.put(appStatStatusKey, EXIST);
                    }
                    // 统计之前错误的app有效uv
                    else if ((TrackerLogicUtils.PlatformType.APP.getName().equals(appPrevStatusTag.getValue()))
                            && cache.get(appStatStatusKey) != null) {
                        errorCache.incr(USER_ + appPrevStatusTag.getValue() + "1" + dateYYYYMMDDHHM0, 1);
                    } else {
                        isInvalid = true;
                    }
                    // 修改cache 状态
                    cache.put(appSessionStatusKey, currTagStr);
                } else { // 当前app tuple时序正常
                    // 先前保存的tuple没有被计算，且先前的tuple的平台是app，后面来的同一session tuple无论是什么平台，只计算先前保存的
                    LOG.info("prev stored tuple is first: " + appPrevStatusTag.isFirst() +
                            " prev stored tuple's platform value=: " + appPrevStatusTag.getValue());
                    if (appPrevStatusTag.isFirst() &&
                            (TrackerLogicUtils.PlatformType.APP.getName().equals(appPrevStatusTag.getValue()))) {
                        LOG.info("tuple is valid and prev stored status is app ");
                        if (cache.get(appStatStatusKey) == null) {
                            cache.put(appStatStatusKey, EXIST);
                            // 修改cache 状态
                            appPrevStatusTag.setFirst(false);
                            cache.put(appSessionStatusKey, appPrevStatusTag.toString());
                            // 当前tuple用先前保存的tuple的平台信息作为平台，做一次有效的app uv计算
                            platformId = Integer.parseInt(appPrevStatusTag.getValue());
                        } else {
                            LOG.info("tuple is repeated, so it is Invalid ");
                            isInvalid = true;
                        }
                    } else { // 先前保存的状态没有被计算 或者先前保存平台的不是app，都不care
                        if (PrintFunction.RANDOM.nextInt(1000) > 995) {
                            LOG.info("tuple is valid but is not satisfy count condition,so it's Invalid");
                        }
                        isInvalid = true;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("HBaseAsyncLRU cache get or put error", e);
            throw new RuntimeException(e);
        }
        values.add(platformId);
        values.add(isInvalid);
        collector.emit(values);
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
        // 记录流过数据信息，补充当前数据缺失信息,若此map在内存中丢失，只会影响后面统计小部分数据的准确性
        try {
            Long starts = System.currentTimeMillis();
            cache = TrackerLogicUtils.getHBaseAsyncLRU(HBaseConstant.Table.UV_CACHE_TEST_TABLE.tbName, null);
            HBaseProjectionCriteria.ColumnMetaData columnMetaData =
                    new HBaseProjectionCriteria.ColumnMetaData("cf", "error_count");
            errorCache = TrackerLogicUtils.getHBaseAsyncLRU(HBaseConstant.Table.UV_CACHE_TEST_TABLE.tbName,
                    columnMetaData);
            LOG.debug("HBaseAsyncLRU cache init success and cost " + (System.currentTimeMillis() - starts));
        } catch (IOException e) {
            LOG.error("AppTrackerAnchorFunction:prepare hbase table or cache error", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void cleanup() {
        // cache 关联的hbase table 关闭连接，这个操作不平凡
        try {
            HTable table = HBaseFactory.getTable(HBaseConstant.Table.UV_CACHE_TEST_TABLE);
            if (table != null) {
                table.close();
            }
        } catch (IOException e) {
            LOG.error("AppTrackerAnchorFunction:prepare hbase table or cache error", e);
            throw new RuntimeException(e);
        }
    }
}
