package com.yihaodian.bi.storm.trident.topology.function;

import backtype.storm.tuple.Values;
import com.yihaodian.bi.kafka.TrackerVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * @author fengwu on 15/7/24
 */
public class GlobUVExtractFunction extends BaseFunction {

    private static Logger LOG = LoggerFactory.getLogger(GlobUVExtractFunction.class);

    @Override
    public void execute(TridentTuple input, TridentCollector collector) {
        TrackerVO trackerVo = (TrackerVO) input.getValueByField("trackerEvent");
        Values values = new Values();

//        try {
//            values.add(TrackerLogicUtils.channel(trackerVo));
//        } catch (Exception e) {
//            // TODO deal with exception
//            e.printStackTrace();
//        }

        //int platformId = TrackerLogicUtils.getPlatformType(trackerVo).getType();
        //values.add(platformId);
        String guid = trackerVo.getGu_id();
        values.add(guid);
        String session_id = trackerVo.getSession_id();
        values.add(session_id);
        //long trckerTimeStamp = DateFmat.transTimeStrToStamp(trackerVo.getTrack_time(),
        // DateFmat.YYYY_MM_DD_HH_MM_SS);
        //long hourSinceEpoch = trckerTimeStamp / 1000 / 60 / 60 ;  // 从纪元开始的小时数
        //String timeline = DateFmat.transferDateToString(new Date(trckerTimeStamp), DateFmat.YYYYMMDDHHMM_STR);
        String timeline = trackerVo.getTrack_time().replaceAll("[-: ]", "");  // 时间到秒
        //String guid_sessionid_hour = guid + ":" + trackerVo.getSession_id() + ":" + hourSinceEpoch;
        //values.add(hourSinceEpoch);

        values.add(timeline);
        //String ext_field2 = trackerVo.getExt_field2();  // provence id
        //String provId = TrackerLogicUtils.provId(trackerVo);
        //values.add(null);
        //values.add(provId);
        //String end_user_id = trackerVo.getEnd_user_id();
        //values.add(end_user_id);
        //values.add(timeline.substring(0, 8));
        collector.emit(values);
    }


    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
    }

    @Override
    public void cleanup() {
        // TODO close resources
    }
}
