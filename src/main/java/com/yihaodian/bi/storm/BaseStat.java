package com.yihaodian.bi.storm;

import java.io.Serializable;

/**
 * 需要统计的类必须继承的对象
 * 
 * @author zhaoheng Aug 5, 2015 2:27:43 PM
 */
public class BaseStat implements Serializable {

    protected String tag;

    public BaseStat(){
    }
    public BaseStat(TopoGroup tag){
        this.tag = tag.getName().toLowerCase();
    }

    public String getTag() {
        return tag;
    }
}
