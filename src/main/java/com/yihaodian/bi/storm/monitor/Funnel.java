package com.yihaodian.bi.storm.monitor;

import java.sql.*;
import java.util.Calendar;

/**
 * Created by fengwu on 15/9/17.
 */
public class Funnel extends BaseMonitor {

    private static String TOPIC = "[STORM监控][漏斗流量]";
    private static String sql;
    private static String errLog;
    private static long ONE_HOUR;
    private static double threshold;
    private static PreparedStatement pstmt;
    private static boolean err = false;

    static {
        sql =
                "select t.date_id as date_id,\n" +
                        "       t.hour_id as hour_id, t.pc_uv as storm_pc_uv,\n" +
                        "       t1.pc_uv as hive_pc_uv,\n" +
                        "       (t.pc_uv - t1.pc_uv) / t1.pc_uv as pc_uv_relative_error_rate,\n" +
                        "       t.h5_uv as storm_h5_uv,\n" +
                        "       t1.h5_uv as hive_h5_uv,\n" +
                        "       (t.h5_uv - t1.h5_uv) / t1.h5_uv as h5_uv_relative_error_rate,\n" +
                        "       t.app_uv as storm_app_uv,\n" +
                        "       t1.app_uv as hive_app_uv,\n" +
                        "       (t.app_uv - t1.app_uv) / t1.app_uv as app_uv_relative_error_rate,\n" +
                        "       t.acdv_uv as storm_acdv_uv,\n" +
                        "       t1.acdv_uv as hive_acdv_uv,\n" +
                        "       (t.acdv_uv - t1.acdv_uv) / t1.acdv_uv as acdv_uv_relative_error_rate,\n" +
                        "       t.core_uv as storm_core_uv,\n" +
                        "       t1.core_uv as hive_core_uv,\n" +
                        "       (t.core_uv - t1.core_uv) / t1.core_uv as core_uv_relative_error_rate,\n" +
                        "       t.own_uv as storm_own_uv,\n" +
                        "       t1.own_uv as hive_own_uv,\n" +
                        "       (t.own_uv - t1.own_uv) / t1.own_uv as own_uv_relative_error_rate,\n" +
                        "       t.land_uv as storm_land_uv,\n" +
                        "       t1.land_uv as hive_land_uv,\n" +
                        "       (t.land_uv - t1.land_uv) / t1.land_uv as land_uv_relative_error_rate,\n" +
                        "       t.kw_uv as storm_kw_uv,\n" +
                        "       t1.kw_uv as hive_kw_uv,\n" +
                        "       (t.kw_uv - t1.kw_uv) / t1.kw_uv as kw_uv_relative_error_rate,\n" +
                        "       t.categ_uv as storm_categ_uv,\n" +
                        "       t1.categ_uv as hive_categ_uv,\n" +
                        "       (t.categ_uv - t1.categ_uv) / t1.categ_uv as categ_uv_relative_error_rate,\n" +
                        "       t.cms_uv as storm_cms_uv,\n" +
                        "       t1.cms_uv as hive_cms_uv,\n" +
                        "       (t.cms_uv - t1.cms_uv) / t1.cms_uv cms_uv_relative_error_rate,\n" +
                        "       t.chnl_uv as storm_chnl_uv,\n" +
                        "       t1.chnl_uv as hive_chnl_uv,\n" +
                        "       (t.chnl_uv - t1.chnl_uv) / t1.chnl_uv as chnl_uv_relative_error_rate,\n" +
                        "       t.rcmd_uv as storm_rcmd_uv,\n" +
                        "       t1.rcmd_uv as hive_rcmd_uv,\n" +
                        "       (t.rcmd_uv - t1.rcmd_uv) / t1.rcmd_uv as rcmd_uv_relative_error_rate,\n" +
                        "       t.detl_uv as storm_detl_uv,\n" +
                        "       t1.detl_uv as hive_detl_uv,\n" +
                        "       (t.detl_uv - t1.detl_uv) / t1.detl_uv as detl_uv_relative_error_rate,\n" +
                        "       t.cart_uv as storm_cart_uv,\n" +
                        "       t1.cart_uv as hive_cart_uv,\n" +
                        "       (t.cart_uv - t1.cart_uv) / t1.cart_uv as cart_uv_relative_error_rate,\n" +
                        "       t.ordr_num as storm_ordr_num,\n" +
                        "       t1.ordr_num as hive_ordr_num,\n" +
                        "       (t.ordr_num - t1.ordr_num) / t1.ordr_num as ordr_num_relative_error_rate\n" +
                        "  from rpt.rpt_realtime_scrn_tfc_funnel_s t\n" +
                        "  join rpt.rpt_realtime_scrn_tfc_funnel t1\n" +
                        "    on t.date_id = t1.date_id\n" +
                        "   and t.hour_id = t1.hour_id\n" +
                        "   where t.date_id = ? and t.hour_id = ?\n";

        errLog = "insert into rpt.rpt_realtime_montr_err_log\n" + "values (?, ?, ?, ?, 'Funnel')";
        ONE_HOUR = 60 * 60 * 1000;
        threshold = 0.0;
    }

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        new Funnel().doMonitor(args);
    }

    @Override
    protected void monitor(String[] args) throws Exception {
        pstmt = oracleCon.prepareStatement(errLog);
        PreparedStatement stmt = oracleCon.prepareStatement(sql);
        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        c.add(Calendar.HOUR_OF_DAY, -1);
        stmt.setDate(1, new Date(c.getTimeInMillis()));
        stmt.setInt(2, c.get(Calendar.HOUR_OF_DAY));
        ResultSet rs = stmt.executeQuery();
        threshold = Double.parseDouble(args[0]);
        while (rs.next()) {
            Date date_id = rs.getDate(1);
            String hour_id = rs.getString(2);
            //land_uv
            compare("land_uv", rs.getLong("storm_land_uv"), rs.getLong("hive_land_uv"),
                    rs.getDouble("land_uv_relative_error_rate"), date_id, hour_id);
            // pc_uv
            compare("pc_uv", rs.getLong("storm_pc_uv"), rs.getLong("hive_pc_uv"),
                    rs.getDouble("pc_uv_relative_error_rate"), date_id, hour_id);
            // h5_uv
            compare("h5_uv", rs.getLong("storm_h5_uv"), rs.getLong("hive_h5_uv"),
                    rs.getDouble("h5_uv_relative_error_rate"), date_id, hour_id);
            // app_uv
            compare("app_uv", rs.getLong("storm_app_uv"), rs.getLong("hive_app_uv"),
                    rs.getDouble("app_uv_relative_error_rate"), date_id, hour_id);
            //acdv_uv  acdv_relative_error_rate
            compare("acdv_uv", rs.getLong("storm_acdv_uv"), rs.getLong("hive_acdv_uv"),
                    rs.getDouble("acdv_uv_relative_error_rate"), date_id, hour_id);
            //core_uv
            compare("core_uv", rs.getLong("storm_core_uv"), rs.getLong("hive_core_uv"),
                    rs.getDouble("core_uv_relative_error_rate"), date_id, hour_id);
            //own_uv
            compare("own_uv", rs.getLong("storm_own_uv"), rs.getLong("hive_own_uv"),
                    rs.getDouble("own_uv_relative_error_rate"), date_id, hour_id);
            //kw_uv
            compare("kw_uv", rs.getLong("storm_kw_uv"), rs.getLong("hive_kw_uv"),
                    rs.getDouble("kw_uv_relative_error_rate"), date_id, hour_id);
            //categ_uv
            compare("", rs.getLong("storm_categ_uv"), rs.getLong("hive_categ_uv"),
                    rs.getDouble("categ_uv_relative_error_rate"), date_id, hour_id);
            //cms_uv
            compare("cms_uv", rs.getLong("storm_cms_uv"), rs.getLong("hive_cms_uv"),
                    rs.getDouble("cms_uv_relative_error_rate"), date_id, hour_id);
            //chnl_uv
            compare("chnl_uv", rs.getLong("storm_chnl_uv"), rs.getLong("hive_chnl_uv"),
                    rs.getDouble("chnl_uv_relative_error_rate"), date_id, hour_id);
            //rcmd_uv
            compare("rcmd_uv", rs.getLong("storm_rcmd_uv"), rs.getLong("hive_rcmd_uv"),
                    rs.getDouble("rcmd_uv_relative_error_rate"), date_id, hour_id);
            //detl_uv
            compare("detl_uv", rs.getLong("storm_detl_uv"), rs.getLong("hive_detl_uv"),
                    rs.getDouble("detl_uv_relative_error_rate"), date_id, hour_id);
            //cart_uv
            compare("cart_uv", rs.getLong("storm_cart_uv"), rs.getLong("hive_cart_uv"),
                    rs.getDouble("cart_uv_relative_error_rate"), date_id, hour_id);
            //ordr_num
            compare("ordr_num", rs.getLong("storm_ordr_num"), rs.getLong("hive_ordr_num"),
                    rs.getDouble("ordr_num_relative_error_rate"), date_id, hour_id);
        }

        if (err) {
            pstmt.executeBatch();
            pstmt.close();
        }
    }

    private void compare(String statIndex,
                         long stormValue,
                         long hiveValue,
                         double relativeErrorRate,
                         Date date_id,
                         String hour_id) throws Exception {
        System.out.println("date_id:" + date_id +
                " hour_id:" + hour_id +
                " storm:" + stormValue +
                " hive:" + hiveValue +
                " diff:" + relativeErrorRate);
        if (Math.abs(relativeErrorRate) > threshold) {
            String msg = String.format(date_id + " " + hour_id + "时，" + statIndex + " Storm数据与Hive相差 %.2f%%", relativeErrorRate * 100);
            sendMessageByCache(msg, TOPIC);
            pstmt.setDate(1, date_id);
            pstmt.setTime(2, new java.sql.Time(date_id.getTime() + ONE_HOUR * Integer.parseInt(hour_id)));
            pstmt.setString(3, msg);
            pstmt.setTime(4, new java.sql.Time(System.currentTimeMillis()));
            pstmt.addBatch();
            if (!err) err = true;
        }
    }

    @Override
    public void help() {
        msg("漏斗流量监控");
    }

    @Override
    public String getMonitorName() {
        return "Funnel";
    }
}
