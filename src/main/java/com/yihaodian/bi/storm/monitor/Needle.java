package com.yihaodian.bi.storm.monitor;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Needle extends BaseMonitor {

    private static String TOPIC    = "[STORM监控][神针流量]";
    static String         sql      = "select s.date_id,\n" + "       s.hour_id,\n"
                                     + "       s.globl_sec_uv as storm,\n" + "       o.globl_sec_uv as hive,\n"
                                     + "       (s.globl_sec_uv - o.globl_sec_uv) / o.globl_sec_uv as diff\n"
                                     + "  from rpt.rpt_realtime_trfc_hour_s s\n"
                                     + "  join rpt.rpt_realtime_trfc_hour_o o\n" + "    on s.date_id = o.date_id\n"
                                     + "   and s.hour_id = o.hour_id\n" + "   where s.date_id = trunc(sysdate)"
                                     + "  order by hour_id desc";
    static String         errLog   = "insert into rpt.rpt_realtime_montr_err_log\n" + "values (?, ?, ?, ?, 'Needle')";
    static long           ONE_HOUR = 60 * 60 * 1000;

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        new Needle().doMonitor(args);
    }

    @Override
    protected void monitor(String[] args) throws Exception {
        PreparedStatement pstmt = oracleCon.prepareStatement(errLog);
        boolean err = false;
        Statement stmt = oracleCon.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        double threshold = Double.parseDouble(args[0]);

        while (rs.next()) {
            Date date_id = rs.getDate(1);
            String hour_id = rs.getString(2);
            long storm = rs.getLong(3);
            long hive = rs.getLong(4);
            double diff = rs.getDouble(5);

            System.out.println("date_id:" + date_id + " hour_id:" + hour_id + " storm:" + storm + " hive:" + hive
                               + " diff:" + diff);
            if (Math.abs(diff) > threshold) {
                String msg = String.format(date_id + " " + hour_id + "时，Storm数据与Hive相差 %.2f%%", diff * 100);
                sendMessageByCache(msg, TOPIC);
                pstmt.setDate(1, date_id);
                pstmt.setTime(2, new java.sql.Time(date_id.getTime() + ONE_HOUR * Integer.parseInt(hour_id)));
                pstmt.setString(3, msg);
                pstmt.setTime(4, new java.sql.Time(System.currentTimeMillis()));
                pstmt.addBatch();
                if (!err) err = true;
            }
        }

        if (err) {
            pstmt.executeBatch();
            pstmt.close();
        }
    }

    @Override
    public void help() {
        msg("神针流量监控");
    }

    @Override
    public String getMonitorName() {
        // TODO Auto-generated method stub
        return "Needle";
    }

}
