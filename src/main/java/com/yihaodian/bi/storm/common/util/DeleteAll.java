package com.yihaodian.bi.storm.common.util;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.client.Result;

import com.yihaodian.bi.hbase.dao.BaseDao;
import com.yihaodian.bi.hbase.dao.impl.BaseDaoImpl;

public class DeleteAll {

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            printUsage();
            return;
        }
        BaseDaoImpl dao = new BaseDaoImpl();
        String tableName = args[0];
        String rowKeyPrefix = args[1];

        if (args.length >= 4) {
            long minStamp = Long.parseLong(args[2]);
            long maxStamp = Long.parseLong(args[3]);
            dao.deleteAll(tableName, rowKeyPrefix, minStamp, maxStamp);
        } else {
            dao.deleteAll(tableName, rowKeyPrefix);
        }
        // List<Result> rsList = dao.getRecordByRowKeyRegex(tableName, rowKeyPrefix);
        // for (Result result : rsList) {
        // String rowKeyString = new String(result.getRow());
        // System.out.println("delete - " + rowKeyString);
        // dao.deleteRecord(tableName, rowKeyString);
        // }

        System.out.println("Successfully deleted all " + rowKeyPrefix + "* from " + tableName);

    }
    
    public static void printUsage() {
        System.out.println("Usage: DeleteAll tablename prefix [minstamp maxstamp]");
    }

}
