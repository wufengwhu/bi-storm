package com.yihaodian.bi.storm.logic.tracker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.database.DBConnFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

import com.google.common.base.Strings;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.cached.HBaseRepresentor;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.TopoGroup;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.PlatformType;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.PageType;
import com.yihaodian.common.DateUtil;

import static com.yihaodian.bi.storm.common.util.Constant.*;


/**
 * Created on 11/24/15.
 * <p/>
 * This application is to monitor all trackers on YiHaoDian website.
 * Trackers are categorized into three types-- Desktop, H5 pages, App --
 * according to different visit sources, further manage drill-down by
 * visited page categories, [portal|detail|channel|SEO|CMS] pages.
 * The PV, UV and UV with more than two hits of all the types above are
 * calculated instantly.
 * <p/>
 * All results are stored in HBase, and here is a list of row keys:
 * <p/>
 * [pv|uv|vu]_[pc|h5|app]_yyyyMMddHHmm
 * [pv|uv|vu]_[pc|h5|app]_[ptl|dtl|chl|seo|cms]_yyyyMMddHHmm
 * <p/>
 * Since trackers from PC, H5, APP are mutually exclusive, the aggregation of the
 * three equals the trackers of whole YiHaoDian website. Likewise, different page
 * type-- portal, detail, channel, seo, cms pages-- could be aggregated.
 */

public class TrackerMonitorPackage {

  public static final String EXIST = "0";

  public enum Kpi {
    PV(1, "pv"), UV(2, "uv"), VU(3, "vu");

    private Integer _id;
    private String _name;

    Kpi(Integer id, String name) {
      _id = id;
      _name = name;
    }

    public Integer getId() {
      return _id;
    }

    public String getName() {
      return _name;
    }
  }


  private static String buildRowKey(String prefix, String... str) {
    StringBuilder rs = new StringBuilder();
    rs.append(prefix);
    for (String s : str) {
      rs.append(_ + s);
    }
    return rs.toString();
  }

  /**
   *
   */
  public static class TrackerMonitorBolt extends BaseStat implements IRichBolt {

    private static final Logger LOG = LoggerFactory.getLogger(TrackerMonitorBolt.class);
    private AbstractBaseCMap globCache;
    private AbstractBaseCMap pageCategCache;
    private String globStatusKey;
    private String pageCategId;
    private StatConstants.CacheStat stat;
    private OutputCollector _collector;
    private TrackerVO t = new TrackerVO();
    private String timestamp;
    //    private String winYYYYMMDD;
    private PlatformType pltm;
    private String refpageCategId;
    private PageType pageCateg;
    private String winYYYYMMDDHHMM;
    private String pvPltmKey;
    private String uvPltmKey;
    private String pvPltmCategKey;
    private String uvPltmCategKey;
    private String vuPltmCategKey;
    private String pageStatusKey;
    private String refPageStatusKey;
    private PageType refPageCateg;

    public TrackerMonitorBolt(TopoGroup tag) {
      super(tag);
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
      try {
        Long starts = System.currentTimeMillis();
        globCache = (AbstractBaseCMap) TrackerLogicUtils.getTrackerMonitorHBaseAsyncLRU();
        pageCategCache = TrackerLogicUtils.getPageTypeToCategHBaseLRUReadonly();
        stat = new StatConstants.CacheStat(this);
        TrackerLogicUtils.initTrackerMonitorCache();
        LOG.debug("Successfully initailed caches and taken: " + (System.currentTimeMillis() - starts) + "ms.");
      } catch (Exception e) {
        LOG.error("Error occurred when initialing caches.", e);
        throw new RuntimeException(e);
      }
    }

    /**
     * UVs are summarized distinctly within one day, thus GUIDs are cached.
     * Additionally, valid UV are counted according to the cache.
     * <p/>
     * Since the cache is persisted in HBase, the row key pattern is:
     * <p/>
     * yyyyMMdd_GUID
     * <p/>
     * , in which GUID stands for a guid in the tracker records.
     *
     * @param input
     */
    @Override
    public void execute(Tuple input) {
      try {
        stat.intervalWriteAndClean(true);
        stat.incr(stat.TRACKER_BOLT_RECV);
        init(input);

        if (!TrackerLogicUtils.isBtnLink(t.getButton_position())) { // 过滤button_link的流量
          pltm = TrackerLogicUtils.getPlatformType(t);
          pvPltmKey = buildRowKey(Kpi.PV.getName(), pltm.getLiteral(), winYYYYMMDDHHMM);
          uvPltmKey = buildRowKey(Kpi.UV.getName(), pltm.getLiteral(), winYYYYMMDDHHMM);

          // PV
          incrGlobCache(pvPltmKey); // platform PV

          // UV
          if (!isExist(globStatusKey)) {
            setExist(globStatusKey);
            incrGlobCache(uvPltmKey); // platform UV
          }

          // page UV
          if (pageCateg != null) {
            pvPltmCategKey = buildRowKey(Kpi.PV.getName(), pltm.getLiteral(), pageCateg.getName(), winYYYYMMDDHHMM);
            incrGlobCache(pvPltmCategKey); // platform-page PV
            if (!isExist(pageStatusKey)) {
              setExist(pageStatusKey);
              uvPltmCategKey = buildRowKey(Kpi.UV.getName(), pltm.getLiteral(), pageCateg.getName(), winYYYYMMDDHHMM);
              incrGlobCache(uvPltmCategKey); // platform-page UV
            }
          }

          // 2-hit page UV
          if (!isExist(refPageStatusKey) && refPageCateg != null) {
            setExist(refPageStatusKey);
            vuPltmCategKey = buildRowKey(Kpi.VU.getName(), pltm.getLiteral(), refPageCateg.getName(), winYYYYMMDDHHMM);
            incrGlobCache(vuPltmCategKey); // 2-hit page UV
          }

        }
        stat.incr(stat.TRACKER_BOLT_SUCC);
      } catch (Exception e) {
        LOG.error("FunnelLogicBolt exception " + input.getMessageId(), e);
        stat.incr(stat.TRACKER_BOLT_FAIL);
      }
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
      return null;
    }

    private void init(Tuple input) throws Exception {
      // 清空上次计算的状态
      clear();
      // set Tracker Object
      t.setGu_id(input.getStringByField("guid"));
      t.setSession_id(input.getStringByField("sid"));
      t.setPlatform(input.getStringByField("pltfmId"));
      t.setExt_field2(input.getStringByField("provId"));
      t.setTrack_time(input.getStringByField("tracktime"));
      t.setTracker_u(input.getStringByField("tracku"));
      t.setReferer(input.getStringByField("referer"));
      t.setEnd_user_id(input.getStringByField("uid"));
      t.setUrl(input.getStringByField("url"));
      t.setContainer(input.getStringByField("container"));
      t.setPagetypeid(input.getStringByField("pagetypeId"));
      t.setRefpagetypeid(input.getStringByField("refpagetypeId"));
      t.setLink_position(input.getStringByField("linkPos"));
      t.setButton_position(input.getStringByField("buttonPos"));
      t.setPositiontypeid(input.getStringByField("posTypeId"));

      timestamp = t.getTrack_time().replaceAll("[-: ]", "");
//      winYYYYMMDD = timestamp.substring(0, 8); // global interval（20151231）, day
      winYYYYMMDDHHMM = timestamp.substring(0, 12); // display internal（201512312359）, minute
      globStatusKey = buildRowKey(winYYYYMMDDHHMM, t.getGu_id()); // 去重用 yyyyMMdd_guid

      // page_categ_id根据orig_page_type_id到dw.dim_page_type中查
      pageCategId = pageCategCache.get(t.getPagetypeid());
      refpageCategId = pageCategCache.get(t.getRefpagetypeid());

      pageCateg = mapPageCategId(pageCategId);
      refPageCateg = mapPageCategId(refpageCategId);
      if (pageCateg != null) {
        //  page status that cached for 2-hit page
        pageStatusKey = buildRowKey(winYYYYMMDDHHMM, t.getGu_id(), pageCateg.getName());
      }
      if (refPageCateg != null) {
        //  page status that cached for 2-hit page
        refPageStatusKey = buildRowKey(winYYYYMMDDHHMM, t.getGu_id(), refPageCateg.getName());
      }
    }

    private void clear() {
      globStatusKey = null;
      pageStatusKey = null;
      refPageStatusKey = null;
    }

    /**
     * Map page category Id to PageType Enum.
     *
     * @param pageCategId page category Id
     * @return page category name
     */
    private PageType mapPageCategId(String pageCategId) {
      PageType pageType = null;
      if (!Strings.isNullOrEmpty(pageCategId)) {
        if (pageCategId.equals(CMS_ID.toString())) {
          pageType = PageType.CMS;
        } else if (pageCategId.equals(KEY_WORD_ID.toString())) {
          pageType = PageType.KEYWORD;
        } else if (pageCategId.equals(CATEGORY_ID.toString())) {
          pageType = PageType.CATEGORY;
        } else if (pageCategId.equals(CHANNEL_ID.toString())) {
          pageType = PageType.CHANNEL;
        } else if (pageCategId.equals(DETAIL_ID.toString())) {
          pageType = PageType.DETAIL;
        } else if (pageCategId.equals(PORTAL_ID.toString())) {
          pageType = PageType.PORTAL;
        } else if (pageCategId.equals(PROMOTION_ID.toString())) {
          pageType = PageType.PROMOTION;
        }
      }
      return pageType;
    }

    /**
     * Determine whether the specific key exists in global cache.
     *
     * @param key the specific key to test
     * @return true if the key is exists, otherwise false
     * @throws Exception
     */
    private boolean isExist(String key) throws Exception {
      return !Strings.isNullOrEmpty(key) && globCache.get(key) != null;
    }

    /**
     * set the value of the specific key to EXIST in glob cache.
     *
     * @param key
     * @throws Exception
     */
    private void setExist(String key) throws Exception {
      globCache.put(key, EXIST);
    }

    /**
     * Increase the value of a specific key by one in the glob cache.
     *
     * @param key the key
     * @throws Exception
     */
    private void incrGlobCache(String key) throws Exception {
      if (!Strings.isNullOrEmpty(key)) {
        globCache.incr(key, 1L);
      }
    }
  }

  /**
   * This class is used to fetch data on HBase.
   */
  public static class DataViewer extends HBaseRepresentor {

    private static final Logger LOG = LoggerFactory.getLogger(DataViewer.class);

    public long sumPlatform(Kpi kpi, PlatformType pltfm, String startTimeStamp,
                            String endTimeStamp) throws IOException {
      String startRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), startTimeStamp);
      String endRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), endTimeStamp);
      return super.sum(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName, startRow, endRow);
    }

    public long sumPlatformPage(Kpi kpi, PlatformType pltfm, PageType pageType, String startTimeStamp,
                                String endTimeStamp) throws IOException {
      String startRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), pageType.getName(), startTimeStamp);
      String endRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), pageType.getName(), endTimeStamp);
      return super.sum(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName, startRow, endRow);
    }

    public Map<String, Long> curvePlatform(Kpi kpi, PlatformType pltfm, String startTimesStamp,
                                           String endTimeStamp) {
      String origin = buildRowKey(kpi.getName(), pltfm.getLiteral(), startTimesStamp.substring(0, 8));
      String startRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), startTimesStamp);
      String endRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), endTimeStamp);

      return super.curvePartial(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName,
              origin, startRow, endRow);
    }

    public Map<String, Long> curvePlatformPage(Kpi kpi, PlatformType pltfm, PageType page,
                                               String startTimesStamp, String endTimeStamp) {
      String origin = buildRowKey(kpi.getName(), pltfm.getLiteral(), page.getName(),
              startTimesStamp.substring(0, 8));
      String startRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), page.getName(), startTimesStamp);
      String endRow = buildRowKey(kpi.getName(), pltfm.getLiteral(), page.getName(), endTimeStamp);

      return super.curvePartial(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName,
              origin, startRow, endRow);
    }

    public long sumPage(String prefix, PageType pageType, String startTimeStamp,
                        String endTimeStamp) throws IOException {
      long rs = 0;
      for (PlatformType pltfm : PlatformType.values()) {
        String startRow = buildRowKey(prefix, pltfm.getName(), pageType.getName(), startTimeStamp);
        String endRow = buildRowKey(prefix, pltfm.getName(), pageType.getName(), endTimeStamp);
        rs += super.sum(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName, startRow, endRow);
      }
      return rs;
    }

    public long platformKpi(Kpi kpi, PlatformType pltfm, String timeStamp)
            throws IOException {
      return sumPlatform(kpi, pltfm, timeStamp.substring(0, 8), timeStamp);
    }

    public long platformPageKpi(Kpi kpi, PlatformType pltfm, PageType pageType,
                                String timeStamp) throws IOException {
      return sumPlatformPage(kpi, pltfm, pageType, timeStamp.substring(0, 8), timeStamp);
    }

    public long getKpiValue(Kpi kpi, PlatformType pltfm, String timeStamp) throws IOException {
      String rowKey = buildRowKey(kpi.getName(), pltfm.getLiteral(), timeStamp);

      return super.get(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName, rowKey);
    }

    public long getKpiValue(Kpi kpi, PlatformType pltfm, PageType page, String timeStamp) throws IOException {
      String rowKey = buildRowKey(kpi.getName(), pltfm.getLiteral(), page.getName(), timeStamp);

      return super.get(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE.tbName, rowKey);
    }

    /**
     * 功能函数 用于测试
     *
     * @param map
     */
    private static void traverseMap(Map<String, ?> map) {
      List<String> kvList = new ArrayList<String>();
      for (Map.Entry<?, ?> e : map.entrySet()) {
        kvList.add(e.getKey() + "=>" + e.getValue());
      }
      String[] kvArray = kvList.toArray(new String[0]);
      Arrays.sort(kvArray);
      for (int i = 0; i < kvArray.length; i++) {
        System.out.println(kvArray[i]);
      }
    }

  }

  /**
   *
   *
   */
  public static class Sync2DB {

    static StatConstants.KafkaSpoutStat stat;
    static final long ONE_MIN = 60 * 1000L;
    static final long ONE_HOUR = 60 * 60 * 1000L;
    static final int UPDATE_INTERVAL = 30;
    static Connection conn;
    static PreparedStatement pstmt0;
    static PreparedStatement pstmt1;
    static String INSERT_MINUTE_TRACKER = "insert into " + TABLE_TRACKER_MONITOR +
            "(date_id, " +
            "date_time_id, " +
            "pltfm_lvl2_id," +
            "pltfm_lvl2_name," +
            "page_categ_id," +
            "page_categ_name," +
            "kpi_id," +
            "kpi_name," +
            "kpi_val," +
            "update_time) values(?,?,?,?,?,?,?,?,?,?)";
    static String UPDATE_MINUTE_TRACKER = "update " + TABLE_TRACKER_MONITOR +
            " set kpi_val= ?,\n" +
            "update_time = ?\n" +
            "where date_time_id = ?\n" +
            "and pltfm_lvl2_id = ?\n" +
            "and page_categ_id = ?\n" +
            "and kpi_id = ?\n";
    static DataViewer viewer;
    static java.sql.Date date_id;
    static java.sql.Date date_id_lasthour;
    static Timestamp date_time_id;
    static String timeStamp;
    static String yesterdayTimestamp;
    static String lastWeekTimestamp;
    static String lastHourTimestamp;
    static int hour_id;

    static {
      try {
        init();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private static void init() throws Exception {
      conn = DBConnFactory.getConnection("mysql").getConnection();
//      conn = DBConnFactory.getConnection("oracle").getConnection();
      conn.setAutoCommit(false);

      lastHourTimestamp = getLastHourTimestamp();
      yesterdayTimestamp = DateUtil.getFormatYestoday("yyyyMMddHHmm");
      lastWeekTimestamp = DateUtil.getFormatCurrentAdd(-7, "yyyyMMddHHmm");

      date_id = new java.sql.Date(System.currentTimeMillis());
      date_id_lasthour = new java.sql.Date(System.currentTimeMillis() - ONE_HOUR);
      date_time_id = getDateTimeId(); //date time id of LAST minute
      timeStamp = getTimeStampString(date_time_id); //timestamp of LAST minute
      pstmt0 = conn.prepareStatement(INSERT_MINUTE_TRACKER);
      pstmt1 = conn.prepareStatement(UPDATE_MINUTE_TRACKER);
      if (!Debug.DEBUG) viewer = new DataViewer();
    }

    private static String getTimeStampString(Timestamp ts) {
      return ts.toString().substring(0, 19).replaceAll("[-: ]", "");
    }

    private static Timestamp getDateTimeId() {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.MINUTE, -1);
      c.set(Calendar.SECOND, 0);
      c.set(Calendar.MILLISECOND, 0);

      return new Timestamp(c.getTime().getTime());
    }

    private static Timestamp getUpdateTime() {
      return new Timestamp(System.currentTimeMillis());
    }

    private static String getLastHourTimestamp() {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.HOUR_OF_DAY, -1);
      return DateUtil.getFormatDateTime(c.getTime(), "yyyyMMddHHmmss");
    }

    private static void sync() throws SQLException, IOException {
      pstmt0.setDate(1, date_id);
      pstmt0.setTimestamp(2, date_time_id);
      pstmt0.setTimestamp(10, getUpdateTime());

      // platform of each page type detail
      for (PlatformType pltfm : PlatformType.values()) {
        pstmt0.setInt(3, pltfm.getId());
        pstmt0.setString(4, pltfm.getLiteral());
        for (PageType page : PageType.values()) {
          pstmt0.setInt(5, page.getId());
          pstmt0.setString(6, page.getLiteral());
          for (Kpi kpi : Kpi.values()) {
            pstmt0.setInt(7, kpi.getId());
            pstmt0.setString(8, kpi.getName());
            pstmt0.setLong(9, getPlatformPageKpi(kpi, pltfm, page, timeStamp.substring(0, 12)));
            pstmt0.addBatch();
          }
        }
      }

      // each platform aggregating all page type
      for (PlatformType pltfm : PlatformType.values()) {
        pstmt0.setInt(3, pltfm.getId());
        pstmt0.setString(4, pltfm.getLiteral());
        pstmt0.setInt(5, -99);
        pstmt0.setString(6, "全站");
        for (Kpi kpi : Kpi.values()) {
          if (!kpi.equals(Kpi.VU)) {
            pstmt0.setInt(7, kpi.getId());
            pstmt0.setString(8, kpi.getName());
            pstmt0.setLong(9, getPlatformKpi(kpi, pltfm, timeStamp.substring(0, 12)));
            pstmt0.addBatch();
          }
        }
      }
      pstmt0.executeBatch();
      conn.commit();
    }

    public static void update() throws SQLException, IOException {
      Calendar c = Calendar.getInstance();
      c.set(Calendar.SECOND, 0);
      c.set(Calendar.MILLISECOND, 0);
      pstmt1.setTimestamp(2, getUpdateTime());
      for (int i = 0; i < UPDATE_INTERVAL; c.add(Calendar.MINUTE, -1), i++) {
        Date d = c.getTime();
        timeStamp = DateUtil.getFormatDateTime(d, "yyyyMMddHHmmss");
        pstmt1.setTimestamp(3, new Timestamp(d.getTime()));

        // platform of each page type detail
        for (PlatformType pltfm : PlatformType.values()) {
          pstmt1.setInt(4, pltfm.getId());
          for (PageType page : PageType.values()) {
            pstmt1.setInt(5, page.getId());
            for (Kpi kpi : Kpi.values()) {
              pstmt1.setInt(6, kpi.getId());
              pstmt1.setLong(1, getPlatformPageKpi(kpi, pltfm, page, timeStamp.substring(0, 12)));
              pstmt1.addBatch();
            }
          }
        }

        // each platform aggregating all page type
        for (PlatformType pltfm : PlatformType.values()) {
          pstmt1.setInt(4, pltfm.getId());
          pstmt1.setInt(5, -99);
          for (Kpi kpi : Kpi.values()) {
            if (!kpi.equals(Kpi.VU)) {
              pstmt1.setInt(6, kpi.getId());
              pstmt1.setLong(1, getPlatformKpi(kpi, pltfm, timeStamp.substring(0, 12)));
              pstmt1.addBatch();
            }
          }
        }
      }
      pstmt1.executeBatch();
      conn.commit();
    }

    private static long getPlatformKpi(Kpi kpi, PlatformType pltfm, String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return viewer.getKpiValue(kpi, pltfm, timeStamp);
    }

    private static long getPlatformPageKpi(Kpi kpi, PlatformType pltfm, PageType page,
                                           String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return viewer.getKpiValue(kpi, pltfm, page, timeStamp);
    }

    public static double rate(long n, long d) {
      return d == 0L ? 0 : (double) n / d;
    }


    public static void close() {
      try {
        pstmt0.close();
        pstmt1.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    public static void main(String[] args) {
      try {
//        Calendar c = Calendar.getInstance();
//        if (c.get(Calendar.MINUTE) == 0) {
//          if (c.get(Calendar.HOUR_OF_DAY) == 0) {
//            clearHourTracker();
//          }
//        }
        sync();
        update();

      } catch (SQLException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        close();
      }
    }

    private static void usage() {
      System.out.println("Command : TopoName[BI_Needle] ");
    }
  }
}

