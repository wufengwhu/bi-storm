package com.yihaodian.bi.storm.logic.tracker;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import joptsimple.internal.Strings;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.cached.HBaseRepresentor;
import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.common.StatConstants.CacheStat;
import com.yihaodian.bi.database.DBConnFactory;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.TopoGroup;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.ChannelResult;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.PlatformType;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.Tag;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.VipType;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.VisiterType;
import com.yihaodian.common.DateUtil;

public class NeedlePackage {

  public static final String USER_ = "user_";
  public static final String PROV_ = "prov_";
  public static final String CHNL_ = "chnl_";
  public static final String GLOB_ = "glob_";
  public static final String HOUR_ = "hour_";
  public static final String _USER_ = "_user_";
  public static final String _PROV_ = "_prov_";
  public static final String _CHNL_ = "_chnl_";
  public static final String _VIP_ = "_vip_";
  public static final String EXIST = "1";

  public static final String _ = "_";

  public static class UvLogicBolt extends BaseStat implements IRichBolt {

    private static boolean _tran = false;

    public UvLogicBolt(TopoGroup tag) {
      super(tag);
    }

    public UvLogicBolt(TopoGroup tag, boolean tran) {
      super(tag);
      _tran = tran;
    }

    private static final long serialVersionUID = 4195223561461461950L;

    private static final Logger LOG = LoggerFactory.getLogger(UvLogicBolt.class);

    private CacheStat stat;

    OutputCollector _collector;

    private CMap cache;
    private Tag[] preStatus = new Tag[3];                                // pltmId,provId,chnlId
    private Tag[] curStatus = new Tag[3];                                // pltmId,provId,chnlId

    private TrackerVO t = new TrackerVO();
    private String timestamp;
    private String dateYYYYMMDDHHM0;
    private String dateYYYYMMDD;
    private String guid;
    private String statusKey;
    private String globStatusKey;
    private String globStatKey;
    private String globHourGuidSid;
    private String globHourGuid;
    private String globHourStatKey;
    private AtomicLong size = new AtomicLong(1);
    private AtomicLong time = new AtomicLong(1);
    private long lastTranPrintPoint = 0;
    long preTime;
    long curTime;

    private PlatformType pltm;
    private String prov;
    private ChannelResult chnl;

    AtomicInteger i = new AtomicInteger();

    @Override
    public void execute(Tuple input) {
      stat.intervalWriteAndClean(true);
      stat.incr(stat.TRACKER_BOLT_RECV);

      // 去掉button_link
      if (TrackerLogicUtils.isBtnLink(input.getStringByField("buttonPos"))) return;

      try {
        // size.incrementAndGet();
        init(input);

        pltm = TrackerLogicUtils.getPlatformType(t);
        prov = TrackerLogicUtils.provId(t);
        chnl = TrackerLogicUtils.channel(t);

        if (cache.get(statusKey) == null) { // 非有效
          cache.put(statusKey, StringUtils.join(new String[]{
                  new Tag(String.valueOf(pltm.getType()), timestamp, 1).toString(),
                  new Tag(prov, timestamp, 1).toString(),
                  new Tag(chnl.getVal(), timestamp, 1, chnl.getType()).toString()}, _)); // 更新statusCache
        } else { // 有效（但需要去重）
          setPreState(cache.get(statusKey));
          setCurState(new Tag[]{new Tag(String.valueOf(pltm.getType()), timestamp, 0, chnl.getType()),
                  new Tag(prov, timestamp, 0, chnl.getType()),
                  new Tag(chnl.getVal(), timestamp, 0, chnl.getType())});

          updateCache();
        }
        // 计算H5，PC的UV值

        updateCacheH5PC(pltm);
        updateHourGlob();
        // time.addAndGet(System.currentTimeMillis() - st);
        // printTime();
        if (_tran) _collector.ack(input);
        stat.incr(stat.TRACKER_BOLT_SUCC);

        if (!stat.keepAlive()) {
          LOG.error("Bolt is not alive, turning off the SWITCHER...");
          stat.setSwitcher(false);
        }
      } catch (Exception e) {
        LOG.error("UvCacheBolt exception " + input.getMessageId(), e);
        if (_tran) _collector.fail(input);
        // throw new RuntimeException("UvCacheBolt runtime exception", e);
        stat.incr(stat.TRACKER_BOLT_FAIL);
      }
    }

    private void printTime() {
      if (System.currentTimeMillis() - lastTranPrintPoint > 5000) {
        System.out.println("Count=" + size.get() + ", Time=" + time.get() + ", AvgTime=" + time.get()
                / size.get());
        lastTranPrintPoint = System.currentTimeMillis();
      }
    }

    private void init(Tuple input) {
      guid = input.getStringByField("guid");
      t.setSession_id(input.getStringByField("sid"));
      t.setPlatform(input.getStringByField("pltfmId"));
      t.setExt_field2(input.getStringByField("provId"));
      t.setTrack_time(input.getStringByField("tracktime"));
      t.setTracker_u(input.getStringByField("tracku"));
      t.setReferer(input.getStringByField("referer"));
      t.setEnd_user_id(input.getStringByField("uid"));
      t.setUrl(input.getStringByField("url"));
      t.setContainer(input.getStringByField("container"));
      t.setPagetypeid(input.getStringByField("pagetypeId"));

      timestamp = t.getTrack_time().replaceAll("[-: ]", "");
      dateYYYYMMDD = timestamp.substring(0, 8); // 全局窗口（20150724）
      dateYYYYMMDDHHM0 = timestamp.substring(0, 11) + "0"; // win为时间窗口（10分钟）
      statusKey = dateYYYYMMDD + _ + guid + _ + t.getSession_id(); // 判断有效UV用
      globStatusKey = dateYYYYMMDD + _ + guid; // 去重用 yyyyMMdd_guid
      globStatKey = GLOB_ + dateYYYYMMDDHHM0; // 全局统计 glob_yyyyMMddHHM0
      String dateYYYYMMDDHH = timestamp.substring(0, 10); // 取到小时（2015072400）
      globHourGuidSid = dateYYYYMMDDHH + _ + guid + _ + t.getSession_id(); // yyyyMMddHH_guid_sessionId
      globHourGuid = dateYYYYMMDDHH + _ + guid; // yyyyMMddHH_guid
      globHourStatKey = HOUR_ + dateYYYYMMDDHH; // 小时统计 hour_yyyyMMddHH
    }

    private void updateHourGlob() throws Exception {
      if (cache.get(globHourGuidSid) == null) {
        cache.put(globHourGuidSid, EXIST);
      } else {
        if (cache.get(globHourGuid) == null) {
          cache.put(globHourGuid, EXIST);
          cache.incr(globHourStatKey, 1);
        }
      }
    }

    private void updateCacheH5PC(PlatformType pt) throws Exception {
      String usr_id = t.getEnd_user_id();
      if (cache.get(userStatusKey()) == null) { // guid 不存在
        if (usr_id != null && !usr_id.isEmpty()) { // 当前track已登陆
          cache.put(userStatusKey(), pt.getName() + "@1"); // user_timestamp_guid => pltfmId
          if (cache.get(vipStatusKey(pt.getLiteral(), usr_id)) == null) { // 该用户不存在
            cache.put(vipStatusKey(pt.getLiteral(), usr_id), EXIST);
            if (pt == PlatformType.H5 || pt == PlatformType.PC) {
              cache.incr(userKey(pt.getName(), VipType.VIP.getType(), dateYYYYMMDDHHM0), 1);
            }

          }
        } else { // 未登陆
          cache.put(userStatusKey(), pt.getName() + "@0");
          cache.incr(userKey(pt.getName(), VipType.NON_VIP.getType(), dateYYYYMMDDHHM0), 1);
        }
      } else {
        if (usr_id != null && !usr_id.isEmpty()) { // guid存在 当前已登陆
          if (cache.get(vipStatusKey(PlatformType.H5.getLiteral(), usr_id)) == null
                  && cache.get(vipStatusKey(PlatformType.PC.getLiteral(), usr_id)) == null
                  && cache.get(vipStatusKey(PlatformType.APP.getLiteral(), usr_id)) == null) { // 用户不存在于任何平台
            String[] tk = cache.get(userStatusKey()).split("@");
            String prePt = tk[0];
            String preLg = tk[1];
            cache.put(vipStatusKey(prePt, usr_id), EXIST);
            if ("0".equals(preLg)
                    && (PlatformType.H5.getName().equals(prePt) || PlatformType.PC.getName().equals(prePt))) {
              cache.incr(userKey(prePt, VipType.VIP.getType(), dateYYYYMMDDHHM0), 1);
              cache.incr(userKey(prePt, VipType.NON_VIP.getType(), dateYYYYMMDDHHM0), -1);

              cache.put(userStatusKey(), prePt + "@1");
            }
          }
        }
      }
    }

    private void updateCache() throws Exception {
      Tag[] update = new Tag[]{preStatus[0], preStatus[1], preStatus[2]};
      boolean flag = false;

      // 全站
      if (cache.get(globStatusKey) == null) { // 全局不存在该guid
        cache.put(globStatusKey, EXIST);
        cache.incr(globStatKey, 1);
      }

      // app有效UV
      String appStatusKey = userStatusKey(PlatformType.APP.getLiteral());
      if (curStatus[0].before(preStatus[0])) {
        if (PlatformType.APP.getName().equals(curStatus[0].getValue())) {
          if (cache.get(appStatusKey) == null) {
            cache.put(appStatusKey, "1");
            cache.incr(userKey(curStatus[0].getValue(), 1, dateYYYYMMDDHHM0), 1);
          }
        } else if (PlatformType.APP.getName().equals(preStatus[0].getValue())
                && cache.get(appStatusKey) != null) {
          cache.incr(userKey(preStatus[0].getValue(), 1, dateYYYYMMDDHHM0), -1);
        }
        update[0] = curStatus[0];
        flag = true;
      } else {
        if (preStatus[0].isFirst() && (PlatformType.APP.getName().equals(preStatus[0].getValue()))) {
          if (cache.get(appStatusKey) == null) {
            cache.put(appStatusKey, "1");
            cache.incr(userKey(preStatus[0].getValue(), 1, dateYYYYMMDDHHM0), 1);
            preStatus[0].setFirst(false);
            update[0] = preStatus[0];
            flag = true;
          }
        }
      }
      // 省份
      if (curStatus[1].before(preStatus[1])) {
        if (cache.get(provStatusKey(curStatus[1].getValue())) == null) {
          cache.put(provStatusKey(curStatus[1].getValue()), "1");
          cache.incr(provKey(curStatus[1].getValue(), dateYYYYMMDDHHM0), 1);
        } else {
          cache.incr(provKey(curStatus[1].getValue(), dateYYYYMMDDHHM0), 1);
          cache.incr(provKey(preStatus[1].getValue(), dateYYYYMMDDHHM0), -1);

        }
        update[1] = curStatus[1];
        flag = true;
      } else {
        if (preStatus[1].isFirst()) {
          if (cache.get(provStatusKey(preStatus[1].getValue())) == null) {
            cache.put(provStatusKey(preStatus[1].getValue()), "1");
            cache.incr(provKey(preStatus[1].getValue(), dateYYYYMMDDHHM0), 1);
            preStatus[1].setFirst(false);
            update[1] = preStatus[1];
            flag = true;
          }
        }
      }

      // 渠道
      if (curStatus[2].before(preStatus[2])) {
        if (curStatus[2].isTrackerU()) {
          if (cache.get(chnlStatusKey(curStatus[2].getValue())) == null) {
            cache.put(chnlStatusKey(curStatus[2].getValue()), "1");
            cache.incr(chnlKey(curStatus[2].getValue(), dateYYYYMMDDHHM0), 1);
            update[2] = curStatus[2];
            flag = true;
          }
        }
      } else {
        if (preStatus[2].isFirst()) {
          if (curStatus[2].isTrackerU() && !preStatus[2].isTrackerU()) {
            if (cache.get(chnlStatusKey(curStatus[2].getValue())) == null) {
              cache.put(chnlStatusKey(curStatus[2].getValue()), "1");
              cache.incr(chnlKey(curStatus[2].getValue(), dateYYYYMMDDHHM0), 1);

              update[2] = curStatus[2];
              flag = true;
            }
          } else {
            if (cache.get(chnlStatusKey(preStatus[2].getValue())) == null) {
              cache.put(chnlStatusKey(preStatus[2].getValue()), "1");
              cache.incr(chnlKey(preStatus[2].getValue(), dateYYYYMMDDHHM0), 1);
              preStatus[2].setFirst(false);
              update[2] = preStatus[2];
              flag = true;
            }
          }
        } else {
          if (curStatus[2].isTrackerU() && !preStatus[2].isTrackerU()) {
            if (cache.get(chnlStatusKey(curStatus[2].getValue())) == null) {
              cache.put(chnlStatusKey(curStatus[2].getValue()), "1");
              cache.incr(chnlKey(curStatus[2].getValue(), dateYYYYMMDDHHM0), 1);
              cache.incr(chnlKey(preStatus[2].getValue(), dateYYYYMMDDHHM0), -1);

              update[2] = curStatus[2];
              flag = true;
            }
          }
        }

      }

      if (flag) {
        cache.put(statusKey, StringUtils.join(update, _));
      }
    }

    private String userKey(String pltfmId, int login, String ts) {
      return USER_ + pltfmId + _ + login + _ + ts;
    }

    private String provKey(String provId, String ts) {
      return PROV_ + provId + _ + ts;
    }

    private String chnlKey(String provId, String ts) {
      return CHNL_ + provId + _ + ts;
    }

    private String userStatusKey(String pltfmId) {
      return globStatusKey + _USER_ + pltfmId;
    }

    private String userStatusKey() {
      return USER_ + globStatusKey;
    }

    private String vipStatusKey(String pltfmId, String userId) {
      return dateYYYYMMDD + _VIP_ + pltfmId + _ + userId;
    }

    private String provStatusKey(String provId) {
      return globStatusKey + _PROV_ + provId;
    }

    private String chnlStatusKey(String chnlId) {
      return globStatusKey + _CHNL_ + chnlId;
    }

    private void setPreState(String vals) {
      String[] s = StringUtils.splitPreserveAllTokens(vals, _);

      for (int i = 0; i < 3; i++) {
        if (s[i] == null || s[i].isEmpty()) LOG.warn(vals);
        else this.preStatus[i] = Tag.parseTag(s[i]);
      }
    }

    private void setCurState(Tag[] vals) {
      for (int i = 0; i < 3; i++) {
        this.curStatus[i] = vals[i];
      }
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
      _collector = collector;
      try {
        TrackerLogicUtils.initNeedleCache();
        cache = TrackerLogicUtils.getNeedleHBaseAsyncLRU();
        stat = new CacheStat(this);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }

      preTime = System.currentTimeMillis();

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
      return null;
    }

    @Override
    public void cleanup() {
    }

  }

  public static class Sync2DB {

    static StatConstants.KafkaSpoutStat stat;
    static final long ONE_MIN = 60 * 1000L;
    static final long ONE_HOUR = 60 * 60 * 1000L;
    static final int UPDATE_HOURS = 12;
    static final int STAT_TYPE_GLOB = 1;
    static final int STAT_TYPE_USER = 2;
    static final int STAT_TYPE_NAVI = 3;
    static final int STAT_TYPE_PROV = 4;
    static final int STAT_TYPE_CHNL = 5;
    static final String CLEAR_HOUR_TRACKER = "delete from rpt.rpt_realtime_trfc_hour_s";
    static final String CLEAR_INDEX_TRACKER = "delete from rpt.rpt_realtime_trfc_s";
    static final String QUERY_PROV_ID = "select prov_id from dw.dim_prov where cur_flag =1 and enabl_flag = 1";
    static final String QUERY_CHNL_ID = "select distinct chanl_categ_lvl1_id from dw.cde_tfc_chanl";
    static Connection conn;
    static PreparedStatement pstmt0;
    static PreparedStatement pstmt1;
    static PreparedStatement pstmt2;
    static String INSERT_HOUR_TRACKER = "insert into rpt.rpt_realtime_trfc_hour_s values(?,?,?,?)";
    static String INSERT_INDEX_TRACKER = "insert into rpt.rpt_realtime_trfc_s values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    static String UPDATE_HOUR_TRACKER = "update rpt.rpt_realtime_trfc_hour_s set globl_sec_uv = ? , updt_time = ? where  date_id = ? and hour_id = ?";

    static DataViewer uvViewer;
    static java.sql.Date date_id;
    static java.sql.Date date_id_lasthour;
    static Timestamp date_time_id;
    static String timeStamp;
    static String yestodayTimestamp;
    static String lastWeekTimestamp;
    static String lastHourTimestamp;

    static {
      try {
        init();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private static void init() throws Exception {
      conn = DBConnFactory.getConnection("oracle").getConnection();
      conn.setAutoCommit(false);

      timeStamp = DateUtil.getCurrentDateString("yyyyMMddHHmmss");
      lastHourTimestamp = getLastHourTimestamp();
      yestodayTimestamp = DateUtil.getFormatYestoday("yyyyMMddHHmm");
      lastWeekTimestamp = DateUtil.getFormatCurrentAdd(-7, "yyyyMMddHHmm");

      date_id = new java.sql.Date(System.currentTimeMillis());
      date_id_lasthour = new java.sql.Date(System.currentTimeMillis() - ONE_HOUR);
      date_time_id = getDateTimeId();
      pstmt0 = conn.prepareStatement(INSERT_INDEX_TRACKER);
      pstmt1 = conn.prepareStatement(INSERT_HOUR_TRACKER);
      pstmt2 = conn.prepareStatement(UPDATE_HOUR_TRACKER);
      if (!Debug.DEBUG) uvViewer = new DataViewer();
    }

    private static Timestamp getDateTimeId() {
      long curTime = System.currentTimeMillis();
      return new Timestamp(curTime - curTime % ONE_MIN);
    }

    private static Timestamp getUpdateTime() {
      return new Timestamp(System.currentTimeMillis());
    }

    private static String getLastHourTimestamp() {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.HOUR_OF_DAY, -1);
      return DateUtil.getFormatDateTime(c.getTime(), "yyyyMMddHHmmss");
    }

    public static void syncLastHourTracker() throws SQLException, IOException {

      pstmt1.setDate(1, date_id_lasthour);
      pstmt1.setLong(2, Long.parseLong(lastHourTimestamp.substring(8, 10)) + 1);
      pstmt1.setLong(3, getGlobHourTracker(lastHourTimestamp));
      pstmt1.setTimestamp(4, getUpdateTime());

      pstmt1.executeUpdate();
      conn.commit();
    }

    public static void updateHourTracker() throws SQLException, IOException {
      Date date = DateUtil.getDate(lastHourTimestamp, "yyyyMMddHHmmss");
      Calendar c = Calendar.getInstance();
      c.setTime(date);
      for (int i = 0; i < UPDATE_HOURS; i++) {
        Date d = c.getTime();
        pstmt2.setLong(1, getGlobHourTracker(DateUtil.getFormatDateTime(d, "yyyyMMddHHmmss")));
        pstmt2.setTimestamp(2, getUpdateTime());
        pstmt2.setDate(3, new java.sql.Date(d.getTime()));
        pstmt2.setLong(4, Long.parseLong(DateUtil.getHours(d)) + 1);
        pstmt2.addBatch();

        c.add(Calendar.HOUR_OF_DAY, -1);
      }

      pstmt2.executeBatch();
      conn.commit();
    }

    public static void syncHourTracker() throws SQLException, IOException {
      syncLastHourTracker();
      updateHourTracker();
    }

    public static void syncIndexTracker() throws SQLException, IOException {
      clearIndexTracker();
      syncGlobTracker();
      syncPlfmTracker();
      syncProvTracker();
      syncChnlTracker();
      conn.commit();
    }

    public static void clearIndexTracker() throws SQLException {
      conn.createStatement().executeUpdate(CLEAR_INDEX_TRACKER);
    }

    private static void syncGlobTracker() throws SQLException, IOException {
      pstmt0.setDate(1, date_id);
      pstmt0.setTimestamp(2, date_time_id);
      pstmt0.setInt(3, STAT_TYPE_GLOB);
      pstmt0.setString(4, "全站有效UV");
      pstmt0.setString(5, "-99");
      pstmt0.setString(6, "-99");
      pstmt0.setInt(7, -99);
      pstmt0.setTimestamp(15, getUpdateTime());
      pstmt0.setInt(16, -99);

      long curVal;
      long yestodayVal;
      long lastWeekVal;
      long yestodayTotalVal;
      long lastWeekTotalVal;

      curVal = getGlobTracker(timeStamp);
      yestodayVal = getGlobTracker(yestodayTimestamp);
      lastWeekVal = getGlobTracker(lastWeekTimestamp);
      yestodayTotalVal = getGlobTracker(yestodayTimestamp.substring(0, 8) + "2359");
      lastWeekTotalVal = getGlobTracker(lastWeekTimestamp.substring(0, 8) + "2359");
      pstmt0.setLong(8, curVal);
      pstmt0.setLong(9, yestodayVal);
      if (yestodayVal == 0) {
        pstmt0.setFloat(10, 0);
      } else {
        pstmt0.setFloat(10, ((float) curVal - (float) yestodayVal) / (float) yestodayVal);
      }
      pstmt0.setLong(11, lastWeekVal);
      if (lastWeekVal == 0) {
        pstmt0.setFloat(12, 0);
      } else {
        pstmt0.setFloat(12, ((float) curVal - (float) lastWeekVal) / (float) lastWeekVal);
      }
      pstmt0.setLong(13, yestodayTotalVal);
      pstmt0.setLong(14, lastWeekTotalVal);

      pstmt0.executeUpdate();
      pstmt0.clearParameters();
    }

    /**
     * This is a temporary method for changing of lvl1 channel id.
     */
    private static String mapChnl(String o) {
      if ("9".equals(o)) return "1003";
      else if ("1".equals(o)) return "1004";
      else if ("10".equals(o)) return "1005";
      else if ("4".equals(o)) return "1007";
      else if ("5".equals(o)) return "1008";
      else if ("7".equals(o)) return "1010";
      else return o;
    }

    public static void syncChnlTracker() throws SQLException, IOException {
      pstmt0.setDate(1, date_id);
      pstmt0.setTimestamp(2, date_time_id);
      pstmt0.setInt(3, STAT_TYPE_CHNL);
      pstmt0.setString(4, "全站有效UV");
      pstmt0.setString(5, "-99");
      pstmt0.setString(6, "-99");
      pstmt0.setInt(7, -99);
      pstmt0.setTimestamp(15, getUpdateTime());

      long curVal;
      long yestodayVal;
      long lastWeekVal;
      long yestodayTotalVal;
      long lastWeekTotalVal;

      ResultSet res = conn.createStatement().executeQuery(QUERY_CHNL_ID);

      while (res.next()) {
        String chnlId = res.getString(1);
        curVal = getChnlTracker(timeStamp, (chnlId)) +
                getChnlTracker(timeStamp, mapChnl(chnlId));  // this line is a temporary solution
        yestodayVal = getChnlTracker(yestodayTimestamp, chnlId) +
                getChnlTracker(yestodayTimestamp, mapChnl(chnlId));  // this line is a temporary solution
        lastWeekVal = getChnlTracker(lastWeekTimestamp, chnlId) +
                getChnlTracker(lastWeekTimestamp, mapChnl(chnlId));  // this line is a temporary solution
        yestodayTotalVal = getChnlTracker(yestodayTimestamp.substring(0, 8) + "2359", chnlId) +
                getChnlTracker(yestodayTimestamp.substring(0, 8) + "2359", mapChnl(chnlId)); // this line is a temporary solution
        lastWeekTotalVal = getChnlTracker(lastWeekTimestamp.substring(0, 8) + "2359", chnlId) +
                getChnlTracker(lastWeekTimestamp.substring(0, 8) + "2359", mapChnl(chnlId)); // this line is a temporary solution

        pstmt0.setLong(8, curVal);
        pstmt0.setLong(9, yestodayVal);
        if (yestodayVal == 0) {
          pstmt0.setFloat(10, 0);
        } else {
          pstmt0.setFloat(10, ((float) curVal - (float) yestodayVal) / (float) yestodayVal);
        }
        pstmt0.setLong(11, lastWeekVal);
        if (lastWeekVal == 0) {
          pstmt0.setFloat(12, 0);
        } else {
          pstmt0.setFloat(12, ((float) curVal - (float) lastWeekVal) / (float) lastWeekVal);
        }
        pstmt0.setLong(13, yestodayTotalVal);
        pstmt0.setLong(14, lastWeekTotalVal);
        pstmt0.setInt(16, Integer.parseInt(chnlId));

        pstmt0.addBatch();
      }

      pstmt0.executeBatch();
      pstmt0.clearBatch();
      pstmt0.clearParameters();
    }

    private static long getChnlTracker(String timeStamp, String chnlId) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.chnlDot(timeStamp, chnlId);
    }

    private static void syncProvTracker() throws SQLException, IOException {
      pstmt0.setDate(1, date_id);
      pstmt0.setTimestamp(2, date_time_id);
      pstmt0.setInt(3, STAT_TYPE_PROV);
      pstmt0.setString(4, "全站有效UV");
      pstmt0.setString(5, "-99");
      pstmt0.setString(6, "-99");
      pstmt0.setTimestamp(15, getUpdateTime());
      pstmt0.setInt(16, -99);

      long curVal;
      long yestodayVal;
      long lastWeekVal;
      long yestodayTotalVal;
      long lastWeekTotalVal;

      ResultSet res = conn.createStatement().executeQuery(QUERY_PROV_ID);

      while (res.next()) {
        String provId = res.getString(1);
        curVal = getProvTracker(timeStamp, provId);
        yestodayVal = getProvTracker(yestodayTimestamp, provId);
        lastWeekVal = getProvTracker(lastWeekTimestamp, provId);
        yestodayTotalVal = getProvTracker(yestodayTimestamp.substring(0, 8) + "2359", provId);
        lastWeekTotalVal = getProvTracker(lastWeekTimestamp.substring(0, 8) + "2359", provId);

        pstmt0.setInt(7, Integer.parseInt(provId));
        pstmt0.setLong(8, curVal);
        pstmt0.setLong(9, yestodayVal);
        if (yestodayVal == 0) {
          pstmt0.setFloat(10, 0);
        } else {
          pstmt0.setFloat(10, ((float) curVal - (float) yestodayVal) / (float) yestodayVal);
        }
        pstmt0.setLong(11, lastWeekVal);
        if (lastWeekVal == 0) {
          pstmt0.setFloat(12, 0);
        } else {
          pstmt0.setFloat(12, ((float) curVal - (float) lastWeekVal) / (float) lastWeekVal);
        }
        pstmt0.setLong(13, yestodayTotalVal);
        pstmt0.setLong(14, lastWeekTotalVal);

        pstmt0.addBatch();
      }

      pstmt0.executeBatch();
      pstmt0.clearBatch();
      pstmt0.clearParameters();
    }

    private static long getProvTracker(String timeStamp, String provId) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.provDot(timeStamp, provId);
    }

    private static void syncPlfmTracker() throws SQLException, IOException {
      pstmt0.setDate(1, date_id);
      pstmt0.setTimestamp(2, date_time_id);
      pstmt0.setInt(3, STAT_TYPE_USER);
      pstmt0.setString(6, "-99");
      pstmt0.setInt(7, -99);
      pstmt0.setTimestamp(15, getUpdateTime());
      pstmt0.setInt(16, -99);

      long curVal;
      long yestodayVal;
      long lastWeekVal;
      long yestodayTotalVal;
      long lastWeekTotalVal;

      for (VisiterType t : VisiterType.values()) {
        curVal = getUserTracker(t, timeStamp);
        yestodayVal = getUserTracker(t, yestodayTimestamp);
        lastWeekVal = getUserTracker(t, lastWeekTimestamp);
        yestodayTotalVal = getUserTracker(t, yestodayTimestamp.substring(0, 8) + "2359");
        lastWeekTotalVal = getUserTracker(t, lastWeekTimestamp.substring(0, 8) + "2359");
        pstmt0.setString(4, t.getName());
        pstmt0.setString(5, t.getType());
        pstmt0.setLong(8, curVal);
        pstmt0.setLong(9, yestodayVal);
        if (yestodayVal == 0) {
          pstmt0.setFloat(10, 0);
        } else {
          pstmt0.setFloat(10, ((float) curVal - (float) yestodayVal) / (float) yestodayVal);
        }
        pstmt0.setLong(11, lastWeekVal);
        if (lastWeekVal == 0) {
          pstmt0.setFloat(12, 0);
        } else {
          pstmt0.setFloat(12, ((float) curVal - (float) lastWeekVal) / (float) lastWeekVal);
        }
        pstmt0.setLong(13, yestodayTotalVal);
        pstmt0.setLong(14, lastWeekTotalVal);

        pstmt0.addBatch();
      }

      pstmt0.executeBatch();
      pstmt0.clearBatch();
      pstmt0.clearParameters();
    }

    public static void syncHistoryTracker(String dateId) throws SQLException, IOException {
      Date date = DateUtil.getDate(dateId, "yyyy-MM-dd");
      Calendar c = Calendar.getInstance();
      c.setTime(date);
      c.add(Calendar.SECOND, -1);
      for (int i = 0; i < 24; i++) {
        c.add(Calendar.HOUR_OF_DAY, 1);
        Date d = c.getTime();
        timeStamp = DateUtil.getFormatDateTime(d, "yyyyMMddHHmmss");
        lastHourTimestamp = timeStamp;
        yestodayTimestamp = DateUtil.getFormatDateAdd(d, -1, "yyyyMMddHHmmss");
        lastWeekTimestamp = DateUtil.getFormatDateAdd(d, -7, "yyyyMMddHHmmss");
        date_id = new java.sql.Date(d.getTime());
        date_id_lasthour = new java.sql.Date(d.getTime());
        date_time_id = new java.sql.Timestamp(d.getTime());

        syncLastHourTracker();
        syncIndexTracker();
      }

    }

    public static Long getGlobTracker(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.globDot(timeStamp);
    }

    public static long getGlobHourTracker(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.getGlobHour(timeStamp);
    }

    public static Long getUserTracker(VisiterType type, String timeStamp) throws IOException {
      if (type == VisiterType.APP) return getApp(timeStamp);
      else if (type == VisiterType.H5_VIP) return getH5Member(timeStamp);
      else if (type == VisiterType.H5_NON_VIP) return getH5Guest(timeStamp);
      else if (type == VisiterType.PC_VIP) return getPCMember(timeStamp);
      else return getPCGuest(timeStamp);
    }

    public static Long getApp(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.userDot(timeStamp, PlatformType.APP.getName(), "1");
    }

    public static Long getH5Member(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.userDot(timeStamp, PlatformType.H5.getName(), "1");
    }

    public static Long getH5Guest(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.userDot(timeStamp, PlatformType.H5.getName(), "0");
    }

    public static Long getPCMember(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.userDot(timeStamp, PlatformType.PC.getName(), "1");
    }

    public static Long getPCGuest(String timeStamp) throws IOException {
      if (Debug.DEBUG) return 100L;

      return uvViewer.userDot(timeStamp, PlatformType.PC.getName(), "0");
    }

    public static void close() {
      try {
        pstmt0.close();
        pstmt1.close();
        pstmt2.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    public static void main(String[] args) {
      try {
        if (args.length == 0) {
          if (Calendar.getInstance().get(Calendar.MINUTE) == 0) {
            syncLastHourTracker();
          }

          syncIndexTracker();
          updateHourTracker();

        } else {
          syncHistoryTracker(args[0]);
        }
      } catch (SQLException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        close();
      }
    }

    private static void usage() {
      System.out.println("Command : TopoName[BI_Needle] ");
    }
  }

  public static class DataViewer extends HBaseRepresentor {

    static final String USER_ = "user_";
    static final String PROV_ = "prov_";
    static final String CHNL_ = "chnl_";
    static final String GLOB_ = "glob_";
    static final String HOUR_ = "hour_";
    static final String _ = "_";

    private static final Logger LOG = LoggerFactory.getLogger(DataViewer.class);

    /**
     * 绘制某一时间点的，某平台的有效Uv （H5，PC包括非有效Uv）
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param pltmId    平台Id
     * @param login     登陆标志，1为登陆，0为未登陆
     * @return <k,v>对组成的结果集
     * @throws IOException
     */
    public long userDot(String timeStamp, String pltmId, String login) throws IOException {
      String win = timeStamp.substring(0, 8);
      String startRow = USER_ + pltmId + _ + login + _ + win;
      String endRow = USER_ + pltmId + _ + login + _ + timeStamp;

      return super.sum(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一之间点之前的，某平台的有效Uv （H5，PC包括非有效Uv）
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param pltmId    平台Id
     * @param login     登陆标志，1为登陆，0为未登陆
     * @return <k,v>对组成的结果集
     */
    public Map<String, Long> userCurve(String timeStamp, String pltmId, String login) {
      String win = timeStamp.substring(0, 8);
      String startRow = USER_ + pltmId + _ + login + win;
      String endRow = USER_ + pltmId + _ + login + _ + timeStamp;

      return super.curve(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间点，某省份的有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param provId    省份Id
     * @return <k,v>对组成的结果集
     * @throws IOException
     */
    public long provDot(String timeStamp, String provId) throws IOException {
      String win = timeStamp.substring(0, 8);
      String startRow = PROV_ + provId + _ + win;
      String endRow = PROV_ + provId + _ + timeStamp;

      return super.sum(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间点之前的，某省份的有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param provId    省份Id
     * @return <k,v>对组成的结果集
     */
    public Map<String, Long> provCurve(String timeStamp, String provId) {
      String win = timeStamp.substring(0, 8);
      String startRow = PROV_ + provId + _ + win;
      String endRow = PROV_ + provId + _ + timeStamp;

      return super.curve(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间点的，某渠道的有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param chnlId    渠道Id
     * @return 由<k,v>对组成的结果集
     * @throws IOException
     */
    public long chnlDot(String timeStamp, String chnlId) throws IOException {
      String win = timeStamp.substring(0, 8);
      String startRow = CHNL_ + chnlId + _ + win;
      String endRow = CHNL_ + chnlId + _ + timeStamp;

      return super.sum(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间点的，某渠道的有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param chnlId    渠道Id
     * @return 由<k,v>对组成的结果集
     * @throws IOException
     */
    public long globHourDot(String timeStamp, String chnlId) throws IOException {
      String win = timeStamp.substring(0, 10);
      String key = HOUR_ + win;

      return super.get(HBaseConstant.Table.UV_CACHE_TABLE.tbName, key);
    }

    /**
     * 绘制某一之间点之前的，某渠道的有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @param chnlId    渠道Id
     * @return 由<k,v>对组成的结果集
     */
    public Map<String, Long> chnlCurve(String timeStamp, String chnlId) {
      String win = timeStamp.substring(0, 8);
      String startRow = CHNL_ + chnlId + _ + win;
      String endRow = CHNL_ + chnlId + _ + timeStamp;

      return super.curve(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间点的全站有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @return 由<k,v>对组成的结果集
     * @throws IOException
     */
    public long globDot(String timeStamp) throws IOException {
      String win = timeStamp.substring(0, 8);
      String startRow = GLOB_ + win;
      String endRow = GLOB_ + timeStamp;

      return super.sum(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 绘制某一时间区间的全站有效Uv
     *
     * @param startTimeStamp 时间戳 i.e. 201507301200
     * @return 由<k,v>对组成的结果集
     * @throws IOException
     */
    public long globSum(String startTimeStamp, String endTimeStamp) throws IOException {
      String startRow = GLOB_ + startTimeStamp;
      String endRow = GLOB_ + endTimeStamp;

      return super.sum(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 取某一个时间点全站有效Uv
     *
     * @param
     * @return
     * @throws IOException
     */
    public long getGlobHour(String timestamp) throws IOException {
      String rowkey = HOUR_ + timestamp.substring(0, 10);

      return super.get(HBaseConstant.Table.UV_CACHE_TABLE.tbName, rowkey);
    }

    /**
     * 取某一个时间点全站有效Uv
     *
     * @param
     * @return
     * @throws IOException
     */
    public long getGlobLastHour(String timestamp) throws IOException {
      Date d = DateUtil.getDate(timestamp, "yyyyMMddHHmmss");
      Calendar c = Calendar.getInstance();
      c.setTime(d);
      c.add(Calendar.HOUR_OF_DAY, -1);
      String rowkey = HOUR_ + DateUtil.getFormatDateTime(c.getTime(), "yyyyMMddHH");
      return super.get(HBaseConstant.Table.UV_CACHE_TABLE.tbName, rowkey);
    }

    /**
     * 绘制某一时间点之前的全站有效Uv
     *
     * @param timeStamp 时间戳 i.e. 201507301200
     * @return 由<k,v>对组成的结果集
     */
    public Map<String, Long> globCurve(String timeStamp) {
      String win = timeStamp.substring(0, 8);
      String startRow = GLOB_ + win;
      String endRow = GLOB_ + timeStamp;

      return super.curve(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
    }

    /**
     * 功能函数 用于测试
     *
     * @param map
     */
    private static void traverseMap(Map<String, ?> map) {
      List<String> kvList = new ArrayList<String>();
      for (Map.Entry<?, ?> e : map.entrySet()) {
        kvList.add(e.getKey() + "=>" + e.getValue());
      }
      String[] kvArray = kvList.toArray(new String[0]);
      Arrays.sort(kvArray);
      for (int i = 0; i < kvArray.length; i++) {
        System.out.println(kvArray[i]);
      }
    }

    public static void main(String[] args) {
      DataViewer viewer = new DataViewer();
      Map<String, ?> map = null;
      if (args.length > 0) {
        if ("glob".equalsIgnoreCase(args[0])) {
          if (args.length >= 2) map = viewer.globCurve(args[1]);
        } else if ("user".equalsIgnoreCase(args[0])) {
          if (args.length >= 4) map = viewer.userCurve(args[1], args[2], args[3]);
        } else if ("prov".equalsIgnoreCase(args[0])) {
          if (args.length >= 3) map = viewer.provCurve(args[1], args[2]);
        } else if ("chnal".equalsIgnoreCase(args[0])) {
          if (args.length >= 2) map = viewer.chnlCurve(args[1], args[2]);
        }
      }
      if (map == null) {
        printUsage();
      } else {
        traverseMap(map);
      }
    }

    private static void printUsage() {
      System.out.println("glob [timestamp(201507301200)]");
      System.out.println("user [timeStamp(201507301200)] [pltmId] [login(0|1)]");
      System.out.println("prov [timeStamp(201507301200)] [provId]");
      System.out.println("chnal [timeStamp(201507301200)] [chnlIdLevel1]");
    }
  }
}
