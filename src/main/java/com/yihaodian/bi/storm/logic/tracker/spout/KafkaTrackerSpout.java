package com.yihaodian.bi.storm.logic.tracker.spout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import com.yihaodian.bi.common.StatConstants.KafkaSpoutStat;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.kafka.consumer.KafkaTrackerConsumerThread;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.TopoGroup;

/**
 * 类KafkaTrackerSpout.java的实现描述：TODO 类实现描述
 *
 * @author zhaoheng Jul 20, 2015 1:48:51 PM
 */
public class KafkaTrackerSpout extends BaseStat implements IRichSpout {

    public static class TranValue {

        public TranValue(String key, long currentTimeMillis, TrackerVO t) {
            this.key = key;
            this.value = t;
            this.startTime = currentTimeMillis;
        }

        String key;
        TrackerVO value;
        long startTime;
    }

    public static final Logger LOG = LoggerFactory.getLogger(KafkaTrackerSpout.class);
    public static final long serialVersionUID = -633828471355602266L;
    private KafkaTrackerConsumerThread kafka = null;
    private SpoutOutputCollector collector = null;
    private String kafkaGroupId = null;
    private KafkaSpoutStat stat = null;
    private int tranListMaxSize = 5000;
    private long lastTranPrintPoint = 0;
    private Map<String, TranValue> tranList;
    private AtomicLong tranMesgSize = new AtomicLong(1);
    private AtomicLong allSize = new AtomicLong(1);
    private AtomicLong tranAllTime = new AtomicLong(1);
    private AtomicLong msgACK = new AtomicLong(1);
    private AtomicLong msgFAIL = new AtomicLong(1);
    private boolean tran = false;

    public KafkaTrackerSpout(TopoGroup tag, boolean tran) {
        super(tag);
        this.kafkaGroupId = tag.getName().toLowerCase();
        this.tran = tran;
        if (tran) {
            tranList = new ConcurrentHashMap<String, TranValue>(tranListMaxSize);
        }
    }

    public KafkaTrackerSpout(TopoGroup tag, boolean tran, int tranListSize) {
        super(tag);
        this.kafkaGroupId = tag.getName().toLowerCase();
        this.tranListMaxSize = tranListSize;
        this.tran = tran;
        if (tran) {
            tranList = new ConcurrentHashMap<String, TranValue>(tranListMaxSize);
        }
    }

    public KafkaTrackerSpout(String groupid) {
        super();
        this.tag = groupid.toLowerCase();
        this.kafkaGroupId = groupid.toLowerCase();
    }

    @Override
    public void nextTuple() {
        //stat.intervalWriteAndClean(true);
        // if (tran && tranList.size() > tranListMaxSize) {
        // stat.incr(stat.KAFKA_TRACKER_SPOUT_TRAN_SKIP);
        // return;
        // }
        if (!kafka.getQueue().isEmpty()) {
            TrackerVO t;
            String id;
            String tracktime;
            String guid;
            String sid;
            String pltfmId;
            String uid;
            String provId;
            String tracku;
            String referer;
            String url;
            String container;
            String pagetypeId;
            String refpagetypeId;
            String orderCode;
            String posTypeId;
            String linkPos;
            String buttonPos;

            while (true) {
                t = kafka.getQueue().poll();
                if (t == null) break;
                guid = t.getGu_id();
                if (guid == null || guid.isEmpty()) break;
                id = t.getId();
                tracktime = t.getTrack_time();
                sid = t.getSession_id();
                pltfmId = t.getPlatform();
                uid = t.getEnd_user_id();
                provId = t.getExt_field2();
                tracku = t.getTracker_u();
                referer = t.getReferer();
                url = t.getUrl();
                container = t.getContainer();
                pagetypeId = t.getPagetypeid();
                refpagetypeId = t.getRefpagetypeid();
                orderCode = t.getOrder_code();
                posTypeId = t.getPositiontypeid();
                linkPos = t.getLink_position();
                buttonPos = t.getButton_position();

                stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_ALL);
                stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_MSG_ALL);
                if (tran) {
                    allSize.getAndIncrement();
                    tranList.put(id, new TranValue(id, System.currentTimeMillis(), t));
                }
                if (tran) {
                    tranMesgSize.incrementAndGet();
                    collector.emit(new Values(tracktime, guid, sid, pltfmId, uid, provId, tracku, referer, url,
                            container, pagetypeId), id);
                } else {
                    collector.emit(new Values(tracktime, guid, sid, pltfmId, uid, provId, tracku, referer, url,
                            container, pagetypeId, refpagetypeId, orderCode, posTypeId, linkPos, buttonPos));
                }
            }
        }
        try {
            if (!stat.keepAlive()) {
                LOG.info("SWITCHER is off. SPOUT is Stopping consuming from KAFKA...");
                close();
            }
        } catch (Exception e) {
            LOG.warn("Failed to check SPOUT hearbeat", e);
        }
        // printlnDebugInfo();
    }

    private void printlnDebugInfo() {
        if (System.currentTimeMillis() - lastTranPrintPoint > 5000) {
            lastTranPrintPoint = System.currentTimeMillis();

            if (tran) System.out.println("TranALLSize =" + allSize.get() + ", TranMsgSize=" + tranMesgSize.get()
                    + ", TranAllTime=" + tranAllTime.get() + " ,TranMsgAvgTime="
                    + (tranAllTime.get() / tranMesgSize.get()));
        }
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        try {
            init();
            kafka.setIsRun(true);
            stat = new KafkaSpoutStat(this);
            stat.setSwitcher(true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void init() throws IOException {
        kafka = new KafkaTrackerConsumerThread(kafkaGroupId, false);
        kafka.start();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("tracktime", "guid", "sid", "pltfmId", "uid", "provId", "tracku", "referer", "url",
                "container", "pagetypeId", "refpagetypeId", "orderCode", "posTypeId", "linkPos", "buttonPos"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

    @Override
    public void ack(Object msgId) {
        stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_MSG_SUCCESS);
        // LOG.info("Needle ACK:" + msgACK.getAndIncrement());
        // if (tran) {
        // TranValue tv = tranList.get(msgId.toString());
        // if (tv != null) {
        // stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_SUCCESS);
        // tranAllTime.addAndGet(System.currentTimeMillis() - tv.startTime);
        // tranList.remove(msgId.toString());
        // }
        // }
    }

    @Override
    public void activate() {

    }

    @Override
    public void close() {
        kafka.setIsRun(false);
    }

    @Override
    public void deactivate() {

    }

    @Override
    public void fail(Object msgId) {
        stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_MSG_FAIL);
        // LOG.info("Needle FAIL:" + msgFAIL.getAndIncrement());
        // if (tran) {
        // TranValue tv = tranList.get(msgId.toString());
        // TrackerVO t = tv.value;
        // if (tv != null) {
        // stat.incr(stat.KAFKA_TRACKER_SPOUT_EMIT_FAIL);
        //
        // String tracktime = t.getTrack_time();
        // String guid = t.getGu_id();
        // String sid = t.getSession_id();
        // String pltfmId = t.getPlatform();
        // String uid = t.getEnd_user_id();
        // String provId = t.getExt_field2();
        // String tracku = t.getTracker_u();
        // String referer = t.getReferer();
        // String url = t.getUrl();
        // String container = t.getContainer();
        // String pagetypeId = t.getPagetypeid();
        // collector.emit(new Values(tracktime, guid, sid, pltfmId, uid,
        // provId, tracku, referer, url, container,
        // pagetypeId), tv.key);
        // }
        // }
    }
}
