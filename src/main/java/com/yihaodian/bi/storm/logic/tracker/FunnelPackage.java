package com.yihaodian.bi.storm.logic.tracker;

import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.EXIST;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage.USER_;
import static com.yihaodian.bi.storm.logic.tracker.NeedlePackage._;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

import com.google.common.base.Strings;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.cached.HBaseRepresentor;
import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.database.DBConnFactory;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.TopoGroup;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils.PlatformType;
import com.yihaodian.common.DateUtil;

/**
 * Created by fengwu on 15/9/2.
 */
public class FunnelPackage {

    public static final String LAND_ = "land_";

    public static final String PAGE_ = "page_";

    public static final String DETL_ = "detl_";

    public static final String CMS_ = "cms_";

    public static final String KW_ = "kw_";

    public static final String CATEG_ = "categ_";

    public static final String CHNL_ = "chnl_";

    public static final String ORDER_ = "order_";

    public static final String CART_ = "cart_";

    public static final String ACDV_ = "acdv_"; // 主动流量

    public static final String CORE_ = "core_"; // 核心流量

    public static final String OWN_ = "own_";  // 自由流量

    public static final String RCMD_ = "rcmd_"; // 个性化推荐

    public static class FunnelLogicBolt extends BaseStat implements IRichBolt {

        private static final long serialVersionUID = 5105808468351052463L;

        private static final Logger LOG = LoggerFactory.getLogger(FunnelLogicBolt.class);

        private AbstractBaseCMap funnelCache;

        private AbstractBaseCMap pageCategCache;

        private String landStatKey;

        private String globStatusKey;

        private String cartUvKey;

        private String cartStatKey;

        private String orderStatKey;

        private String pageCategId;

        private StatConstants.CacheStat stat;

        private OutputCollector _collector;

        private TrackerVO t = new TrackerVO();

        private String timestamp;

        private String dateYYYYMMDDHH;

        private String guid;

        private String acdvUvKey;

        private String acdvUvStatKey;

        private String coreUvKey;

        private String coreUvStatKey;

        private String ownUvKey;

        private String ownUvStatKey;

        private String rcmdUvKey;

        private String rcmdUvStatKey;

        private String detlUvKey;

        private String detlUvStatKey;

        private TrackerLogicUtils.PlatformType pltm;

        private TrackerLogicUtils.ChannelResult chnl;

        private String cmsUvKey;

        private String cmsUvStatKey;

        private String kwUvKey;

        private String kwUvStatKey;

        private String categUvKey;

        private String categStatKey;

        private String chnlUvKey;

        private String chnlStatKey;

        private String cmsDetlUvKey;

        private String cmsDetlStatKey;

        private String kwDetlUvKey;

        private String kwDetlStatKey;

        private String categDetlUvKey;

        private String categDetlStatKey;

        private String chnlDetlUvKey;

        private String chnlDetlStatKey;

        private String orderStatusKey;

        private boolean isRcmdFlag;

        private String rcmdDetlUvKey;

        private String rcmdDetlStatKey;

        private String refpageCategId;

        public FunnelLogicBolt(TopoGroup tag) {
            super(tag);
        }

        @Override
        public void prepare(Map map, TopologyContext topologyContext, OutputCollector collector) {
            try {
                Long starts = System.currentTimeMillis();
                funnelCache = TrackerLogicUtils.getHBaseAsyncLRU(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, null);
                pageCategCache = TrackerLogicUtils.getPageTypeToCategHBaseLRUReadonly();
                stat = new StatConstants.CacheStat(this);
                TrackerLogicUtils.initFunnelCache();
                LOG.debug("HBaseAsyncLRU funnelCache init success and cost " + (System.currentTimeMillis() - starts));
            } catch (Exception e) {
                LOG.error("FunnelPackage.FunnelLogicBolt:get hbase table or funnelCache error", e);
                throw new RuntimeException(e);
            }
        }

        @Override
        public void execute(Tuple input) {
            try {
                stat.intervalWriteAndClean(true);
                stat.incr(stat.TRACKER_BOLT_RECV);
                init(input);

                if (!TrackerLogicUtils.isBtnLink(t.getButton_position())) { // 过滤button_link的流量
                    pltm = TrackerLogicUtils.getPlatformType(t);
                    chnl = TrackerLogicUtils.channelFunnel(t);

                    // 全站，
                    if (Strings.isNullOrEmpty(funnelCache.get(globStatusKey))) { // 还未统计
                        // glob landing page uv stats
                        funnelCache.put(globStatusKey, chnl.getVal() + _ + String.valueOf(chnl.getType()));
                        funnelCache.incr(landStatKey, 1L);
                        // PC, APP, H5 flow 三个平台的以GUID互斥， 总和为全站流量和
                        String pltStatKey = USER_ + pltm.getName() + _ + dateYYYYMMDDHH;
                        funnelCache.incr(pltStatKey, 1L);

                        // 自有 ，核心， 主动 根据渠道划分， 非互斥， 一个GUID存在多个渠道时，取最早的那个
                        if (TrackerLogicUtils.isAcdvUv(chnl.getVal())) {
                            if (funnelCache.get(acdvUvKey) == null) {
                                funnelCache.put(acdvUvKey, EXIST);
                                funnelCache.incr(acdvUvStatKey, 1);
                            }
                        }

                        if (TrackerLogicUtils.isCoreUv(chnl.getVal())) {
                            if (funnelCache.get(coreUvKey) == null) {
                                funnelCache.put(coreUvKey, EXIST);
                                funnelCache.incr(coreUvStatKey, 1);
                            }
                        }

                        if (TrackerLogicUtils.isOwnUv(chnl.getVal())) {
                            if (funnelCache.get(ownUvKey) == null) {
                                funnelCache.put(ownUvKey, EXIST);
                                funnelCache.incr(ownUvStatKey, 1);
                            }
                        }
                    } else {
                        String[] tk = StringUtils.splitPreserveAllTokens(funnelCache.get(globStatusKey), _);
                        if ("0".equals(tk[1]) && "2".equals(String.valueOf(chnl.getType()))) {
                            if (TrackerLogicUtils.isAcdvUv(chnl.getVal())) {
                                funnelCache.incr(acdvUvStatKey, 1);
                            }
                            if (TrackerLogicUtils.isCoreUv(chnl.getVal())) {
                                funnelCache.incr(coreUvStatKey, 1);
                            }
                            if (TrackerLogicUtils.isOwnUv(chnl.getVal())) {
                                funnelCache.incr(ownUvStatKey, 1);
                            }

                            if (TrackerLogicUtils.isAcdvUv(tk[0])) {
                                funnelCache.incr(acdvUvStatKey, -1);
                            }
                            if (TrackerLogicUtils.isCoreUv(tk[0])) {
                                funnelCache.incr(coreUvStatKey, -1);
                            }
                            if (TrackerLogicUtils.isOwnUv(tk[0])) {
                                funnelCache.incr(ownUvStatKey, -1);
                            }

                            funnelCache.put(globStatusKey, chnl.getVal() + _ + String.valueOf(chnl.getType()));
                        }
                    }

                    // stat landing pages stream next jump by search, category, CMS, channel, personal-recommendation,
                    if (!Strings.isNullOrEmpty(cmsUvKey) && funnelCache.get(cmsUvKey) == null) {
                        funnelCache.put(cmsUvKey, EXIST);
                        funnelCache.incr(cmsUvStatKey, 1L);
                    }

                    if (!Strings.isNullOrEmpty(kwUvKey) && funnelCache.get(kwUvKey) == null) {
                        funnelCache.put(kwUvKey, EXIST);
                        funnelCache.incr(kwUvStatKey, 1L);
                    }

                    if (!Strings.isNullOrEmpty(categUvKey) && funnelCache.get(categUvKey) == null) {
                        funnelCache.put(categUvKey, EXIST);
                        funnelCache.incr(categStatKey, 1L);
                    }

                    if (!Strings.isNullOrEmpty(chnlUvKey) && funnelCache.get(chnlUvKey) == null) {
                        funnelCache.put(chnlUvKey, EXIST);
                        funnelCache.incr(chnlStatKey, 1L);
                    }
                }

                // detl uv
                if (!Strings.isNullOrEmpty(detlUvKey) && funnelCache.get(detlUvKey) == null) {
                    funnelCache.put(detlUvKey, EXIST);
                    funnelCache.incr(detlUvStatKey, 1L);
                }

                // 跳转商祥统计
                if (!Strings.isNullOrEmpty(cmsDetlUvKey) && funnelCache.get(cmsDetlUvKey) == null) {
                    funnelCache.put(cmsDetlUvKey, EXIST);
                    funnelCache.incr(cmsDetlStatKey, 1);
                }

                if (!Strings.isNullOrEmpty(kwDetlUvKey) && funnelCache.get(kwDetlUvKey) == null) {
                    funnelCache.put(kwDetlUvKey, EXIST);
                    funnelCache.incr(kwDetlStatKey, 1L);
                }
                if (!Strings.isNullOrEmpty(categDetlUvKey) && funnelCache.get(categDetlUvKey) == null) {
                    funnelCache.put(categDetlUvKey, EXIST);
                    funnelCache.incr(categDetlStatKey, 1L);
                }
                if (!Strings.isNullOrEmpty(chnlDetlUvKey) && funnelCache.get(chnlDetlUvKey) == null) {
                    funnelCache.put(chnlDetlUvKey, EXIST);
                    funnelCache.incr(chnlDetlStatKey, 1L);
                }

                // cart uv
                if (!Strings.isNullOrEmpty(cartUvKey) && funnelCache.get(cartUvKey) == null) {
                    funnelCache.put(cartUvKey, EXIST);
                    funnelCache.incr(cartStatKey, 1L);
                }

                // rcmd => cart
                if (!Strings.isNullOrEmpty(rcmdDetlUvKey) && funnelCache.get(rcmdDetlUvKey) == null) {
                    funnelCache.put(rcmdDetlUvKey, EXIST);
                    funnelCache.incr(rcmdDetlStatKey, 1L);
                }

                // order
                if (!Strings.isNullOrEmpty(pageCategId) && ((pageCategId.equals("8") || pageCategId.equals("19")))) {
                    String orderCode = input.getStringByField("orderCode");
                    if (!Strings.isNullOrEmpty(orderCode)) {
                        orderStatusKey = ORDER_ + orderCode + dateYYYYMMDDHH;
                        if (funnelCache.get(orderStatusKey) == null) {
                            orderStatKey = ORDER_ + dateYYYYMMDDHH; // 订单统计key
                            funnelCache.put(orderStatusKey, EXIST);
                            funnelCache.incr(orderStatKey, 1L);
                        }
                    }
                }

                // 个性与精准化
                if (isRcmdFlag && funnelCache.get(rcmdUvKey) == null) {
                    funnelCache.put(rcmdUvKey, EXIST);
                    funnelCache.incr(rcmdUvStatKey, 1L);
                }
                stat.incr(stat.TRACKER_BOLT_SUCC);

            } catch (Exception e) {
                LOG.error("FunnelLogicBolt exception " + input.getMessageId(), e);
                stat.incr(stat.TRACKER_BOLT_FAIL);
            }
        }

        @Override
        public void cleanup() {

        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

        }

        @Override
        public Map<String, Object> getComponentConfiguration() {
            return null;
        }

        private void init(Tuple input) throws Exception {
            // 清空上次计算的状态
            clear();
            guid = input.getStringByField("guid");
            t.setSession_id(input.getStringByField("sid"));
            t.setPlatform(input.getStringByField("pltfmId"));
            t.setExt_field2(input.getStringByField("provId"));
            t.setTrack_time(input.getStringByField("tracktime"));
            t.setTracker_u(input.getStringByField("tracku"));
            t.setReferer(input.getStringByField("referer"));
            t.setEnd_user_id(input.getStringByField("uid"));
            t.setUrl(input.getStringByField("url"));
            t.setContainer(input.getStringByField("container"));
            t.setPagetypeid(input.getStringByField("pagetypeId"));
            t.setRefpagetypeid(input.getStringByField("refpagetypeId"));
            t.setLink_position(input.getStringByField("linkPos"));
            t.setButton_position(input.getStringByField("buttonPos"));
            t.setPositiontypeid(input.getStringByField("posTypeId"));

            timestamp = t.getTrack_time().replaceAll("[-: ]", "");
            dateYYYYMMDDHH = timestamp.substring(0, 10); // 全局窗口（2015072410）
            landStatKey = LAND_ + dateYYYYMMDDHH; // 全站landing page统计 land_yyyyMMddHHM0
            globStatusKey = dateYYYYMMDDHH + _ + guid; // 去重用 yyyyMMdd_guid
            acdvUvKey = ACDV_ + globStatusKey;
            coreUvKey = CORE_ + globStatusKey;
            ownUvKey = OWN_ + globStatusKey;
            rcmdUvKey = RCMD_ + globStatusKey;

            acdvUvStatKey = ACDV_ + dateYYYYMMDDHH;
            coreUvStatKey = CORE_ + dateYYYYMMDDHH;
            ownUvStatKey = OWN_ + dateYYYYMMDDHH;
            rcmdUvStatKey = RCMD_ + dateYYYYMMDDHH;
            isRcmdFlag = TrackerLogicUtils.isRcmdUv(t);

            // TODO page_categ_id根据orig_page_type_id到dw.dim_page_type中查
            pageCategId = Strings.isNullOrEmpty(t.getPagetypeid()) ? null : pageCategCache.get(t.getPagetypeid());
            refpageCategId = Strings.isNullOrEmpty(t.getRefpagetypeid()) ? null : pageCategCache.get(t.getRefpagetypeid());
            boolean isAddCart = TrackerLogicUtils.isAddCart(t);

            if (!Strings.isNullOrEmpty(pageCategId)) {
                if (pageCategId.equals("1")) {
                    cmsUvKey = CMS_ + globStatusKey;
                    cmsUvStatKey = CMS_ + dateYYYYMMDDHH;
                    // 点击加入购物车了
                    if (isAddCart) {
                        cmsDetlUvKey = CMS_ + DETL_ + globStatusKey;
                        cmsDetlStatKey = CMS_ + DETL_ + dateYYYYMMDDHH;
                    }
                } else if (pageCategId.equals("2")) {
                    kwUvKey = KW_ + globStatusKey;
                    kwUvStatKey = KW_ + dateYYYYMMDDHH;
                    if (isAddCart) {
                        kwDetlUvKey = KW_ + DETL_ + globStatusKey;
                        kwDetlStatKey = KW_ + DETL_ + dateYYYYMMDDHH;
                    }
                } else if (pageCategId.equals("3")) {
                    categUvKey = CATEG_ + globStatusKey;
                    categStatKey = CATEG_ + dateYYYYMMDDHH;
                    if (isAddCart) {
                        categDetlUvKey = CATEG_ + DETL_ + globStatusKey;
                        categDetlStatKey = CATEG_ + DETL_ + dateYYYYMMDDHH;
                    }
                } else if (pageCategId.equals("10")) {
                    chnlUvKey = CHNL_ + globStatusKey;
                    chnlStatKey = CHNL_ + dateYYYYMMDDHH;
                    if (isAddCart) {
                        chnlDetlUvKey = CHNL_ + DETL_ + globStatusKey;
                        chnlDetlStatKey = CHNL_ + DETL_ + dateYYYYMMDDHH;
                    }
                } else if (pageCategId.equals("7")) {
                    // 商详页统计key
                    detlUvKey = DETL_ + globStatusKey;
                    detlUvStatKey = DETL_ + dateYYYYMMDDHH;
                    // 跳转商详页 统计key
                    if (!Strings.isNullOrEmpty(refpageCategId)) {
                        if (refpageCategId.equals("1")) {
                            // cms_detl_uv
                            cmsDetlUvKey = CMS_ + DETL_ + globStatusKey;
                            cmsDetlStatKey = CMS_ + DETL_ + dateYYYYMMDDHH;
                        } else if (refpageCategId.equals("2")) {
                            // kw_detl_uv
                            kwDetlUvKey = KW_ + DETL_ + globStatusKey;
                            kwDetlStatKey = KW_ + DETL_ + dateYYYYMMDDHH;
                        } else if (refpageCategId.equals("3")) {
                            // categ_detl_uv
                            categDetlUvKey = CATEG_ + DETL_ + globStatusKey;
                            categDetlStatKey = CATEG_ + DETL_ + dateYYYYMMDDHH;
                        } else if (refpageCategId.equals("10")) {
                            chnlDetlUvKey = CHNL_ + DETL_ + globStatusKey;
                            chnlDetlStatKey = CHNL_ + DETL_ + dateYYYYMMDDHH;
                        }
                    }
                    if (isRcmdFlag) {
                        rcmdDetlUvKey = RCMD_ + DETL_ + globStatusKey;
                        rcmdDetlStatKey = RCMD_ + DETL_ + dateYYYYMMDDHH;
                    }
                }
            }

            // TODO 购物车 判断
            if (isAddCart) {
                // 加入购物车产生的商详页统计
                detlUvKey = DETL_ + globStatusKey;
                detlUvStatKey = DETL_ + dateYYYYMMDDHH;

                //
                if (isRcmdFlag) {
                    rcmdDetlUvKey = RCMD_ + DETL_ + globStatusKey;
                    rcmdDetlStatKey = RCMD_ + DETL_ + dateYYYYMMDDHH;
                }

                cartUvKey = CART_ + globStatusKey;
                cartStatKey = CART_ + dateYYYYMMDDHH;
            }
        }

        private void clear() {
            detlUvKey = null;
            cmsUvKey = null;
            kwUvKey = null;
            categUvKey = null;
            chnlUvKey = null;
            rcmdUvKey = null;
            cartUvKey = null;
            cmsDetlUvKey = null;
            kwDetlUvKey = null;
            categDetlUvKey = null;
            chnlDetlUvKey = null;
            rcmdDetlUvKey = null;
        }
    }

    /**
     * 类FunnelPackage.java的实现描述：DataViewer 类实现描述
     */
    public static class DataViewer extends HBaseRepresentor {

        private static final Logger LOG = LoggerFactory.getLogger(DataViewer.class);

        /**
         * 绘制某一时间点的，某平台的有效Uv （H5，PC包括非有效Uv）
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @param pltfm     平台Id
         * @param timeStamp 登陆标志，1为登陆，0为未登陆
         * @return <k,v>对组成的结果集
         * @throws IOException
         */
        public long userDot(PlatformType pltfm, String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = USER_ + pltfm.getName() + _ + win;
            String endRow = USER_ + pltfm.getName() + _ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点，落地页有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return <k,v>对组成的结果集
         * @throws IOException
         */
        public long landDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = LAND_ + win;
            String endRow = LAND_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的，某渠道的有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @param chnlId    渠道Id
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long chnlDot(String timeStamp, String chnlId) throws IOException {
            String win = timeStamp.substring(0, 8);
            String startRow = CHNL_ + chnlId + _ + win;
            String endRow = CHNL_ + chnlId + _ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的，某渠道的有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long acdvDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = ACDV_ + win;
            String endRow = ACDV_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long coreDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CORE_ + win;
            String endRow = CORE_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long ownDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = OWN_ + win;
            String endRow = OWN_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long cmsDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CMS_ + win;
            String endRow = CMS_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long kwDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = KW_ + win;
            String endRow = KW_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long categDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CATEG_ + win;
            String endRow = CATEG_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long chnlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CHNL_ + win;
            String endRow = CHNL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long rcmdDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = RCMD_ + win;
            String endRow = RCMD_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long cmsDetlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CMS_ + DETL_ + win;
            String endRow = CMS_ + DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long kwDetlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = KW_ + DETL_ + win;
            String endRow = KW_ + DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long categDetlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CATEG_ + DETL_ + win;
            String endRow = CATEG_ + DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long chnlDetlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CHNL_ + DETL_ + win;
            String endRow = CHNL_ + DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long rcmdDetlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = RCMD_ + DETL_ + win;
            String endRow = RCMD_ + DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long detlDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = DETL_ + win;
            String endRow = DETL_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long cartDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = CART_ + win;
            String endRow = CART_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点的全站有效Uv
         *
         * @param timeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         * @throws IOException
         */
        public long orderDot(String timeStamp) throws IOException {
            String win = timeStamp.substring(0, 10);
            String startRow = ORDER_ + win;
            String endRow = ORDER_ + timeStamp;

            return super.sum(HBaseConstant.Table.FUNNEL_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点之前的全站有效Uv
         *
         * @param startTimeStamp 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         */
        public Map<String, Long> curve(String prefix, String startTimeStamp, String endTimeStamp) {
            String startRow = prefix + startTimeStamp;
            String endRow = prefix + endTimeStamp;
            return super.curve(HBaseConstant.Table.UV_CACHE_TABLE.tbName, startRow, endRow);
        }

        /**
         * 绘制某一时间点之前的全站有效Uv
         *
         * @param prefix 时间戳 i.e. 201507301200
         * @return 由<k,v>对组成的结果集
         */
        public Map<String, Long> curve(String prefix) {
            String endTimeStamp = DateUtil.getFormatDateTime(new Date(), "yyyyMMddHH");
            String startTimeStamp = endTimeStamp.substring(0, 8);
            return this.curve(prefix, startTimeStamp, endTimeStamp);
        }

        /**
         * 功能函数 用于测试
         *
         * @param map
         */
        private static void traverseMap(Map<String, ?> map) {
            List<String> kvList = new ArrayList<String>();
            for (Map.Entry<?, ?> e : map.entrySet()) {
                kvList.add(e.getKey() + "=>" + e.getValue());
            }
            String[] kvArray = kvList.toArray(new String[0]);
            Arrays.sort(kvArray);
            for (int i = 0; i < kvArray.length; i++) {
                System.out.println(kvArray[i]);
            }
        }

        public static void main(String[] args) {
            DataViewer viewer = new DataViewer();
            Map<String, ?> map = null;
            if (args.length > 0) {
            }
            if (map == null) {
                printUsage();
            } else {
                traverseMap(map);
            }
        }

        private static void printUsage() {
            System.out.println("glob [timestamp(201507301200)]");
            System.out.println("user [timeStamp(201507301200)] [pltmId] [login(0|1)]");
            System.out.println("prov [timeStamp(201507301200)] [provId]");
            System.out.println("chnal [timeStamp(201507301200)] [chnlIdLevel1]");
        }
    }

    public static class Sync2DB {

        static StatConstants.KafkaSpoutStat stat;
        static final long ONE_MIN = 60 * 1000L;
        static final long ONE_HOUR = 60 * 60 * 1000L;
        static final int UPDATE_HOURS = 3;
        static final String CLEAR_HOUR_TRACKER = "delete from rpt.rpt_realtime_scrn_tfc_funnel_s";
        static Connection conn;
        static PreparedStatement pstmt0;
        static PreparedStatement pstmt1;
        static String INSERT_HOUR_TRACKER = "insert into rpt.rpt_realtime_scrn_tfc_funnel_s values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        static String UPDATE_HOUR_TRACKER = "update rpt.rpt_realtime_scrn_tfc_funnel_s\n"
                + "   set pc_uv              = ?,\n"
                + "       pc_uv_rate         = ?,\n"
                + "       h5_uv              = ?,\n"
                + "       h5_uv_rate         = ?,\n"
                + "       app_uv             = ?,\n"
                + "       app_uv_rate        = ?,\n"
                + "       acdv_uv            = ?,\n"
                + "       acdv_uv_rate       = ?,\n"
                + "       core_uv            = ?,\n"
                + "       core_uv_rate       = ?,\n"
                + "       own_uv             = ?,\n"
                + "       own_uv_rate        = ?,\n"
                + "       land_uv            = ?,\n"
                + "       kw_uv              = ?,\n"
                + "       kw_detl_uv_rate    = ?,\n"
                + "       categ_uv           = ?,\n"
                + "       categ_detl_uv_rate = ?,\n"
                + "       cms_uv             = ?,\n"
                + "       cms_detl_uv_rate   = ?,\n"
                + "       chnl_uv            = ?,\n"
                + "       chnl_detl_uv_rate  = ?,\n"
                + "       rcmd_uv            = ?,\n"
                + "       rcmd_detl_uv_rate  = ?,\n"
                + "       detl_uv            = ?,\n"
                + "       detl_cart_rate     = ?,\n"
                + "       cart_uv            = ?,\n"
                + "       cart_ordr_rate     = ?,\n"
                + "       ordr_num           = ?,\n"
                + "       etl_time           = ?\n"
                + " where date_id = ?\n" + "   and hour_id = ?";
        static DataViewer uvViewer;
        static java.sql.Date date_id;
        static java.sql.Date date_id_lasthour;
        static Timestamp date_time_id;
        static String timeStamp;
        static String yestodayTimestamp;
        static String lastWeekTimestamp;
        static String lastHourTimestamp;
        static int hour_id;

        static {
            try {
                init();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private static void init() throws Exception {
            conn = DBConnFactory.getConnection("oracle").getConnection();
            conn.setAutoCommit(false);

            timeStamp = DateUtil.getCurrentDateString("yyyyMMddHHmmss");
            lastHourTimestamp = getLastHourTimestamp();
            yestodayTimestamp = DateUtil.getFormatYestoday("yyyyMMddHHmm");
            lastWeekTimestamp = DateUtil.getFormatCurrentAdd(-7, "yyyyMMddHHmm");

            date_id = new java.sql.Date(System.currentTimeMillis());
            date_id_lasthour = new java.sql.Date(System.currentTimeMillis() - ONE_HOUR);
            date_time_id = getDateTimeId();
            hour_id = Integer.parseInt(timeStamp.substring(8, 10));
            pstmt0 = conn.prepareStatement(INSERT_HOUR_TRACKER);
            pstmt1 = conn.prepareStatement(UPDATE_HOUR_TRACKER);
            if (!Debug.DEBUG) uvViewer = new DataViewer();
        }

        private static Timestamp getDateTimeId() {
            long curTime = System.currentTimeMillis();
            return new Timestamp(curTime - curTime % ONE_MIN);
        }

        private static Timestamp getUpdateTime() {
            return new Timestamp(System.currentTimeMillis());
        }

        private static String getLastHourTimestamp() {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.HOUR_OF_DAY, -1);
            return DateUtil.getFormatDateTime(c.getTime(), "yyyyMMddHHmmss");
        }

        public static void clearHourTracker() throws SQLException {
            conn.createStatement().executeUpdate(CLEAR_HOUR_TRACKER);
        }

        private static void syncFunnel() throws SQLException, IOException {
            long landUv = getLandUv(timeStamp);
            long appUv = getUserUv(PlatformType.APP, timeStamp);
            long pcUv = getUserUv(PlatformType.PC, timeStamp);
            long h5Uv = getUserUv(PlatformType.H5, timeStamp);
            long acdvUv = getAcdvUv(timeStamp);
            long coreUv = getCoreUv(timeStamp);
            long ownUv = getOwnUv(timeStamp);
            long kwUv = getKwUv(timeStamp);
            long categUv = getCategUv(timeStamp);
            long cmsUv = getCmsUv(timeStamp);
            long chnlUv = getChnlUv(timeStamp);
            long rcmdUv = getRcmdUv(timeStamp);
            long detlUv = getDetlUv(timeStamp);
            long kwDetlUv = getKwDetlUv(timeStamp);
            long categDetlUv = getCategDetlUv(timeStamp);
            long cmsDetlUv = getCmsDetlUv(timeStamp);
            long chnlDetlUv = getChnlDetlUv(timeStamp);
            long rcmdDetlUv = getRcmdDetlUv(timeStamp);
            long cartUv = getCartUv(timeStamp);
            long orderNum = getOrderNum(timeStamp);

            pstmt0.setDate(1, date_id);
            pstmt0.setInt(2, hour_id);
            pstmt0.setLong(3, pcUv);
            pstmt0.setDouble(4, rate(pcUv, landUv));
            pstmt0.setLong(5, h5Uv);
            pstmt0.setDouble(6, rate(h5Uv, landUv));
            pstmt0.setLong(7, appUv);
            pstmt0.setDouble(8, rate(appUv, landUv));
            pstmt0.setLong(9, acdvUv);
            pstmt0.setDouble(10, rate(acdvUv, landUv));
            pstmt0.setLong(11, coreUv);
            pstmt0.setDouble(12, rate(coreUv, landUv));
            pstmt0.setLong(13, ownUv);
            pstmt0.setDouble(14, rate(ownUv, landUv));
            pstmt0.setLong(15, landUv);
            pstmt0.setLong(16, kwUv);
            pstmt0.setDouble(17, rate(kwDetlUv, kwUv));
            pstmt0.setLong(18, categUv);
            pstmt0.setDouble(19, rate(categDetlUv, categUv));
            pstmt0.setLong(20, cmsUv);
            pstmt0.setDouble(21, rate(cmsDetlUv, cmsUv));
            pstmt0.setLong(22, chnlUv);
            pstmt0.setDouble(23, rate(chnlDetlUv, chnlUv));
            pstmt0.setLong(24, rcmdUv);
            pstmt0.setDouble(25, rate(rcmdDetlUv, rcmdUv));
            pstmt0.setLong(26, detlUv);
            pstmt0.setDouble(27, rate(cartUv, detlUv));
            pstmt0.setLong(28, cartUv);
            pstmt0.setDouble(29, rate(orderNum, cartUv));
            pstmt0.setLong(30, orderNum);
            pstmt0.setTimestamp(31, getUpdateTime());
            pstmt0.executeUpdate();
            conn.commit();
        }

        public static double rate(long n, long d) {
            return d == 0L ? 0 : (double) n / d;
        }

        public static void updateFunnel() throws SQLException, IOException {
            Calendar c = Calendar.getInstance();
            for (int i = 0; i < UPDATE_HOURS; c.add(Calendar.HOUR_OF_DAY, -1), i++) {
                Date d = c.getTime();
                timeStamp = DateUtil.getFormatDateTime(d, "yyyyMMddHHmmss");
                long landUv = getLandUv(timeStamp);
                long appUv = getUserUv(PlatformType.APP, timeStamp);
                long pcUv = getUserUv(PlatformType.PC, timeStamp);
                long h5Uv = getUserUv(PlatformType.H5, timeStamp);
                long acdvUv = getAcdvUv(timeStamp);
                long coreUv = getCoreUv(timeStamp);
                long ownUv = getOwnUv(timeStamp);
                long kwUv = getKwUv(timeStamp);
                long categUv = getCategUv(timeStamp);
                long cmsUv = getCmsUv(timeStamp);
                long chnlUv = getChnlUv(timeStamp);
                long rcmdUv = getRcmdUv(timeStamp);
                long detlUv = getDetlUv(timeStamp);
                long kwDetlUv = getKwDetlUv(timeStamp);
                long categDetlUv = getCategDetlUv(timeStamp);
                long cmsDetlUv = getCmsDetlUv(timeStamp);
                long chnlDetlUv = getChnlDetlUv(timeStamp);
                long rcmdDetlUv = getRcmdDetlUv(timeStamp);
                long cartUv = getCartUv(timeStamp);
                long orderNum = getOrderNum(timeStamp);

                pstmt1.setLong(1, pcUv);
                pstmt1.setDouble(2, rate(pcUv, landUv));
                pstmt1.setLong(3, h5Uv);
                pstmt1.setDouble(4, rate(h5Uv, landUv));
                pstmt1.setLong(5, appUv);
                pstmt1.setDouble(6, rate(appUv, landUv));
                pstmt1.setLong(7, acdvUv);
                pstmt1.setDouble(8, rate(acdvUv, landUv));
                pstmt1.setLong(9, coreUv);
                pstmt1.setDouble(10, rate(coreUv, landUv));
                pstmt1.setLong(11, ownUv);
                pstmt1.setDouble(12, rate(ownUv, landUv));
                pstmt1.setLong(13, landUv);
                pstmt1.setLong(14, kwUv);
                pstmt1.setDouble(15, rate(kwDetlUv, kwUv));
                pstmt1.setLong(16, categUv);
                pstmt1.setDouble(17, rate(categDetlUv, categUv));
                pstmt1.setLong(18, cmsUv);
                pstmt1.setDouble(19, rate(cmsDetlUv, cmsUv));
                pstmt1.setLong(20, chnlUv);
                pstmt1.setDouble(21, rate(chnlDetlUv, chnlUv));
                pstmt1.setLong(22, rcmdUv);
                pstmt1.setDouble(23, rate(rcmdDetlUv, rcmdUv));
                pstmt1.setLong(24, detlUv);
                pstmt1.setDouble(25, rate(cartUv, detlUv));
                pstmt1.setLong(26, cartUv);
                pstmt1.setDouble(27, rate(orderNum, cartUv));
                pstmt1.setLong(28, orderNum);
                pstmt1.setTimestamp(29, getUpdateTime());
                pstmt1.setDate(30, new java.sql.Date(d.getTime()));
                pstmt1.setInt(31, Integer.parseInt(DateUtil.getHours(d)));
                pstmt1.addBatch();
            }
            pstmt1.executeBatch();
            conn.commit();
        }

        public static long getLandUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.landDot(timeStamp);
        }

        public static Long getUserUv(PlatformType pltfm, String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.userDot(pltfm, timeStamp);
        }

        public static long getAcdvUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.acdvDot(timeStamp);
        }

        public static long getCoreUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.coreDot(timeStamp);
        }

        public static long getOwnUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.ownDot(timeStamp);
        }

        public static long getKwUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.kwDot(timeStamp);
        }

        public static long getCategUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.categDot(timeStamp);
        }

        public static long getCmsUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.cmsDot(timeStamp);
        }

        public static long getChnlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.chnlDot(timeStamp);
        }

        public static long getRcmdUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.rcmdDot(timeStamp);
        }

        public static long getKwDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.kwDetlDot(timeStamp);
        }

        public static long getCategDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.categDetlDot(timeStamp);
        }

        public static long getCmsDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.cmsDetlDot(timeStamp);
        }

        public static long getChnlDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.chnlDetlDot(timeStamp);
        }

        public static long getRcmdDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.rcmdDetlDot(timeStamp);
        }

        public static long getDetlUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.detlDot(timeStamp);
        }

        public static long getCartUv(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.cartDot(timeStamp);
        }

        public static long getOrderNum(String timeStamp) throws IOException {
            if (Debug.DEBUG) return 100L;

            return uvViewer.orderDot(timeStamp);
        }

        public static void close() {
            try {
                pstmt0.close();
                pstmt1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        public static void main(String[] args) {
            try {
                Calendar c = Calendar.getInstance();
                if (c.get(Calendar.MINUTE) == 0) {
                    if (c.get(Calendar.HOUR_OF_DAY) == 0) {
                        clearHourTracker();
                    }
                    syncFunnel();
                }
                updateFunnel();

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }

        private static void usage() {
            System.out.println("Command : TopoName[BI_Needle] ");
        }
    }
}
