package com.yihaodian.bi.storm.logic.tracker;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import storm.trident.state.JSONNonTransactionalSerializer;
import storm.trident.state.Serializer;

import com.google.common.base.Strings;
import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.cached.AsyncCommitCMap;
import com.yihaodian.bi.cached.BufferedReadCMap;
import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.cached.ConcurrentMemoryCMap;
import com.yihaodian.bi.cached.factory.CMapFactory;
import com.yihaodian.bi.cached.factory.HBaseCMapFactory;
import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.common.util.DateUtil;
import com.yihaodian.bi.common.util.FileUtil;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria.ColumnMetaData;

import static com.yihaodian.bi.storm.common.util.Constant.CMS_ID;
import static com.yihaodian.bi.storm.common.util.Constant.PORTAL_ID;
import static com.yihaodian.bi.storm.common.util.Constant.CATEGORY_ID;
import static com.yihaodian.bi.storm.common.util.Constant.DETAIL_ID;
import static com.yihaodian.bi.storm.common.util.Constant.CHANNEL_ID;
import static com.yihaodian.bi.storm.common.util.Constant.KEY_WORD_ID;
import static com.yihaodian.bi.storm.common.util.Constant.PROMOTION_ID;

/**
 * 流量逻辑工具
 */
public class TrackerLogicUtils {

  private static final Logger LOG = LoggerFactory.getLogger(TrackerLogicUtils.class);

  public static final String cacheBaseDir = "/user/bi_etl/cache/";
  public static final String channleCacheFile = cacheBaseDir + "chnl.cache";
  public static final String trackUCacheFile = cacheBaseDir + "tracku.cache";
  public static final String trackUToChanlIdCacheFile = cacheBaseDir + "trackerU_ChanlId.cache";
  public static final String chanlIdToAcdvFlgCacheFile = cacheBaseDir + "chanlId_acdvFlg.cache";
  public static final String chanlIdToCoreFlgCacheFile = cacheBaseDir + "chanlId_coreFlg.cache";
  public static final String trackCartCacheFile = cacheBaseDir + "trackCart.cache";
  public static final String trackAlgCacheFile = cacheBaseDir + "trackAlg.cache";
  public static final int UPDATE_HOUR_POINT = 16;
  public static final String OWN_CHNL_ID = "9";

  protected static AbstractBaseCMap trackeruHbaseLRUReadonly = null;
  protected static AbstractBaseCMap channelHbaseLRUReadonly = null;
  protected static AbstractBaseCMap trackeruToChanlIdHbaseLRUReadonly = null;
  protected static AbstractBaseCMap chanlIdToAcdvFlgHbaseLRUReadonly = null;
  protected static AbstractBaseCMap chanlIdToCoreFlgHbaseLRUReadonly = null;
  protected static AbstractBaseCMap pageTypeToCategHbaseLRUReadonly = null;
  protected static AbstractBaseCMap needleUvHBaseLRU = null;
  protected static AbstractBaseCMap funnelUvHBaseLRU = null;
  protected static AbstractBaseCMap hbaseLRU = null;
  protected static AbstractBaseCMap trackerMonitorHBaseLRU = null;


  public synchronized static CMap getNeedleHBaseAsyncLRU() throws IOException {
    if (needleUvHBaseLRU == null) {
      if (!Debug.DEBUG) {
        needleUvHBaseLRU = new AsyncCommitCMap(
                new BufferedReadCMap(
                        new ConcurrentMemoryCMap(
                                new LRUMap(
                                        CMapFactory.CACHE_SIZE)),
                        HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.UV_CACHE_TABLE,
                                false, null)),
                true, CMapFactory.ASYNC_INTERVAL);
      } else {
        needleUvHBaseLRU = new ConcurrentMemoryCMap(new ConcurrentHashMap());
      }
    }
    return needleUvHBaseLRU;
  }

  public synchronized static CMap getFunnelHBaseAsyncLRU() throws IOException {
    if (funnelUvHBaseLRU == null) {
      if (!Debug.DEBUG) {
        funnelUvHBaseLRU = new AsyncCommitCMap(
                new BufferedReadCMap(
                        new ConcurrentMemoryCMap(
                                new LRUMap(
                                        CMapFactory.CACHE_SIZE)),
                        HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.FUNNEL_CACHE_TABLE,
                                false, null)),
                true, CMapFactory.ASYNC_INTERVAL);
      } else {
        funnelUvHBaseLRU = new ConcurrentMemoryCMap(new ConcurrentHashMap());
      }
    }
    return funnelUvHBaseLRU;
  }

  /**
   * get the global cache of TrackerMonitor application
   *
   * @return the global cache
   * @throws IOException
   */
  public synchronized static CMap getTrackerMonitorHBaseAsyncLRU() throws IOException {
    if (trackerMonitorHBaseLRU == null) {
      if (!Debug.DEBUG) {
        trackerMonitorHBaseLRU = new AsyncCommitCMap(
                new BufferedReadCMap(
                        new ConcurrentMemoryCMap(new LRUMap(CMapFactory.CACHE_SIZE)),
                        HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.TRACKER_MONITOR_CACHE_TABLE,
                                false, null)), true, CMapFactory.ASYNC_INTERVAL);
      } else {
        trackerMonitorHBaseLRU = new ConcurrentMemoryCMap(new ConcurrentHashMap());
      }
    }
    return trackerMonitorHBaseLRU;
  }

  public synchronized static AbstractBaseCMap getHBaseAsyncLRU(String tableName, ColumnMetaData columnMetaData)
          throws IOException {
    if (hbaseLRU == null) {
      if (!Debug.DEBUG) {
        hbaseLRU = new AsyncCommitCMap(
                new BufferedReadCMap(
                        new ConcurrentMemoryCMap(
                                new LRUMap(
                                        CMapFactory.CACHE_SIZE)),
                        HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.getTable(tableName),
                                false, columnMetaData)),
                true, CMapFactory.ASYNC_INTERVAL);
      } else {
        hbaseLRU = new ConcurrentMemoryCMap(new ConcurrentHashMap());
      }
    }
    return hbaseLRU;
  }

  public static void clearCache(AbstractBaseCMap cache) throws Exception {
    cache.clear();
  }

  public synchronized static int loadCacheDataToMem(CMap mem, String file, String split) throws Exception {

    BufferedReader br;
    if (!Debug.DEBUG) {
      br = FileUtil.readFileByHadoop(file);
    } else {
      br = FileUtil.readZipFile(file).get(0);
    }
    String line = null;
    String[] lines;
    int count = 0;
    while ((line = br.readLine()) != null) {
      lines = line.split(split);
      mem.put(lines[0], lines[1]);
      count++;
    }
    br.close();
    return count;
  }

  // chanlId -> chanl_categ_lvl1_id
  public synchronized static AbstractBaseCMap getChannelHBaseLRUReadonly() throws Exception {
    if (channelHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        channelHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.CHANNEL_KV_TABLE,
                        true, null));
        LOG.info("KV <chanlId, chanl_categ_lvl1_id> has been loaded into cache :)");
      } else {
        channelHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap<String, Object>(1200000));
        int count = loadCacheDataToMem(channelHbaseLRUReadonly, "chnl.zip", "\001");
        System.out.println("Load test channel data " + count);
      }
    }
    return channelHbaseLRUReadonly;
  }

  // trackeru -> chanl_categ_lvl1_id
  public synchronized static AbstractBaseCMap getTrackeruHBaseLRUReadonly() throws Exception {
    if (trackeruHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        trackeruHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.TRACKERU_KV_TABLE,
                        true, null));
        LOG.info("KV <trackeru, chanl_categ_lvl1_id> has been loaded into cache :)");
      } else {
        trackeruHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap(1200000));
        int count = loadCacheDataToMem(trackeruHbaseLRUReadonly, "tracku.zip", "\t");
        System.out.println("Load test tracker data " + count);
      }
    }
    return trackeruHbaseLRUReadonly;
  }

  // trackeru -> chanl_id
  public synchronized static AbstractBaseCMap getTrackeruToChanlIdHBaseLRUReadonly() throws Exception {
    if (trackeruToChanlIdHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        trackeruToChanlIdHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.TRACKERU_CHANNEL_KV_TABLE,
                        true, null));
        LOG.info("KV <trackeru, chanl_id> has been loaded into cache :)");
      } else {
        trackeruToChanlIdHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap(1200000));
        int count = loadCacheDataToMem(trackeruToChanlIdHbaseLRUReadonly, "trackerU_chanlId.zip", "\t");
        System.out.println("Load test trackerU_chanlId data " + count);
      }
    }
    return trackeruToChanlIdHbaseLRUReadonly;
  }

  // chanl_id -> acdv_flg
  public synchronized static AbstractBaseCMap getChanlToAcdvFlgHBaseLRUReadonly() throws Exception {
    if (chanlIdToAcdvFlgHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        chanlIdToAcdvFlgHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.CHANNEL_ACDVFLG_KV_TABLE,
                        true, null));
        LOG.info("KV <chanl_id, acdv_flg> has been loaded into cache :)");
      } else {
        chanlIdToAcdvFlgHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap(1200000));
        int count = loadCacheDataToMem(chanlIdToAcdvFlgHbaseLRUReadonly, "chanlId_acdvFlg.zip", "\t");
        System.out.println("Load test trackerU_chanlId data " + count);
      }
    }
    return chanlIdToAcdvFlgHbaseLRUReadonly;
  }

  // chanl_id -> core_flg
  public synchronized static AbstractBaseCMap getChanlToCoreFlgHBaseLRUReadonly() throws Exception {
    if (chanlIdToCoreFlgHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        chanlIdToCoreFlgHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.CHANNEL_COREFLG_KV_TABLE,
                        true, null));
        LOG.info("KV <chanl_id, core_flg> has been loaded into cache :)");
      } else {
        chanlIdToCoreFlgHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap(1200000));
        int count = loadCacheDataToMem(chanlIdToCoreFlgHbaseLRUReadonly, "chanlId_coreFlg.zip", "\t");
        System.out.println("Load test chanlId_coreFlg data " + count);
      }
    }
    return chanlIdToCoreFlgHbaseLRUReadonly;
  }

  // page_type_id -> page_categ_id
  public synchronized static AbstractBaseCMap getPageTypeToCategHBaseLRUReadonly() throws Exception {
    if (pageTypeToCategHbaseLRUReadonly == null) {
      if (!Debug.DEBUG) {
        pageTypeToCategHbaseLRUReadonly = new BufferedReadCMap(
                new ConcurrentMemoryCMap(
                        new LRUMap(
                                CMapFactory.CACHE_SIZE)),
                HBaseCMapFactory.getHBaseCMap(HBaseConstant.Table.PAGETYPE_CATEG_KV_TABLE,
                        true, null));
        LOG.info("KV <page_type_id, page_categ_id> has been loaded into cache :)");
      } else {
        pageTypeToCategHbaseLRUReadonly = new ConcurrentMemoryCMap(new HashMap(1200000));
        int count = loadCacheDataToMem(pageTypeToCategHbaseLRUReadonly, "pageType_categ.zip", "\t");
        System.out.println("Load test pageType_categ data " + count);
      }
    }
    return pageTypeToCategHbaseLRUReadonly;
  }

  /**
   * Initial all caches of Needle application
   *
   * @throws Exception
   */
  public synchronized static void initNeedleCache() throws Exception {
    channelHbaseLRUReadonly = getChannelHBaseLRUReadonly();
    trackeruHbaseLRUReadonly = getTrackeruHBaseLRUReadonly();
  }

  /**
   * Initial all caches of Funnel application
   *
   * @throws Exception
   */
  public synchronized static void initFunnelCache() throws Exception {
    channelHbaseLRUReadonly = getChannelHBaseLRUReadonly();
    chanlIdToAcdvFlgHbaseLRUReadonly = getChanlToAcdvFlgHBaseLRUReadonly();
    chanlIdToCoreFlgHbaseLRUReadonly = getChanlToCoreFlgHBaseLRUReadonly();
    trackeruToChanlIdHbaseLRUReadonly = getTrackeruToChanlIdHBaseLRUReadonly();
    pageTypeToCategHbaseLRUReadonly = getPageTypeToCategHBaseLRUReadonly();
    initCartTrackPattern();
    initAlgTrackPattern();
    setTrackPatternUpdateTimer();
    LOG.info("TrackerLogicUtils:initFunnelCache initial funnel cache success");
  }

  /**
   * Initial all caches of TrackerMonitor application
   *
   * @throws Exception
   */
  public synchronized static void initTrackerMonitorCache() throws Exception {
    trackerMonitorHBaseLRU = (AbstractBaseCMap) getTrackerMonitorHBaseAsyncLRU();
    pageTypeToCategHbaseLRUReadonly = getPageTypeToCategHBaseLRUReadonly();
    LOG.info("Successfully initial all caches of TrackerMonitor.");
  }

  public enum VipType {
    VIP(1), NON_VIP(0);

    private int _type;

    VipType(int type) {
      _type = type;
    }

    public int getType() {
      return _type;
    }
  }

  public enum PlatformType {
    APP(123, "123", "app", 10), H5(4, "4", "h5", 20), PC(-1, "-1", "pc", -10);

    private int _type;
    private String _name;
    private String _literal;
    private int _id;

    PlatformType(int type, String name, String literal, int id) {
      _type = type;
      _name = name;
      _literal = literal;
      _id = id;
    }

    public int getType() {
      return _type;
    }

    public String getName() {
      return _name;
    }

    public String getLiteral() {
      return _literal;
    }

    public int getId() {
      return _id;
    }
  }

  public static class ChannelResult {

    private String _value;
    private int _type; // 2: 表示由trackeru计算的来，否则为0

    private ChannelResult(String value, int type) {
      _value = value;
      _type = type;
    }

    public int getType() {
      return _type;
    }

    public String getVal() {
      return _value;
    }

  }

  public enum PageType {
    PORTAL(PORTAL_ID, "ptl", "首页"),
    DETAIL(DETAIL_ID, "dtl", "详情页"),
    CATEGORY(CATEGORY_ID, "ctg", "类目页"),
    CHANNEL(CHANNEL_ID, "chl", "频道页"),
    CMS(CMS_ID, "cms", "CMS页"),
    KEYWORD(KEY_WORD_ID, "kwd", "搜词页"),
    PROMOTION(PROMOTION_ID, "pmt", "搜索促销页");


    private Integer _id;
    private String _name;
    private String _literal;

    PageType(Integer id, String name, String literal) {
      _id = id;
      _name = name;
      _literal = literal;
    }

    public Integer getId() {
      return _id;
    }

    public String getName() {
      return _name;
    }

    public String getLiteral() {
      return _literal;
    }
  }


  public enum VisiterType {
    APP("1", "APP有效UV"), H5_VIP("2", "H5会员"), H5_NON_VIP("3", "H5访客"), PC_VIP("4", "PC会员"), PC_NON_VIP("5", "PC访客");

    private String _type;
    private String _name;

    VisiterType(String type, String name) {
      _type = type;
      _name = name;
    }

    public String getType() {
      return _type;
    }

    public String getName() {
      return _name;
    }
  }

  public enum NoTrackerU {
    BAIDU1(1, "56860", false, "http://open.baidu.com/shopping", "http://opendata.baidu.com/shopping/s"), //
    BAIDU2(1, "10242", true, ".baidu.com/"), //
    SO360(1, "39969", false, "http://so.360.cn/", "http://www.so.com/", "http://m.so.com/",
            "http://www.haosou.com/"), //
    GOOGLE(1, "10253", false, "http://www.google.cn/products"), //
    GOOGLE1(1, "10243", false, "http://www.google.", "https://www.google."), //
    SOSO(1, "10244", true, ".soso.com/"), //
    SOGOU(1, "10245", false, "http://www.sogou.com/"), //
    YOUDAO(1, "10254", false, "http://gouwu.youdao.com/search"), //
    BING(1, "10405", true, ".bing.com/"), //
    YOUDAO1(1, "10406", false, "http://www.youdao.com/search?q="), //
    YAHOO(1, "10407", true, ".yahoo.com/", ".yahoo.cn/"), //
    UC(1, "54761", false, "http://glb.uc.cn/"), //
    SM(1, "15376", true, ".sm.cn/"), //
    YHD(2, "51833", false, "http://www.yhd.com/s-theme/", "http://www.yihaodian.com/s-theme/",
            "http://www.yhd.com/s-theme/", "http://www.yihaodian.com/s-theme/", "http://www.yhd.com/marketing/hs-"), //
    YHD1(2, "99712", false, "http://hot.yhd.com/10122182098/"), //
    YHD2(2, "99710", false, "http://hot.yhd.com/1035182094/"), //
    YHD3(2, "99709", false, "http://hot.yhd.com/10817182093/"), //
    YHD4(2, "99708", false, "http://hot.yhd.com/10418482092/"), //
    YHD5(3, "88842", false, "http://m.yhd.com", "http://m.yihaodian.com", "http://m.1mall.com");//

    // ===========OTHER LOGIC===============
    // when referer is not null and upper(referer) <> "null" and
    // referer not like "%yhd.com%" and
    // referer not like "%yihaodian.com%" and
    // referer not like "%1mall.com%" then
    // cast(41140 as bigint)
    // when c.tracker_u > 0 then
    // cast(41140 as bigint)
    // when pagetypeid in (1, 3, 77) then
    // cast(56859 as bigint)
    // when pagetypeid in (134, 10000) then
    // cast(88841 as bigint)
    // else
    // cast(56860 as bigint)

    String[] urls;
    boolean noHttp;
    String id;
    int type;

    NoTrackerU(int type, String id, boolean noHttp, String... urls) {
      this.type = type;
      this.id = id;
      this.noHttp = noHttp;
      this.urls = urls;
    }

    public int getType() {
      return type;
    }

    public String getId() {
      return id;
    }

    public String[] getUrls() {
      return urls;
    }

    public boolean noHttp() {
      return noHttp;
    }

    public static String checkSEOFromOutSite(String referer) {
      for (NoTrackerU ntu : NoTrackerU.values()) {
        if (ntu.noHttp && ntu.type == 1) {
          if (referer.startsWith("http://") || referer.startsWith("https://")) {
            for (String url : ntu.urls) {
              if (referer.indexOf(url) > 0) {
                return ntu.id;
              }
            }
          }
        } else {
          for (String url : ntu.urls) {
            if (referer.startsWith(url)) {
              return ntu.id;
            }
          }
        }
      }
      return null;
    }

    public static String checkSEOWithinSite(String url) {
      for (NoTrackerU ntu : NoTrackerU.values()) {
        if (ntu.type == 2) {
          for (String u : ntu.urls) {
            if (url.startsWith(u)) {
              return ntu.id;
            }
          }
        }
      }

      return null;
    }

    public static String checkDirect(String url) {
      for (NoTrackerU ntu : NoTrackerU.values()) {
        if (ntu.type == 3) {
          for (String u : ntu.urls) {
            if (url.startsWith(u)) {
              return ntu.id;
            }
          }
        }
      }

      return null;
    }
  }

  /**
   * 　根据button_link字段判断某个流量记录是否为button_link
   *
   * @param btnLink 　button_link字段
   * @return
   */
  public static boolean isBtnLink(String btnLink) {
    if (btnLink != null && !btnLink.isEmpty() && !"null".equalsIgnoreCase(btnLink)) return true;

    return false;
  }

  /**
   * 判断平台类型 TODO 确定类型 0 app 1 h5
   *
   * @param t
   * @return
   */
  public static PlatformType getPlatformType(TrackerVO t) {
    String platform = t.getPlatform();
    String container = t.getContainer();
    String url = t.getUrl();

    if (platform == null) {
      return PlatformType.PC;
    }
    platform = platform.toLowerCase();
    if (container != null) {
      if (container.equals("yhdapp") && (platform.indexOf("iossystem") != -1 || platform.endsWith("iphone"))) {
        return PlatformType.APP;
      } else if (container.equals("yhdapp")
              && (platform.indexOf("ipadsystem") != -1 || platform.endsWith("ipad"))) {
        return PlatformType.APP;
      } else if (container.equals("yhdapp")
              && (platform.indexOf("androidsystem") != -1 || platform.endsWith("android"))) {
        return PlatformType.APP;
      } else if ((container.equals("H5") || (!container.equals("yhd-im-pc") && !container.isEmpty()))
              && ((platform.indexOf("androidsystem") != -1 || platform.indexOf("iossystem") != -1))) {
        return PlatformType.H5;
      }
    }

    if (url != null
            && !url.isEmpty()
            && !url.startsWith("http://m.")
            && (platform.indexOf("iossystem") != -1 || platform.indexOf("ipadsystem") != -1
            || platform.indexOf("androidsystem") != -1 || platform.endsWith("iphone") || platform.endsWith("ipad") || platform.endsWith("android"))) {
      return PlatformType.H5;
    }
    if (platform.indexOf("ipadsystem") != -1) {
      return PlatformType.APP;
    }
    if (url != null
            && !url.isEmpty()
            && (url.startsWith("http://m.yhd.com")
            || (url.startsWith("http://m.yihaodian.com") && url.indexOf("/mw/") != -1) || url.indexOf(".m.yhd.com") != -1)) {
      return PlatformType.H5;
    }

    return PlatformType.PC;
  }

  /**
   * 获得渠道 查cache
   *
   * @param t
   * @return
   * @throws Exception
   */
  public static ChannelResult channel(TrackerVO t) throws Exception {
    String tracker_u = t.getTracker_u();
    String trackeruRet = null;
    int flag = 0; // 如果由trackeru计算得到标记为2， 否则标记为0;
    if (tracker_u != null && !tracker_u.isEmpty()) {
      trackeruRet = trackeruHbaseLRUReadonly.get(tracker_u);
      if (trackeruRet != null) {
        flag = 2;
        return new ChannelResult(trackeruRet, flag);
      }
    }
    String referer = t.getReferer();
    if (referer != null && !referer.isEmpty()) {
      String cid = NoTrackerU.checkSEOFromOutSite(referer);
      if (cid != null) {
        return new ChannelResult(channelHbaseLRUReadonly.get(cid), flag);
      }
    }
    String url = t.getUrl();
    if (url != null && !url.isEmpty()) {
      String cid = NoTrackerU.checkSEOWithinSite(url);
      if (cid != null) {
        return new ChannelResult(channelHbaseLRUReadonly.get(cid), flag);
      }
    }
    if (referer != null && !referer.isEmpty() && !referer.equalsIgnoreCase("null")
            && referer.indexOf("yhd.com") < 0 && referer.indexOf("yihaodian.com") < 0
            && referer.indexOf("1mall.com") < 0) {
      return new ChannelResult(channelHbaseLRUReadonly.get("41140"), flag);
    }
    // 没办法，逻辑需要顺序判断
    if (tracker_u != null && !tracker_u.isEmpty() && trackeruRet == null) {
      flag = 2;
      return new ChannelResult(channelHbaseLRUReadonly.get("41140"), flag);
    }
    if ("1".equals(t.getPagetypeid()) || "3".equals(t.getPagetypeid()) || "77".equals(t.getPagetypeid())) {
      return new ChannelResult(channelHbaseLRUReadonly.get("56859"), flag);
    }
    if ("134".equals(t.getPagetypeid()) || "10000".equals(t.getPagetypeid())) {
      return new ChannelResult(channelHbaseLRUReadonly.get("88841"), flag);
    }
    if (url != null && !url.isEmpty()) {
      String cid = NoTrackerU.checkDirect(url);
      if (cid != null) {
        return new ChannelResult(channelHbaseLRUReadonly.get(cid), flag);
      }
    }

    return new ChannelResult(channelHbaseLRUReadonly.get("56860"), flag);
  }

  /**
   * @param t
   * @return 渠道ID
   * @throws Exception
   */
  public static ChannelResult channelFunnel(TrackerVO t) throws Exception {
    String tracker_u = t.getTracker_u();
    String trackeruRet = null;
    int flag = 0; // 如果由trackeru计算得到标记为2， 否则标记为0;
    if (tracker_u != null && !tracker_u.isEmpty()) {
      trackeruRet = trackeruToChanlIdHbaseLRUReadonly.get(tracker_u);
      if (trackeruRet != null) {
        flag = 2;
        return new ChannelResult(trackeruRet, flag);
      }
    }
    String referer = t.getReferer();
    if (referer != null && !referer.isEmpty()) {
      String cid = NoTrackerU.checkSEOFromOutSite(referer);
      if (cid != null) {
        return new ChannelResult(cid, flag);
      }
    }
    String url = t.getUrl();
    if (url != null && !url.isEmpty()) {
      String cid = NoTrackerU.checkSEOWithinSite(url);
      if (cid != null) {
        return new ChannelResult(cid, flag);
      }
    }
    if (referer != null && !referer.isEmpty() && !referer.equalsIgnoreCase("null")
            && referer.indexOf("yhd.com") < 0 && referer.indexOf("yihaodian.com") < 0
            && referer.indexOf("1mall.com") < 0) {
      return new ChannelResult("41140", flag);
    }
    // 没办法，逻辑需要顺序判断
    if (tracker_u != null && !tracker_u.isEmpty() && trackeruRet == null) {
      flag = 2;
      return new ChannelResult("41140", flag);
    }
    if ("1".equals(t.getPagetypeid()) || "3".equals(t.getPagetypeid()) || "77".equals(t.getPagetypeid())) {
      return new ChannelResult("56859", flag);
    }
    if ("134".equals(t.getPagetypeid()) || "10000".equals(t.getPagetypeid())) {
      return new ChannelResult("88841", flag);
    }
    if (url != null && !url.isEmpty()) {
      String cid = NoTrackerU.checkDirect(url);
      if (cid != null) {
        return new ChannelResult(cid, flag);
      }
    }

    return new ChannelResult("56860", flag);
  }

  public static String provId(TrackerVO t) {
    if (t.getExt_field2() == null || t.getExt_field2().isEmpty() || Integer.parseInt(t.getExt_field2()) <= 0) {
      return "1";
    }

    return t.getExt_field2();
  }

  /**
   * 类Tag的实现描述：标记状态
   */
  public static class Tag implements Comparable<Tag> {

    private String _value;
    private String _timestamp;
    private int _first;
    private int _tu;                                              // 当由trackeru计算得到时，设置为2
    private static Serializer serializer = new JSONNonTransactionalSerializer();

    public Tag(String value, String timestamp) {
      _value = value;
      _timestamp = timestamp;
      _first = 0;
      _tu = 0;
    }

    public Tag(String value, String timestamp, int first) {
      this(value, timestamp);
      _first = first;
    }

    public Tag(int _value, String _timestamp, int _first) {
      this._value = _value + "";
      this._first = _first;
      this._timestamp = _timestamp;
    }

    public Tag(String value, String timestamp, boolean first) {
      this(value, timestamp);
      _first = first ? 1 : 0;
    }

    public Tag(String value, String timestamp, boolean first, boolean tu) {
      this(value, timestamp, first);
      _tu = tu ? 2 : 0;
    }

    public Tag(String value, String timestamp, int first, int tu) {
      this(value, timestamp, first);
      _tu = tu;
    }

    public boolean isFirst() {
      return _first == 1 ? true : false;
    }

    public boolean isTrackerU() {
      return _tu == 2 ? true : false;
    }

    public void setFirst(boolean first) {
      if (first) _first = 1;
      else _first = 0;
    }

    public boolean before(Tag t) {
      if (_timestamp.compareTo(t.getTimestamp()) < 0) return true;

      return false;
    }

    public String getValue() {
      return _value;
    }

    public String getTimestamp() {
      return _timestamp;
    }

    public static Tag parseTag(String tag) {
      String[] tokens = StringUtils.splitPreserveAllTokens(tag, "[+@]");
      if (tokens.length != 3) {
        LOG.warn("tag is :" + tag);
        throw new IllegalArgumentException("Invalid tag!!");
      }
      if (Integer.parseInt(tokens[1]) >= 2) return new Tag(tokens[0], tokens[2], Integer.parseInt(tokens[1]) - 2,
              2);

      return new Tag(tokens[0], tokens[2], Integer.parseInt(tokens[1]));
    }

    @Override
    public String toString() {
      return _value + "+" + (_first + _tu) + "@" + _timestamp;
    }

    public static byte[] serialize(Tag t) {
      return serializer.serialize(t);
    }

    public static Tag deserialize(byte[] bytes) {
      return (Tag) serializer.deserialize(bytes);
    }

    @Override
    public int compareTo(Tag t) {
      if (_timestamp.compareTo(t.getTimestamp()) < 0) return 0;
      return 1;
    }
  }

  public static final List<Pattern> cartLink = new ArrayList<Pattern>();
  public static final List<Pattern> cartBtn = new ArrayList<Pattern>();

  public static void initCartTrackPattern() throws Exception {
    LOG.info("Start to initialize Cart Track Pattern List....");
    BufferedReader br = null;
    if (!Debug.DEBUG) {
      br = FileUtil.readFileByHadoop(trackCartCacheFile);
    } else {
      br = FileUtil.readZipFile("trackCart.zip").get(0);
    }
    String line = null;
    String[] arr;
    synchronized (cartLink) {
      synchronized (cartBtn) {
        cartBtn.clear();
        cartLink.clear();
        while ((line = br.readLine()) != null) {
          arr = line.split("\t");
          if (arr.length < 2) {
            continue;
          }
          if ("1".equals(arr[1])) {
            cartLink.add(Pattern.compile(arr[0].replaceAll("%", ".*")));
          } else if ("2".equals(arr[1])) {
            cartBtn.add(Pattern.compile(arr[0].replaceAll("%", ".*")));
          }
        }
      }
    }
    br.close();
    LOG.info("Successfully initialized Cart Track Pattern List.");
  }

  /**
   * 功能函数
   *
   * @param str   a string
   * @param pList Pattern List
   * @return
   */
  private static boolean isMatch(String str, List<Pattern> pList) {
    for (Pattern p : pList) {
      if (p.matcher(str).matches()) {
        return true;
      }
    }
    return false;
  }

  /**
   * 用于判断某tracker是否为加入购物车流量
   *
   * @param t
   * @return
   */
  public static boolean isAddCart(TrackerVO t) {
    String posTypeId = t.getPositiontypeid();
    String linkPos = t.getLink_position();
    String buttonPos = t.getButton_position();

    if (!Strings.isNullOrEmpty(posTypeId) && posTypeId.matches("[3456]")) {
      return true;
    } else if (!Strings.isNullOrEmpty(linkPos) && !linkPos.equalsIgnoreCase("null")) {
      synchronized (cartLink) {
        return isMatch(linkPos, cartLink);
      }
    } else if (!Strings.isNullOrEmpty(buttonPos) && !buttonPos.equalsIgnoreCase("null")) {
      synchronized (cartBtn) {
        return isMatch(buttonPos, cartBtn);
      }
    }

    return false;
  }

  public static final List<Pattern> algLink = new ArrayList<Pattern>();
  public static final List<Pattern> algBtn = new ArrayList<Pattern>();

  public static void initAlgTrackPattern() throws Exception {
    LOG.info("Start to initialize Algorithm Track Pattern List ....");
    BufferedReader br = null;
    if (!Debug.DEBUG) {
      br = FileUtil.readFileByHadoop(trackAlgCacheFile);
    } else {
      br = FileUtil.readZipFile("algConfig.zip").get(0);
    }
    String line = null;
    synchronized (algLink) {
      synchronized (algBtn) {
        algLink.clear();
        algBtn.clear();
        while ((line = br.readLine()) != null) {
          String[] arr = line.split("\t");
          if (arr.length < 2) {
            continue;
          }
          if (arr[1].equalsIgnoreCase("link_position")) {
            algLink.add(Pattern.compile(arr[0].replaceAll("%", ".*")));
          } else if (arr[1].equalsIgnoreCase("button_position")) {
            algBtn.add(Pattern.compile(arr[0].replaceAll("%", ".*")));
          }
        }
      }
    }
    LOG.info("Successfully initialized Algorithm Track Pattern List.");
    br.close();
  }

  public static void setTrackPatternUpdateTimer() {
    new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          Thread.sleep(DateUtil.ONE_HOUR_MILL);
          if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == UPDATE_HOUR_POINT) {
            initAlgTrackPattern();
            initCartTrackPattern();
          }
        } catch (Exception e) {
          LOG.error("Fail to initialize Cart Track Pattern List", e);
        }

      }

    }).start();
    LOG.info("Successfully set Timer for updating Track Pattern started.");
  }

  /**
   * 根据算法id判断是否为个性精准化Uv
   *
   * @param t
   * @return
   */
  public static boolean isRcmdUv(TrackerVO t) {
    String linkPos = t.getLink_position();
    String btnPos = t.getButton_position();
    if (!Strings.isNullOrEmpty(linkPos) && !"null".equalsIgnoreCase(linkPos)) {
      synchronized (algLink) {
        return isMatch(linkPos, algLink);
      }
    } else if (!Strings.isNullOrEmpty(btnPos) && !"null".equalsIgnoreCase(btnPos)) {
      synchronized (algBtn) {
        return isMatch(btnPos, algBtn);
      }
    }
    return false;
  }

  /**
   * 根据渠道Id判断是否为主动流量
   *
   * @param chanlId 渠道Id
   * @return
   * @throws Exception
   */
  public static boolean isAcdvUv(String chanlId) throws Exception {
    String acdvFlg = chanlIdToAcdvFlgHbaseLRUReadonly.get(chanlId);
    if (acdvFlg != null && !acdvFlg.isEmpty() && !"null".equalsIgnoreCase(acdvFlg) && Integer.parseInt(acdvFlg) > 0)
      return true;

    return false;
  }

  /**
   * 根据渠道Id判断是否为核心流量
   *
   * @param chanlId
   * @return
   * @throws Exception
   */
  public static boolean isCoreUv(String chanlId) throws Exception {
    String coreFlg = chanlIdToCoreFlgHbaseLRUReadonly.get(chanlId);
    if (coreFlg != null && !coreFlg.isEmpty() && !"null".equalsIgnoreCase(coreFlg) && Integer.parseInt(coreFlg) > 0)
      return true;

    return false;
  }

  /**
   * 根据渠道Id判断是否为自有流量
   *
   * @param chanlId
   * @return
   * @throws Exception
   */
  public static boolean isOwnUv(String chanlId) throws Exception {
    String chanldCategId = channelHbaseLRUReadonly.get(chanlId);
    if (chanldCategId != null && !chanldCategId.isEmpty() && !"null".equalsIgnoreCase(chanldCategId)
            && OWN_CHNL_ID.equals(chanldCategId)) {
      return true;
    }

    return false;
  }
}
