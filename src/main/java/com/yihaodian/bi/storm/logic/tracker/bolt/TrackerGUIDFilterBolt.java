package com.yihaodian.bi.storm.logic.tracker.bolt;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.yihaodian.bi.common.StatConstants.PVUVStat;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.storm.BaseStat;
import com.yihaodian.bi.storm.TopoGroup;

public class TrackerGUIDFilterBolt extends BaseStat implements IRichBolt {

    public TrackerGUIDFilterBolt(TopoGroup tag){
        super(tag);
    }

    private static final long   serialVersionUID = -5672758362296831352L;
    private static final Logger LOG              = LoggerFactory.getLogger(TrackerGUIDFilterBolt.class);

    private OutputCollector     _collector;
    private String              guid;
    private PVUVStat            stat             = new PVUVStat(this);

    @Override
    public void execute(Tuple input) {
        try {
            List<TrackerVO> list = (List<TrackerVO>) input.getValueByField("track");
            for (TrackerVO t : list) {
                guid = t.getGu_id();
                if (guid != null) {
                    stat.incr(stat.UV_BOLT_GUID_NOTNULL);
                    _collector.emit(new Values(guid, t));
                } else {
                    stat.incr(stat.UV_BOLT_GUID_NULL);
                }
            }
            stat.intervalWriteAndClean();
            _collector.ack(input);
        } catch (Exception e) {
            LOG.error("Shuffling trackers Failed! ", e);
            _collector.fail(input);
            throw new RuntimeException("Shuffling trackers Failed! ", e);
        }
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("guid", "track"));
    }

    @Override
    public void cleanup() {

    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
