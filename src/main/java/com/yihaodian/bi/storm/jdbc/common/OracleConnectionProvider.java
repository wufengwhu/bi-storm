package com.yihaodian.bi.storm.jdbc.common;

import java.sql.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yihaodian.bi.database.DBConnection;

/**
 * Created by wufeng on 2016/3/11.
 */
public class OracleConnectionProvider implements ConnectionProvider {

    private Map<String, String> configMap;
    private Connection conn = null;

    public OracleConnectionProvider(Map<String, String> configMap) {
        this.configMap = configMap;
    }

    @Override public void prepare() {

    }

    @Override public Connection getConnection() {
        try {
            Class.forName(configMap.get("dataSourceClassName"));
            conn = DriverManager.getConnection(configMap.get("dataSource.url"), configMap.get("dataSource.user"),
                                               configMap.get("dataSource.password"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    @Override public void cleanup() {

    }
}
