package com.yihaodian.bi.cached;

import com.yihaodian.bi.storm.trident.hbase.common.Utils;

import java.util.Map;

public class ConcurrentMemoryCMap extends AbstractBaseCMap {

    Map<String, Object> map;

    public ConcurrentMemoryCMap(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public String get(String k) {
        synchronized (map) {
            return Utils.toString(map.get(k));
        }
    }

    @Override
    public void put(String k, String v) {
        synchronized (map) {
            map.put(k, v);
        }
    }

    @Override
    public void puts(Map mps) {
        synchronized (map) {
            map.putAll(mps);
        }
    }

    @Override
    public void incr(String k, long inc) throws Exception {
        synchronized (map) {
            Long v = getLong(k);
            if (v != null) {
                map.put(k, String.valueOf(v + inc));
            } else {
                map.put(k, String.valueOf(inc));
            }
        }
    }

    @Override
    public void put(byte[] k, byte[] v)
            throws Exception {
        synchronized (map) {
            map.put(new String(k), v);
        }
    }

    @Override
    public byte[] get(byte[] k)
            throws Exception {
        synchronized (map) {
            Object obj = map.get(k);
            if (obj == null){
                return null;
            }
            return Utils.toBytes(obj);
        }
    }

    @Override
    public void clear() throws Exception {
        synchronized (map){
            map.clear();
        }
    }
}
