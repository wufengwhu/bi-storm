package com.yihaodian.bi.cached;

import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria.ColumnMetaData;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractBaseCMap implements CMap {

    public static boolean DEBUG = false;

    public abstract void put(byte[] k, byte[] v) throws Exception;

    public abstract byte[] get(byte[] k) throws Exception;

    public abstract void clear() throws Exception;

    @Override
    public Long getLong(String k) throws Exception {
        String v = this.get(k);
        if (v == null) {
            return null;
        } else {
            try {
                return Long.valueOf(v);
            } catch (Exception e) {
                return null;
            }
        }
    }
}
