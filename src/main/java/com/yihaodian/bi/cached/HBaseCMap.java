package com.yihaodian.bi.cached;

import com.google.common.base.Strings;
import com.yihaodian.bi.hbase.HBaseConstant.Table;
import com.yihaodian.bi.hbase.HBaseFactory;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria.ColumnMetaData;
import com.yihaodian.bi.storm.trident.hbase.common.Utils;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Increment;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 类HBaseMap.java的实现描述：TODO 类实现描述
 */
public class HBaseCMap extends AbstractBaseCMap {

    public static final String DEFAULT_CF = "cf";
    public static final String DEFAULT_QL = "value";
    protected HTable table = null;
    protected boolean readonly = false;
    protected ColumnMetaData _columnMetaData;

    public HBaseCMap(Table tb) throws IOException {
        this(tb, false, null);
    }

    public HBaseCMap(Table tb, boolean readonly, ColumnMetaData columnMetaData) throws IOException {
        if (columnMetaData == null) {
            _columnMetaData = new ColumnMetaData(DEFAULT_CF, DEFAULT_QL);
        } else {
            this._columnMetaData = columnMetaData;
        }
        if (!DEBUG) table = HBaseFactory.getTable(tb);
    }

    @Override
    public String get(String k) throws Exception {
        if (Strings.isNullOrEmpty(k)){
            return null;
        }
        byte[] v = table.get(new Get(Bytes.toBytes(k))).
                getValue(_columnMetaData.getColumnFamily(),
                        _columnMetaData.getQualifier());
        if (v == null) {
            return null;
        } else {
            return new String(v);
        }
    }

    @Override
    public void put(byte[] k, byte[] v) throws Exception {
        if (readonly) {
            throw new IOException("readonly");
        }
        Put p = new Put(k);
        p.setWriteToWAL(false);

        p.add(_columnMetaData.getColumnFamily(),
                _columnMetaData.getQualifier(), v);
        table.put(p);

    }

    @Override
    public byte[] get(byte[] k) throws Exception {
        byte[] v = table.get(new Get(k)).
                getValue(_columnMetaData.getColumnFamily(),
                        _columnMetaData.getQualifier());
        if (v == null) {
            return null;
        } else {
            return v;
        }
    }

    @Override
    public void clear() throws Exception {

    }

    @Override
    public void put(String k, String v) throws Exception {
        if (readonly) {
            throw new IOException("readonly");
        }
        Put p = new Put(Bytes.toBytes(k));
        p.setWriteToWAL(false);
        p.add(_columnMetaData.getColumnFamily(),
                _columnMetaData.getQualifier(), Bytes.toBytes(v));
        table.put(p);
    }

    @Override
    public String toString() {
        return "HBaseMap";
    }

    @Override
    public void puts(Map<String, Object> mps) throws Exception {
        if (readonly) {
            throw new IOException("readonly");
        }
        List<Put> ps = new ArrayList<Put>();
        for (String k : mps.keySet()) {
            Put p = new Put(Bytes.toBytes(k));
            p.setWriteToWAL(false);
            p.add(_columnMetaData.getColumnFamily(),
                    _columnMetaData.getQualifier(), Utils.toBytes(mps.get(k)));
            ps.add(p);
        }
        table.put(ps);
    }

    @Override
    public void incr(String k, long inc) throws IOException {
        if (readonly) {
            throw new IOException("readonly");
        }
        table.incrementColumnValue(Bytes.toBytes(k), _columnMetaData.getColumnFamily(),
                _columnMetaData.getQualifier(), inc);
    }

    @Override
    public Long getLong(String k) throws Exception {
        return table.incrementColumnValue(Bytes.toBytes(k), _columnMetaData.getColumnFamily(),
                _columnMetaData.getQualifier(), 0);
    }
}
