package com.yihaodian.bi.cached.factory;

import com.yihaodian.common.yredis.RedisProxy;
import com.yihaodian.common.yredis.client.YredisProxyFactory;
import com.yihaodian.common.yredis.client.exception.RedisInitException;
import org.apache.log4j.Logger;

import com.yihaodian.common.ycache.CacheProxy;
import com.yihaodian.common.ycache.memcache.YmemcacheProxyFactory;
import com.yihaodian.common.ycache.memcache.exception.MemcacheInitException;

public class YCacheCMapFactory {

    private static Logger     LOG   = Logger.getLogger(YCacheCMapFactory.class);
    private static CacheProxy cache = null;
    private static RedisProxy jedis = null;

    static {
        try {
            YmemcacheProxyFactory.configure("memcache.xml");
            YredisProxyFactory.configure("redis.xml");
        } catch (MemcacheInitException e) {
            LOG.error(e);
            throw new RuntimeException(e);
        } catch (RedisInitException e) {
            LOG.error(e);
            throw new RuntimeException(e);
        }
    }

    public synchronized static CacheProxy getCache() {
        if (cache == null) {
            cache = YmemcacheProxyFactory.getClient("bi_storm");
        }
        return cache;
    }

    public synchronized static RedisProxy getRedisCache(){
        if (jedis == null){
            jedis = YredisProxyFactory.getClient("bi_redis_test");
        }
        return jedis;
    }
}
