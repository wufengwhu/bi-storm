package com.yihaodian.bi.cached.factory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.cached.HBaseCMap;
import com.yihaodian.bi.hbase.HBaseConstant.Table;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria;

/**
 * 类HBaseFactory.java的实现描述：TODO 类实现描述
 *
 * @author zhaoheng Jul 28, 2015 4:53:52 PM
 */
public class HBaseCMapFactory {

    protected static final Map<String, HBaseCMap> CMPS = new HashMap<String, HBaseCMap>();

    public synchronized static HBaseCMap getHBaseCMap(Table tb,
                                                      boolean readonly,
                                                      HBaseProjectionCriteria.ColumnMetaData columnMetaData)
            throws IOException {
        if (CMPS.get(tb.tbName) != null) {
            return CMPS.get(tb.tbName);
        } else {
            HBaseCMap table = new HBaseCMap(tb, readonly, columnMetaData);
            CMPS.put(tb.tbName, table);
            return table;
        }
    }
}
