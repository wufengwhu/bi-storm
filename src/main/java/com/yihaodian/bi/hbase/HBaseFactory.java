package com.yihaodian.bi.hbase;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HTable;

import com.yihaodian.bi.hbase.HBaseConstant.Table;

public class HBaseFactory {

    protected static final Configuration       CONF   = HBaseConfiguration.create();
    protected static final Map<String, HTable> TABLES = new HashMap<String, HTable>();

    public synchronized static HTable getTable(Table tb) throws IOException {
        return getTable(tb.getFullName());
    }

    private synchronized static HTable getTable(String tbname) throws IOException {
        if (TABLES.get(tbname) != null) {
            return TABLES.get(tbname);
        } else {
            HTable table = new HTable(CONF, TableName.valueOf(tbname));
            table.setAutoFlush(true, false);
            TABLES.put(tbname, table);
            return table;
        }
    }
}
