package com.yihaodian.bi.hbase.conf;

import backtype.storm.Config;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;

import java.util.HashMap;
import java.util.Map;

public class ConfigurationFactory {

	private static Configuration configuration  = null ;

	public static Configuration getConfiguration()
	{
		configuration = HBaseConfiguration.create() ;
		String filePath = "hbase-site.xml" ;
		Path path = new Path(filePath);
		configuration.addResource(path);
		return configuration ;
	}

	public static void setLocalConfiguration(Configuration conf, String hbaseLocalConfigPath) {

		Map<String, Object> localHbaseConf = new HashMap<String, Object>();
		//local hadoop configurations
		localHbaseConf.put("fs.default.name", "hdfs://localhost:8020");
		localHbaseConf.put("hadoop.tmp.dir", "/usr/local/Cellar/hadoop/hdfs/tmp");
		localHbaseConf.put("mapred.job.tracker", "localhost:9010");
		localHbaseConf.put("mapreduce.framework.name", "yarn");
		localHbaseConf.put("yarn.resourcemanager.hostname", "localhost");
		localHbaseConf.put("yarn.nodemanager.aux-services", "mapreduce_shuffle");
		// local hbase configurations
		localHbaseConf.put("hbase.rootdir", "hdfs://localhost:8020/hbase");
		localHbaseConf.put("hbase.zookeeper.property.clientPort", "2181");
		localHbaseConf.put("hbase.cluster.distributed", "true");
		localHbaseConf.put("hbase.tmp.dir", "/Users/fengwu/tmp");
		localHbaseConf.put("hbase.zookeeper.quorum", "localhost");
		localHbaseConf.put("hbase.zookeeper.property.dataDir", "/Users/fengwu/zookeeper");
		localHbaseConf.put("dfs.replication", "1");

		Path path = new Path(hbaseLocalConfigPath);
        conf.addResource(path);
	}
}
