package com.yihaodian.bi.hbase;

import java.util.ArrayList;
import java.util.List;

/**
 * 类HBaseConstant.java的实现描述：TODO 类实现描述
 *
 * @author zhaoheng Jul 28, 2015 4:54:01 PM
 */
public class HBaseConstant {

    /**
     * 普通表
     */
    public static final int TABLE_TYPE_DEFAULT = 0;

    /**
     * 维表
     */
    public static final int TABLE_TYPE_DIMENSION = 10;

    public static enum Table {
        CHANNEL_KV_TABLE("bi_dim_chnl", TABLE_TYPE_DIMENSION),
        UV_CACHE_TABLE("bi_uv_cache", TABLE_TYPE_DEFAULT),
        STAT_KV_TABLE("bi_stat", TABLE_TYPE_DEFAULT),
        TRACKERU_KV_TABLE("bi_dim_tracker_u", TABLE_TYPE_DIMENSION),
        UV_CACHE_TEST_TABLE("bi_uv_cache_test", TABLE_TYPE_DEFAULT),
        TRACKERU_CHANNEL_KV_TABLE("bi_trackeru_chanl", TABLE_TYPE_DIMENSION),
        CHANNEL_ACDVFLG_KV_TABLE("bi_chanl_acdvflg", TABLE_TYPE_DIMENSION),
        CHANNEL_COREFLG_KV_TABLE("bi_chanl_coreflg", TABLE_TYPE_DIMENSION),
        PAGETYPE_CATEG_KV_TABLE("bi_pagetype_categ", TABLE_TYPE_DIMENSION),
        FUNNEL_CACHE_TABLE("bi_funnel_cache", TABLE_TYPE_DEFAULT),
        TRACKER_MONITOR_CACHE_TABLE("bi_tracker_monitor_cache", TABLE_TYPE_DEFAULT),
        TRIDENT_FUNNEL_CACHE_TABLE("bi_trident_funnel_cache", TABLE_TYPE_DEFAULT),
        TRIDENT_FUNNEL_GUID_CACHE_TABLE("bi_trident_funnel_guid_cache", TABLE_TYPE_DEFAULT);

        public String tbName;
        public String nameSpace = "bi";
        public int type;

        Table(String tbName, int type) {
            this.tbName = tbName;
            this.type = type;
        }
        
        public String getFullName(){
            return nameSpace + ":" + tbName;
        }

        public static List<Table> listByType(int type) {
            List<Table> ts = new ArrayList<Table>(Table.values().length);
            for (Table t : Table.values()) {
                if (t.type == type) {
                    ts.add(t);
                }
            }
            return ts;
        }

        public static Table getTable(String tbName) {// 根据值获得实例
            Table t = null;
            for (Table t1 : Table.values())
                if (t1.tbName.equals(tbName)) {
                    t = t1;
                    break;
                }
            return t;
        }
    }
}
