package kafka;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.storm.TopoSubmitter;
import com.yihaodian.bi.storm.logic.tracker.FunnelPackage;
import com.yihaodian.bi.storm.logic.tracker.NeedlePackage;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import com.yihaodian.bi.storm.logic.tracker.TrackerMonitorPackage;
import com.yihaodian.bi.storm.monitor.Needle;

import junit.framework.TestCase;

public class UnitTest extends TestCase {

  @Override
  protected void setUp() {
    Debug.DEBUG = true;
  }

//    public void testSyncHourTracker() throws SQLException, IOException {
//        Debug.DEBUG = true;
//        NeedlePackage.Sync2DB.syncHourTracker();
//    }

//    public void testMonitorNeedle() throws Exception {
//        TopoSubmitter.submit(new String[] { "monitor", "BI_Needle", "15926415061", "0.01" });
//    }

//    public void testTrackerLogicUtils() {
//        TrackerLogicUtils.initGetAlgorithmId();
//        TrackerLogicUtils.initGetTrackCartFlag();
//    }

//    public void testSyncFunnel() throws SQLException, IOException {
//        Debug.DEBUG = true;
//        FunnelPackage.Sync2DB.main(null);
//    }

//    public void testMonitorRptTranx() throws Exception {
//        TopoSubmitter.submit(new String[] { "monitor", "BI_RptRealtimeMontrTranx", "15926415061", "0.01" });
//    }

//    public void testsyncChnlTracker() throws SQLException, IOException {

//        NeedlePackage.Sync2DB.syncChnlTracker();
//}
      public void testSyncTrackerMonitor() {
        TrackerMonitorPackage.Sync2DB.main(null);
      }
}
