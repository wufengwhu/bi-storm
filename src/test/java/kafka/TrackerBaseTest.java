package kafka;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import junit.framework.TestCase;

import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.common.Debug;
import com.yihaodian.bi.common.StatConstants;
import com.yihaodian.bi.common.StatConstants.CacheStat;
import com.yihaodian.bi.common.StatConstants.KafkaConsumerStat;
import com.yihaodian.bi.common.StatConstants.KafkaSpoutStat;
import com.yihaodian.bi.common.util.DateUtil;
import com.yihaodian.bi.storm.TopoSubmitter;

public abstract class TrackerBaseTest extends TestCase {

    public static final String mockStr = "121507230000000000   http://www.yhd.com/?tracker_u=105761690&uid=&website_id=E100020014| http://f.17glink.com/Ealliance?m=yihaodian,E100020014,E25620,0,0,u_id=      3   9PBPQQPYSQ4VUZPFGM7FASPXZ8PW49Y22SW5                6.60_0_0    GKQBT138SQ8YJT4H2JUA2DT6471ZZ466    105761690       122.241.230.92      msessionid:QX3UNF1R4F94397EAQ5RH7T3JVDGKZ2J9FPG,unionKey:105761690,websiteId:E100020014|        2015-07-23 00:00:00     http://www.yhd.com/?tracker_u=105761690&uid=&website_id=E100020014|&forceId=6   23          6   60                  Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.14 (KHTML, like Gecko) Chrome/24.0.1293.0 Safari/537.14    Win32                           浙江省 6           丽水市 98                  1   KuqViHv-10-DUYEa    1                                                       1600*900                                            1                                   1       1437580846234";
    protected static Map<String, Boolean> hasRun = new HashMap<String, Boolean>();
    public static int FILE_COUNT = 101368;
    public static int BOLT_COUNT = 97721;

    public static interface Test {

        public void test();
    }

    @Override
    protected void setUp() throws Exception {
//        OfflineConsumer.mockList = new ArrayList<String>(100000);
//        String[] temp = StringUtils.splitByWholeSeparator(mockStr, " ");
//        String id = null;
//        String sessionId = null;
//        String guid = null;
//        Random rnd = new Random();
//        for (NoTrackerU tu : TrackerLogicUtils.NoTrackerU.values()) {
//            guid = UUID.randomUUID().toString().replaceAll("-", "");
//            sessionId = UUID.randomUUID().toString().replaceAll("-", "");
//            for (String url : tu.getUrls()) {
//                for (int x = 0; x < 10; x++) {
//                    id = "32150723000" + rnd.nextInt(99999999);
//                    temp[0] = id;
//                    // modif referer
//                    if (tu.noHttp()) {
//                        temp[2] = "http://www" + url;
//                    } else {
//                        temp[2] = url + "com";
//                    }
//                    temp[5] = guid;
//                    temp[10] = sessionId;
//                    temp[11] = null; // tracker_u
//                    OfflineConsumer.mockList.add(StringUtils.join(temp, "\t"));
//                }
//            }
//        }
//        System.out.println("Write mock data " + OfflineConsumer.mockList.size());
//        FILE_COUNT += OfflineConsumer.mockList.size();
//        EMIT_COUNT += OfflineConsumer.mockList.size();
    }

    /**
     * Cache Value
     *
     * @param c
     * @param ks
     * @return
     * @throws Exception
     */
    public String cv(CMap c, String... ks) throws Exception {
        String key = ks[0];
        for (int x = 1; x < ks.length; x++) {
            key = key + "_" + ks[x];
        }
        System.out.println(key + " = " + c.get(key));
        return c.get(key);
    }

    public abstract String getTopoName();

    public void doTest(Test call, String[] cmds) throws Exception {
        if (hasRun.get(getTopoName()) == null) {
            Debug.DEBUG = true;
            StatConstants.INTERVAL = 5;
            StatConstants.FORMATER = DateUtil.YYYYMMDDHHMM;
            List<String> args = new ArrayList<String>();
            args.add("debug");
            args.add(getTopoName());
            args.addAll(Arrays.asList(cmds));
            TopoSubmitter.submit(args.toArray(new String[0]));
            KafkaConsumerStat cst = new KafkaConsumerStat(getTopoName());
            CacheStat bst = new CacheStat(getTopoName());
            StatConstants.KafkaSpoutStat sst = new KafkaSpoutStat(getTopoName());
            String dt = StatConstants.FORMATER.format(new Date(DateUtil.getRecentMinutePoint(StatConstants.INTERVAL
                    * -1)));
            while (true) {

                System.out.println("FILE COUNT=[" + FILE_COUNT + ","
                                   + cst.getValue(cst.KAFKA_TRACKER_CONSUMER_COUNT, dt) + "]  BOLT COUNT=["
                                   + BOLT_COUNT + "," + bst.getValue(bst.TRACKER_BOLT_SUCC, dt) + "]");


                if (FILE_COUNT == cst.getValue(cst.KAFKA_TRACKER_CONSUMER_COUNT, dt)
                        && BOLT_COUNT == bst.getValue(bst.TRACKER_BOLT_SUCC, dt)) {
                    Thread.sleep(1000 * 5);
                    break;
                }

                Thread.sleep(1000);
            }

            // while(bst.getValue(bst.TRACKER_BOLT_RECV, dt) != 100) {
            // System.out.println("bolt:" + bst.getValue(bst.TRACKER_BOLT_RECV, dt));
            // }
            hasRun.put(getTopoName(), true);
        }

        call.test();
        TopoSubmitter.submit(new String[]{"debug", getTopoName(), "stop"});
    }
}
