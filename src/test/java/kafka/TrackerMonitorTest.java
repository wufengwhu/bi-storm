package kafka;

import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;

/**
 * Created on 11/26/15.
 */
public class TrackerMonitorTest extends TrackerBaseTest {
  @Override
  public String getTopoName() {
    return "BI_TrackerMonitor";
  }

  public void testLogic() throws Exception {
    doTest(new Test() {

      @Override
      public void test() {
        try {
          CMap cache = TrackerLogicUtils.getTrackerMonitorHBaseAsyncLRU();

          System.out.println("pv_app_201507230000:" + cache.get("pv_app_201507230000"));
          System.out.println("pv_h5_201507230000:" + cache.get("pv_h5_201507230000"));
          System.out.println("pv_pc_201507230000:" + cache.get("pv_pc_201507230000"));
          System.out.println("uv_app_201507230000:" + cache.get("uv_app_201507230000"));
          System.out.println("uv_h5_201507230000:" + cache.get("uv_h5_201507230000"));
          System.out.println("uv_pc_201507230000:" + cache.get("uv_pc_201507230000"));
          System.out.println("pv_app_cms_201507230000:" + cache.get("pv_app_cms_201507230000"));
          System.out.println("pv_app_kwd_201507230000:" + cache.get("pv_app_kwd_201507230000"));
          System.out.println("pv_app_dtl_201507230000:" + cache.get("pv_app_dtl_201507230000"));
          System.out.println("pv_app_chl_201507230000:" + cache.get("pv_app_chl_201507230000"));
          System.out.println("pv_app_ptl_201507230000:" + cache.get("pv_app_ptl_201507230000"));
          System.out.println("pv_app_pmt_201507230000:" + cache.get("pv_app_pmt_201507230000"));
          System.out.println("uv_app_cms_201507230000:" + cache.get("uv_app_cms_201507230000"));
          System.out.println("uv_app_kwd_201507230000:" + cache.get("uv_app_kwd_201507230000"));
          System.out.println("uv_app_dtl_201507230000:" + cache.get("uv_app_dtl_201507230000"));
          System.out.println("uv_app_chl_201507230000:" + cache.get("uv_app_chl_201507230000"));
          System.out.println("uv_app_ptl_201507230000:" + cache.get("uv_app_ptl_201507230000"));
          System.out.println("uv_app_pmt_201507230000:" + cache.get("uv_app_pmt_201507230000"));
          System.out.println("vu_app_cms_201507230000:" + cache.get("vu_app_cms_201507230000"));
          System.out.println("vu_app_kwd_201507230000:" + cache.get("vu_app_kwd_201507230000"));
          System.out.println("vu_app_dtl_201507230000:" + cache.get("vu_app_dtl_201507230000"));
          System.out.println("vu_app_chl_201507230000:" + cache.get("vu_app_chl_201507230000"));
          System.out.println("vu_app_ptl_201507230000:" + cache.get("vu_app_ptl_201507230000"));
          System.out.println("vu_app_pmt_201507230000:" + cache.get("vu_app_pmt_201507230000"));
          System.out.println("pv_h5_cms_201507230000:" + cache.get("pv_h5_cms_201507230000"));
          System.out.println("pv_h5_kwd_201507230000:" + cache.get("pv_h5_kwd_201507230000"));
          System.out.println("pv_h5_dtl_201507230000:" + cache.get("pv_h5_dtl_201507230000"));
          System.out.println("pv_h5_chl_201507230000:" + cache.get("pv_h5_chl_201507230000"));
          System.out.println("pv_h5_ptl_201507230000:" + cache.get("pv_h5_ptl_201507230000"));
          System.out.println("pv_h5_pmt_201507230000:" + cache.get("pv_h5_pmt_201507230000"));
          System.out.println("uv_h5_cms_201507230000:" + cache.get("uv_h5_cms_201507230000"));
          System.out.println("uv_h5_kwd_201507230000:" + cache.get("uv_h5_kwd_201507230000"));
          System.out.println("uv_h5_dtl_201507230000:" + cache.get("uv_h5_dtl_201507230000"));
          System.out.println("uv_h5_chl_201507230000:" + cache.get("uv_h5_chl_201507230000"));
          System.out.println("uv_h5_ptl_201507230000:" + cache.get("uv_h5_ptl_201507230000"));
          System.out.println("uv_h5_pmt_201507230000:" + cache.get("uv_h5_pmt_201507230000"));
          System.out.println("vu_h5_cms_201507230000:" + cache.get("vu_h5_cms_201507230000"));
          System.out.println("vu_h5_kwd_201507230000:" + cache.get("vu_h5_kwd_201507230000"));
          System.out.println("vu_h5_dtl_201507230000:" + cache.get("vu_h5_dtl_201507230000"));
          System.out.println("vu_h5_chl_201507230000:" + cache.get("vu_h5_chl_201507230000"));
          System.out.println("vu_h5_ptl_201507230000:" + cache.get("vu_h5_ptl_201507230000"));
          System.out.println("vu_h5_pmt_201507230000:" + cache.get("vu_h5_pmt_201507230000"));
          System.out.println("pv_pc_cms_201507230000:" + cache.get("pv_pc_cms_201507230000"));
          System.out.println("pv_pc_kwd_201507230000:" + cache.get("pv_pc_kwd_201507230000"));
          System.out.println("pv_pc_dtl_201507230000:" + cache.get("pv_pc_dtl_201507230000"));
          System.out.println("pv_pc_chl_201507230000:" + cache.get("pv_pc_chl_201507230000"));
          System.out.println("pv_pc_ptl_201507230000:" + cache.get("pv_pc_ptl_201507230000"));
          System.out.println("pv_pc_pmt_201507230000:" + cache.get("pv_pc_pmt_201507230000"));
          System.out.println("uv_pc_cms_201507230000:" + cache.get("uv_pc_cms_201507230000"));
          System.out.println("uv_pc_kwd_201507230000:" + cache.get("uv_pc_kwd_201507230000"));
          System.out.println("uv_pc_dtl_201507230000:" + cache.get("uv_pc_dtl_201507230000"));
          System.out.println("uv_pc_chl_201507230000:" + cache.get("uv_pc_chl_201507230000"));
          System.out.println("uv_pc_ptl_201507230000:" + cache.get("uv_pc_ptl_201507230000"));
          System.out.println("uv_pc_pmt_201507230000:" + cache.get("uv_pc_pmt_201507230000"));
          System.out.println("vu_pc_cms_201507230000:" + cache.get("vu_pc_cms_201507230000"));
          System.out.println("vu_pc_kwd_201507230000:" + cache.get("vu_pc_kwd_201507230000"));
          System.out.println("vu_pc_dtl_201507230000:" + cache.get("vu_pc_dtl_201507230000"));
          System.out.println("vu_pc_chl_201507230000:" + cache.get("vu_pc_chl_201507230000"));
          System.out.println("vu_pc_ptl_201507230000:" + cache.get("vu_pc_ptl_201507230000"));
          System.out.println("vu_pc_pmt_201507230000:" + cache.get("vu_pc_pmt_201507230000"));
        } catch (Exception e) {
          throw new RuntimeException();
        }
      }
    }, new String[]{"1-1-10-1000", "false"});
  }
}
