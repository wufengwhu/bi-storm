package kafka;

import com.yihaodian.bi.cached.CMap;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;

public class NeedleTest extends TrackerBaseTest {

    @Override
    public String getTopoName() {
        return "BI_Needle";
    }

    public void testLogic() throws Exception {
        doTest(new Test() {

            @Override
            public void test() {
                try {
                    CMap cache = TrackerLogicUtils.getNeedleHBaseAsyncLRU();
//                  System.out.println("glob_201508200800:" + cache.get("glob_201508200800"));
//                  System.out.println("hour_2015082008:" + cache.get("hour_2015082008"));
//                  System.out.println("user_123_1_201508200800:" + cache.get("user_123_1_201508200800"));
//                  System.out.println("user_4_1_201508200800:" + cache.get("user_4_1_201508200800"));
//                  System.out.println("user_4_0_201508200800:" + cache.get("user_4_0_201508200800"));
//                  System.out.println("user_-1_1_201508200800:" + cache.get("user_-1_1_201508200800"));
//                  System.out.println("user_-1_0_201508200800:" + cache.get("user_-1_0_201508200800"));
//                  System.out.println("prov_1_201508200800:" + cache.get("prov_1_201508200800"));
//                  System.out.println("prov_2_201508200800:" + cache.get("prov_2_201508200800"));
//                  System.out.println("prov_3_201508200800:" + cache.get("prov_3_201508200800"));
//                  System.out.println("prov_20_201508200800:" + cache.get("prov_20_201508200800"));
//                  System.out.println("chnl_2_201508200800:" + cache.get("chnl_2_201508200800"));
//                  System.out.println("chnl_13_201508200800:" + cache.get("chnl_13_201508200800"));
//                  System.out.println("chnl_1001_201508200800:" + cache.get("chnl_1001_201508200800"));
//                  System.out.println("chnl_1002_201508200800:" + cache.get("chnl_1002_201508200800"));
//                  System.out.println("chnl_1003_201508200800:" + cache.get("chnl_1003_201508200800"));
//                  System.out.println("chnl_1004_201508200800:" + cache.get("chnl_1004_201508200800"));
//                  System.out.println("chnl_1005_201508200800:" + cache.get("chnl_1005_201508200800"));
//                  System.out.println("chnl_1007_201508200800:" + cache.get("chnl_1007_201508200800"));
//                  System.out.println("chnl_1008_201508200800:" + cache.get("chnl_1008_201508200800"));
//                  System.out.println("chnl_1010_201508200800:" + cache.get("chnl_1010_201508200800"));
//                  System.out.println("chnl_1011_201508200800:" + cache.get("chnl_1011_201508200800"));
                    
                    System.out.println("glob_201507230000:" + cache.get("glob_201507230000"));
                    System.out.println("hour_2015072300:" + cache.get("hour_2015072300"));
                    System.out.println("user_123_1_201507230000:" + cache.get("user_123_1_201507230000"));
                    System.out.println("user_4_1_201507230000:" + cache.get("user_4_1_201507230000"));
                    System.out.println("user_4_0_201507230000:" + cache.get("user_4_0_201507230000"));
                    System.out.println("user_-1_1_201507230000:" + cache.get("user_-1_1_201507230000"));
                    System.out.println("user_-1_0_201507230000:" + cache.get("user_-1_0_201507230000"));
                    System.out.println("prov_1_201507230000:" + cache.get("prov_1_201507230000"));
                    System.out.println("prov_2_201507230000:" + cache.get("prov_2_201507230000"));
                    System.out.println("prov_3_201507230000:" + cache.get("prov_3_201507230000"));
                    System.out.println("prov_20_201507230000:" + cache.get("prov_20_201507230000"));
                    System.out.println("chnl_2_201507230000:" + cache.get("chnl_2_201507230000"));
                    System.out.println("chnl_13_201507230000:" + cache.get("chnl_13_201507230000"));
                    System.out.println("chnl_1001_201507230000:" + cache.get("chnl_1001_201507230000"));
                    System.out.println("chnl_1002_201507230000:" + cache.get("chnl_1002_201507230000"));
                    System.out.println("chnl_1003_201507230000:" + cache.get("chnl_1003_201507230000"));
                    System.out.println("chnl_1004_201507230000:" + cache.get("chnl_1004_201507230000"));
                    System.out.println("chnl_1005_201507230000:" + cache.get("chnl_1005_201507230000"));
                    System.out.println("chnl_1007_201507230000:" + cache.get("chnl_1007_201507230000"));
                    System.out.println("chnl_1008_201507230000:" + cache.get("chnl_1008_201507230000"));
                    System.out.println("chnl_1010_201507230000:" + cache.get("chnl_1010_201507230000"));
                    System.out.println("chnl_1011_201507230000:" + cache.get("chnl_1011_201507230000"));
                } catch (Exception e) {
                }
            }
        }, new String[] { "1-1-10-1000", "false" });
    }
}
