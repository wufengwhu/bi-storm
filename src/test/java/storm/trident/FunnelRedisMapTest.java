package storm.trident;

import com.yihaodian.bi.cached.AbstractBaseCMap;
import com.yihaodian.bi.hbase.HBaseConstant;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import com.yihaodian.bi.storm.trident.redis.common.JedisPoolConfig;
import kafka.TrackerBaseTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by fengwu on 15/9/15.
 */
public class FunnelRedisMapTest extends TrackerBaseTest {
    @Override
    public String getTopoName() {
        return "BI_Funnel";
    }

    public void testLogic() throws Exception {
        doTest(new Test() {
            @Override
            public void test() {
                try {
                    JedisPool jedisPool = new JedisPool();
                    Jedis cache = jedisPool.getResource();
                    System.out.println("land_2015072300:" + cache.get("land_2015072300"));
                    System.out.println("user_123_2015072300:" + cache.get("user_123_2015072300"));
                    System.out.println("user_4_2015072300:" + cache.get("user_4_2015072300"));
                    System.out.println("user_-1_2015072300:" + cache.get("user_-1_2015072300"));
                    System.out.println("detl_2015072300:" + cache.get("detl_2015072300"));
                    System.out.println("cms_2015072300:" + cache.get("cms_2015072300"));
                    System.out.println("kw_2015072300:" + cache.get("kw_2015072300"));
                    System.out.println("categ_2015072300:" + cache.get("categ_2015072300"));
                    System.out.println("chnl_2015072300:" + cache.get("chnl_2015072300"));
                    System.out.println("cms_detl_2015072300:" + cache.get("cms_detl_2015072300"));
                    System.out.println("kw_detl_2015072300:" + cache.get("kw_detl_2015072300"));
                    System.out.println("categ_detl_2015072300:" + cache.get("categ_detl_2015072300"));
                    System.out.println("chnl_detl_2015072300:" + cache.get("chnl_detl_2015072300"));
                    System.out.println("cart_2015072300:" + cache.get("cart_2015072300"));
                    System.out.println("rcmd_2015072300:" + cache.get("rcmd_2015072300"));
                    System.out.println("order_2015072300:" + cache.get("order_2015072300"));
                    System.out.println("rcmd_detl_2015072300:" + cache.get("rcmd_detl_2015072300"));
                    System.out.println("acdv_2015072300:" + cache.get("acdv_2015072300"));
                    System.out.println("core_2015072300:" + cache.get("core_2015072300"));
                    System.out.println("own_2015072300:" + cache.get("own_2015072300"));
                    //TrackerLogicUtils.clearCache(cache);
                    System.exit(0);

                } catch (Exception e) {

                }
            }
        }, new String[] { "1-1-10-1000", "false" });
    }
}
