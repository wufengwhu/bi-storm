package storm.trident;

import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.Scheme;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.yihaodian.bi.kafka.TrackerVO;
import com.yihaodian.bi.kafka.consumer.OfflineConsumer;
import com.yihaodian.bi.storm.common.util.Constant;
import com.yihaodian.bi.storm.logic.tracker.TrackerLogicUtils;
import com.yihaodian.bi.storm.trident.redis.common.JedisPoolConfig;
import com.yihaodian.bi.storm.trident.redis.state.Options;
import com.yihaodian.bi.storm.trident.redis.mapper.RedisDataTypeDescription;
import com.yihaodian.bi.storm.trident.redis.state.RedisMapState;
import com.yihaodian.bi.storm.trident.topology.filter.EmptyFieldFilter;
import com.yihaodian.bi.storm.trident.topology.filter.H5PCFilter;
import com.yihaodian.bi.storm.trident.topology.function.H5PCAnchor;
import com.yihaodian.common.yredis.RedisProxy;
import com.yihaodian.common.yredis.client.YredisProxyFactory;
import com.yihaodian.common.yredis.client.exception.RedisInitException;
import storm.kafka.BrokerHosts;
import storm.kafka.ZkHosts;
import storm.kafka.trident.OpaqueTridentKafkaSpout;
import storm.kafka.trident.TridentKafkaConfig;
import storm.trident.operation.builtin.Count;
import storm.trident.state.*;
import backtype.storm.Config;
import storm.trident.testing.FixedBatchSpout;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fengwu on 15/7/22
 */
public class TridentUVTopologyTest {

    private static final String KAFKA_ZK_TEST_CONNECTION = "192.168.39.11:2181,192.168.39.12:2181,192.168.39.13:2181";

    public static StormTopology buildTopology() {
        String clientId = "test_kafka";
        Fields fields = new Fields("trackerEvent", "channel", "platformId", "guid", "session_id", "timeline", "provId", "end_user_id", "day");
        List<List<Object>> output = new ArrayList<List<Object>>();
        OfflineConsumer consumer = new OfflineConsumer("sample.dat.zip");
        int count = 0;
        int index = 0;
        while (consumer.hasNext()) {
            count++;
            List<Object> trackerMsg = new ArrayList<Object>();
            String str = consumer.next();
            TrackerVO trackerVo = new TrackerVO(str.split("\\t"));
            //System.out.println(trackerVo.toString());
            trackerMsg.add(trackerVo);
            trackerMsg.add(Constant.SEO);
            int platformId = TrackerLogicUtils.getPlatformType(trackerVo).getType();
            //System.out.println("platformId:"+platformId);
            trackerMsg.add(platformId);
            trackerMsg.add(trackerVo.getGu_id());
            trackerMsg.add(trackerVo.getSession_id());
            String timeline = trackerVo.getTrack_time().replaceAll("[-: ]", "");
            trackerMsg.add(timeline);
            trackerMsg.add(TrackerLogicUtils.provId(trackerVo));
            trackerMsg.add(trackerVo.getEnd_user_id());
            trackerMsg.add(timeline.substring(0, 8));
            output.add(trackerMsg);
            if (count == 300) {
                break;
            }
        }
        List<Object>[] outputArray = new List[output.size()];
        for (int i = 0; i < output.size(); i++) {
            outputArray[i] = output.get(i);
        }

        FixedBatchSpout kafkaSpout = new FixedBatchSpout(fields, 300, outputArray);
//        FixedBatchSpout kafkaSpout2 = new FixedBatchSpout(fields, 300, outputArray);
        kafkaSpout.setCycle(true);
        // opaque kafka spout (support replay data from the last read position, it is optional)
        BrokerHosts brokerHosts = new ZkHosts(KAFKA_ZK_TEST_CONNECTION);
        TridentKafkaConfig kafkaConfig = new TridentKafkaConfig(brokerHosts, Constant.KAFKA_CONSUMER_NAME_TRACKER, clientId);
        kafkaConfig.scheme = new SchemeAsMultiScheme(new TrackerMessageScheme());
        OpaqueTridentKafkaSpout opaqueTridentKafkaSpout = new OpaqueTridentKafkaSpout(kafkaConfig);

        TridentTopology topology = new TridentTopology();
        Stream trackerStream = topology.newStream("UV_trident", opaqueTridentKafkaSpout);
                //Stream globTrackerStream = topology.newStream("glob_UV_trident", kafkaSpout);
                //.each(new Fields("rowTrackerMsgEvent"), new TrackerEventBuilder(), new Fields("trackerEvent"))
                //.each(new Fields("trackerEvent"), new SupplementFunction(),
                //        new Fields("channel", "platformId", "guid", "session_id", "timeline", "provId", "end_user_id", "day"));

        // Trident State
        JedisPoolConfig poolConfig = new JedisPoolConfig.Builder().build();
        //RedisProxy jedisProxy = getJedisClusterProxy("redis.xml");

        Options opts = new Options();
        opts.dataTypeDescription = new RedisDataTypeDescription(RedisDataTypeDescription.RedisDataType.STRING);
        opts.expireIntervalSec = 43200;
        StateFactory opaqueStatFactory = RedisMapState.opaque(poolConfig, opts);

        // h5pc uv (the tracker events doesn't need belong to the same session even they all launched by the same identity)
        trackerStream.project(new Fields("guid", "timeline", "end_user_id", "platformId", "day"))
                .each(new Fields("platformId"), new H5PCFilter())
                .each(new Fields("guid", "timeline", "end_user_id", "platformId"),
                        new H5PCAnchor(),
                        new Fields("h5pc_state_key"))
                .each(new Fields("h5pc_state_key"), new EmptyFieldFilter())
                .groupBy(new Fields("h5pc_state_key"))
                .persistentAggregate(opaqueStatFactory, new Count(), new Fields("h5pc_counts"));

//        Stream appValidTrackerStream = trackerStream.project(new Fields(
//                "channel", "platformId", "guid", "session_id", "timeline", "provId"))
//                .each(new Fields("channel", "platformId", "guid", "session_id", "timeline", "provId"),
//                        new TrackerAnchor(),
//                        new Fields("pre_ext_field2", "pre_channel", "cleaned_ext_field2",
//                                "cleaned_channel", "isInvalid", "isRepeated"))
//                .each(new Fields("isInvalid"), new InvalidUVFilter());
//
//        // app valid uv in ten minutes slides
//        appValidTrackerStream.each(new Fields("platformId"), new PlatformFilter())
//                .each(new Fields("platformId", "timeline"), new KeyFactory.AppStatKeyFactory(), new Fields("app_stat_key"))
//                .groupBy(new Fields("app_stat_key"))
//                .persistentAggregate(opaqueStatFactory, new Count(), new Fields())
//                .parallelismHint(3);
//
//        // glob valid and filtrate repeated uv in ten minutes slides,
//        validTrackerStream.each(new Fields("isRepeated"), new GlobRepeatedUVFilter())
//                .each(new Fields("timeline"), new KeyFactory.GlobStatKeyFactory(), new Fields("glob_valid_uv_key"))
//                .groupBy(new Fields("glob_valid_uv_key"))
//                .persistentAggregate(opaqueStatFactory, new Count(), new Fields("glob_stat_key_counts"));
////                .partitionPersist(opaqueStatFactory, new Fields("timeline"),
////                        new RedisClusterStateUpdater(globUVStoreMapper).withExpire(86400000));
//
//        // province valid uv in ten minutes slides, include modify previous uv statistics which field "provId" is null
//        validTrackerStream.each(new Fields("isRepeated", "pre_ext_field2", "cleaned_ext_field2", "timeline"),
//                new ProvDecremeter(jedisPool), new Fields("prov_stat_key"))
//                .groupBy(new Fields("prov_stat_key"))
////                .partitionPersist(opaqueStatFactory, new Fields("cleaned_ext_field2", "timeline"),
////                        new RedisClusterStateUpdater(provUVStoreMapper).withExpire(86400000));
//                .persistentAggregate(opaqueStatFactory, new Count(), new Fields("prov_stat_key_counts"));
//
//        // channel valid uv in ten minutes slides, include modify previous uv statistics which field "channel" is null
//        RedisStoreMapper channelUVStoreMapper = new ChannelUVCountStoreMapper();
//        validTrackerStream.each(new Fields("isRepeated", "pre_channel", "cleaned_channel", "timeline"),
//                new ChannelDecremeter(jedisPool), new Fields("chnl_stat_key"))
//                .groupBy(new Fields("chnl_stat_key"))
//                .persistentAggregate(opaqueStatFactory, new Count(), new Fields("chnl_stat_key_counts"));
//                .partitionPersist(opaqueStatFactory, new Fields("cleaned_channel", "timeline"),
//                        new RedisClusterStateUpdater(channelUVStoreMapper).withExpire(86400000));

        return topology.build();
    }

    private static RedisProxy getJedisClusterProxy(String jedisClusterConfigPath) {
        try {
            YredisProxyFactory.configure(jedisClusterConfigPath);
        } catch (RedisInitException e) {
            throw new RuntimeException("Couldn't initialize YredisProxyFactory cause by  " + e);
        }

        return YredisProxyFactory.getClient("bi_redis_test");
    }

    public static void main(String[] args) throws Exception {
        Config conf = new Config();
        conf.setMaxSpoutPending(5);
        RedisProxy redisProxy = getJedisClusterProxy("redis.xml");
        if (args.length == 0) {
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("uvCounter", conf, buildTopology());
            System.out.println("submit topology success");
            Thread.sleep(100 * 60 * 1000);
            cluster.killTopology("uvCounter");
            System.out.println("kill topology success");
            cluster.shutdown();
            System.out.println("shut down storm success");
            System.exit(0);
        } else if (args.length == 2) {
            conf.setNumWorkers(3);
            StormSubmitter.submitTopology(args[1], conf, buildTopology());
        } else {
            System.out.println("Usage: TridentUVTopology <redis configuration file url> [topology name]");
        }
    }

    private static class TrackerMessageScheme implements Scheme {
        public List<Object> deserialize(byte[] bytes) {
            try {
                String msg = new String(bytes, "UTF-8");
                return new Values(msg);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            //TODO: what happend if returns null?
            return null;
        }

        public Fields getOutputFields() {
            return new Fields("rowTrackerMsgEvent");
        }
    }
}
