package storm.trident.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.yihaodian.bi.storm.trident.hbase.common.HBaseProjectionCriteria;
import com.yihaodian.bi.storm.trident.hbase.mapper.HBaseValueMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.SimpleTridentHBaseMapMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.TridentHBaseMapMapper;
import com.yihaodian.bi.storm.trident.hbase.mapper.WordCountValueMapper;
import com.yihaodian.bi.storm.trident.hbase.state.HBaseMapState;
import com.yihaodian.bi.storm.trident.topology.function.PrintFunction;
import org.apache.hadoop.hbase.client.Durability;
import storm.trident.Stream;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.builtin.MapGet;
import storm.trident.operation.builtin.Sum;
import storm.trident.state.StateFactory;
import storm.trident.testing.FixedBatchSpout;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fengwu on 15/8/13
 */
public class WordCountTridentHbaseMapTopology {

    public static StormTopology buildTopology(String hbaseConfig) {
        Fields fields = new Fields("word", "count");
        FixedBatchSpout spout = new FixedBatchSpout(fields, 4,
                new Values("storm", 1),
                new Values("trident", 1),
                new Values("needs", 1),
                new Values("javadoc", 1)
        );
        spout.setCycle(true);

        TridentHBaseMapMapper tridentHBaseMapMapper = new SimpleTridentHBaseMapMapper("value");

        HBaseValueMapper rowToStormValueMapper = new WordCountValueMapper();

        HBaseProjectionCriteria projectionCriteria = new HBaseProjectionCriteria();
        projectionCriteria.addColumn(new HBaseProjectionCriteria.ColumnMetaData("cf", "value"));

        HBaseMapState.Options options = new HBaseMapState.Options()
                .withConfigKey(hbaseConfig)
                .withDurability(Durability.SYNC_WAL)
                .withMapper(tridentHBaseMapMapper)
                .withProjectionCriteria(projectionCriteria)
                .withRowToStormValueMapper(rowToStormValueMapper)
                .withColumnFamily("cf")
                .withTableName("word_count");

        StateFactory stateFactory = HBaseMapState.opaque(options);

        TridentTopology topology = new TridentTopology();
        Stream stream = topology.newStream("spout1", spout);

        TridentState state = stream.groupBy(new Fields("word"))
                .persistentAggregate(stateFactory, new Fields("count"), new Sum(), new Fields("sum"));

        stream.stateQuery(state, new Fields("word"), new MapGet(), new Fields("sum"))
                .each(new Fields("word", "sum"), new PrintFunction(), new Fields());

        return topology.build();

    }

    public static void main(String[] args) throws Exception {

        Config conf = new Config();
        //setLocalConfiguration(conf, args[0]);
        if (args.length == 1) {
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("wordCounter", conf, buildTopology(args[0]));
            Thread.sleep(60 * 1000);
            cluster.killTopology("wordCounter");
            cluster.shutdown();
            System.exit(0);
        } else if (args.length == 2) {
            conf.setNumWorkers(3);
            StormSubmitter.submitTopology(args[1], conf, buildTopology(args[0]));
        } else {
            System.out.println("Usage: TridentFileTopology <hdfs url> [topology name]");
        }
    }


}
